package com.pharmapoint.ui_fragment.home_fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.pharmapoint.R
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.adapter.CustomeSpinnerAdapterArrayListAdd
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddProcutFragmentBinding
import com.pharmapoint.viewmodel.AddProcutViewModel

import android.app.Dialog
import android.util.Log
import android.view.*
import android.widget.*
import com.pharmapoint.model.*
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import kotlinx.android.synthetic.main.add_procut_fragment.*
import android.view.MotionEvent

import android.view.View.OnTouchListener
import com.pharmapoint.AppConfig
import java.util.*
import kotlin.collections.ArrayList


class AddProcutFragment : Fragment() {

    companion object {
        fun newInstance() = AddProcutFragment()
    }

    private lateinit var subCategoryAdapter: CustomeSpinnerAdapterArrayListAdd
    private lateinit var db: AppDatabase
    private lateinit var selectedExpDate: Date
    private lateinit var statusArray: Array<String>
    private lateinit var customSpinAdapter: CustomeSpinnerAdapter
    private lateinit var addProcutFragmentBinding: AddProcutFragmentBinding
    var subCategoryList =  ArrayList<String>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        addProcutFragmentBinding = DataBindingUtil.inflate(inflater, com.pharmapoint.R.layout.add_procut_fragment, container, false)
        addProcutFragmentBinding.addProductViewModel = AddProcutViewModel()
        bindData()
//        bindSpinner();
        return addProcutFragmentBinding.root
    }

    override fun onResume() {
        super.onResume()
        if(AppConfig.lastScanedBarcod.isNotEmpty() && AppConfig.lastScanedBarcod .isNotBlank()) {
            addProcutFragmentBinding.edtBarcode.setText(AppConfig.lastScanedBarcod)
            AppConfig.lastScanedBarcod = ""
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun bindData() {
        db = AppDatabase.getDatabase(this.requireActivity())
        getCategory(false)
        getSubCategory(
            0,false
        )
        getMenufacturer(false)
        getDepartment(false)
        bindSpinner1()
        getBrand(false)

        addProcutFragmentBinding.edtExpDate.setOnClickListener {
            activity?.let { it1 ->
                Utils.showDatePickerFeature(it1){
                    if (it != null) {
                        selectedExpDate = it
                    }
                    addProcutFragmentBinding.edtExpDate.setText(Utils.toDateString(selectedExpDate))
                }

            }
        }

        addProcutFragmentBinding.edtBarcode.setOnTouchListener(OnTouchListener { _, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= addProcutFragmentBinding.edtBarcode.right - addProcutFragmentBinding.edtBarcode.compoundDrawables
                        .get(DRAWABLE_RIGHT).bounds.width()
                ) {
                    Utils.showAddBarcodeDialog(requireActivity(),"Submit",db,edtBarcode.text.toString().trim()){
                        if(it != null && it.isNotEmpty()){
                            addProcutFragmentBinding.edtBarcode.setText(it)
                        }
                    }
                    return@OnTouchListener true
                }
            }
            false
        })

        addProcutFragmentBinding.btnSave.setOnClickListener{

            if(edtBarcode.text.toString().trim().isEmpty() && edtBarcode.text.toString().trim().isBlank()){
                edtBarcode.error = view?.context?.getString(R.string.error_msg)
                edtBarcode.requestFocus()

            } else if(edt_item_code.text.toString().trim().isBlank() && edt_item_code.text.toString().trim().isEmpty()){
                edt_item_code.error = view?.context?.getString(R.string.error_msg)
                edt_item_code.requestFocus()

            } else if(edt_product_name.text.toString().trim().isBlank() && edt_product_name.text.toString().trim().isEmpty()){
                edt_product_name.error = view?.context?.getString(R.string.error_msg)
                edt_product_name.requestFocus()

            } else if(edt_lot_no.text.toString().trim().isBlank() && edt_lot_no.text.toString().trim().isEmpty()){
                edt_lot_no.error = view?.context?.getString(R.string.error_msg)
                edt_lot_no.requestFocus()

            } else if(edt_rec_stock_hand.text.toString().trim().isBlank() && edt_rec_stock_hand.text.toString().trim().isEmpty()){
                edt_rec_stock_hand.error = view?.context?.getString(R.string.error_msg)
                edt_rec_stock_hand.requestFocus()

            } else if(edt_retail_price.text.toString().trim().isBlank() && edt_retail_price.text.toString().trim().isEmpty()){
                edt_retail_price.error = view?.context?.getString(R.string.error_msg)
                edt_retail_price.requestFocus()

            } else if(Regex("^[0][1-9]+|[1-9]d*").matches(edt_pack_size.text)){
                edt_pack_size.error = view?.context?.getString(R.string.error_msg)
                edt_pack_size.requestFocus()

            } else if(edtVat.text.toString().trim().isBlank() && edtVat.text.toString().trim().isEmpty()){
                edtVat.error = view?.context?.getString(R.string.error_msg)
                edtVat.requestFocus()

            } else if(edt_exp_date.text.toString().trim().isBlank() && edt_exp_date.text.toString().trim().isEmpty()){
                Utils.showToast(requireContext(),"Select Expire Date")

            } else{
                insertData()
            }
        }
    }

    private fun insertData() {
        val productModel = ProductModel()
        val time = System.currentTimeMillis()

        if(edt_item_code.text.toString().trim().isNotEmpty() && edt_item_code.text.toString().trim().isNotBlank())
            productModel.item_code = edt_item_code.text.toString().trim()

        if(edt_product_name.text.toString().trim().isNotEmpty() && edt_product_name.text.toString().trim().isNotBlank())
            productModel.product_name = edt_product_name.text.toString().trim()

        if(edt_product_des.text.toString().trim().isNotEmpty() && edt_product_des.text.toString().trim().isNotBlank())
            productModel.product_description = edt_product_des.text.toString().trim()

        if(edt_pack_size.text.toString().trim().isNotEmpty() && edt_pack_size.text.toString().trim().isNotBlank())
            productModel.pack_size = edt_pack_size.text.toString().trim().toIntOrNull()

        if(edt_case_size.text.toString().trim().isNotEmpty() && edt_case_size.text.toString().trim().isNotBlank())
            productModel.case_size = edt_case_size.text.toString().trim().toIntOrNull()

        if(edt_case_cost.text.toString().trim().isNotEmpty() && edt_case_cost.text.toString().trim().isNotBlank())
            productModel.case_cost = edt_case_cost.text.toString().trim().toDouble()

        if(edt_unit_cost.text.toString().trim().isNotEmpty() && edt_unit_cost.text.toString().trim().isNotBlank())
            productModel.unit_cost = edt_unit_cost.text.toString().trim().toDouble()

        if(spn_allow_dis.selectedItemPosition == 0) {
            productModel.allow_discount = "No"

        }else
            productModel.allow_discount = "Yes"

        if(edt_order_level.text.toString().trim().isNotEmpty() && edt_order_level.text.toString().trim().isNotBlank())
            productModel.reorder_level = edt_order_level.text.toString().trim().toIntOrNull()

        if(edt_order_qty.text.toString().trim().isNotEmpty() && edt_order_qty.text.toString().trim().isNotBlank())
            productModel.reorder_qty = edt_order_qty.text.toString().trim().toIntOrNull()

        if(edt_retail_price.text.toString().trim().isNotEmpty() && edt_retail_price.text.toString().trim().isNotBlank())
        productModel.retail_cost = edt_retail_price.text.toString().trim().toDouble()

        if(edtVat.text.toString().trim().isNotEmpty() && edtVat.text.toString().trim().isNotBlank())
        productModel.vat = edtVat.text.toString().trim().toDouble()

        if(edtBuyingFrom.text.toString().trim().isNotEmpty() && edtBuyingFrom.text.toString().trim().isNotBlank())
            productModel.buying_from = edtBuyingFrom.text.toString().trim()

        if(spnDepartment.selectedItemPosition >0 && spnDepartment.selectedItemPosition < addProcutFragmentBinding.addProductViewModel!!.departmentList!!.value!!.size -1)
            productModel.department_id = addProcutFragmentBinding!!.addProductViewModel!!.departmentList!!.value!![spnDepartment.selectedItemPosition - 1]!!.id

        if(spnCategory.selectedItemPosition >0 && spnCategory.selectedItemPosition < addProcutFragmentBinding!!.addProductViewModel!!.categoryList!!.value!!.size -1)
            productModel.category_id = addProcutFragmentBinding!!.addProductViewModel!!.categoryList!!.value!![spnCategory.selectedItemPosition - 1]!!.id

        if(spnSubCategory.selectedItemPosition >0 && spnSubCategory.selectedItemPosition < addProcutFragmentBinding!!.addProductViewModel!!.subCategoryList!!.value!!.size -1)
            productModel.sub_category_id = addProcutFragmentBinding!!.addProductViewModel!!.subCategoryList!!.value!![spnSubCategory.selectedItemPosition - 1]!!.id

        if(spnManufacturer.selectedItemPosition >0 && spnManufacturer.selectedItemPosition < addProcutFragmentBinding!!.addProductViewModel!!.manufacturerList!!.value!!.size -1)
            productModel.manufacturer_id = addProcutFragmentBinding!!.addProductViewModel!!.manufacturerList!!.value!![spnManufacturer.selectedItemPosition - 1]!!.id

        if(spnBrand.selectedItemPosition >0 && spnBrand.selectedItemPosition < addProcutFragmentBinding!!.addProductViewModel!!.brandList!!.value!!.size -1)
            productModel.brand_id = addProcutFragmentBinding!!.addProductViewModel!!.brandList!!.value!![spnBrand.selectedItemPosition - 1]!!.id

        productModel.created_at = time
        productModel.expire_date = selectedExpDate.time
        val count = db.productDao().getExitsProduct(edt_item_code.text.toString().trim())
        productModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
        productModel.status = 1
        if(count == 0){
           val productId = db.productDao().insert(productModel).toInt()
            val barcodeArray = edtBarcode.text.toString().trim().split(",")
            if(barcodeArray.size > 0) {
                barcodeArray.forEach {
                    val barcodeModel = OrderBarcodeModel()
                    barcodeModel.barcode = it
                    barcodeModel.productId = productId
                    barcodeModel.created_at = time
                    db.orderBarcodeDao().insert(barcodeModel)
                }
            }else{
                val barcodeModel = OrderBarcodeModel()
                barcodeModel.barcode = edtBarcode.text.toString().trim()
                barcodeModel.productId = productId
                barcodeModel.created_at = time
                db.orderBarcodeDao().insert(barcodeModel)
            }
            val itemInventory = Item_inventoryModel()
            itemInventory.created_at = time
            itemInventory.expire_date = selectedExpDate.time
            itemInventory.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
            itemInventory.product_id = productId
            itemInventory.quantity  = edt_rec_stock_hand.text.toString().trim().toInt()
            itemInventory.rest_qty  = edt_rec_stock_hand.text.toString().trim().toInt()
            if(edt_lot_no.text.toString().trim().isNotEmpty() && edt_lot_no.text.toString().trim().isNotBlank())
                itemInventory.lot_no = edt_lot_no.text.toString().trim()

            db.itemInventery().insert(itemInventory)
            activity?.let { Utils.showToast(it,"Product Added successfully") }
            clearWidget()
        }else{
            activity?.let { Utils.showToast(it,"Product Already Exits") }
        }

        Log.d("success","success")
    }

    private fun clearWidget() {
        edtBarcode.text.clear()
        edt_item_code.text.clear()
        edt_product_name.text.clear()
        edt_lot_no.text.clear()
        edt_product_des.text.clear()
        edt_pack_size.text.clear()
        edt_rec_stock_hand.text.clear()
        edt_exit_stock_hand.text.clear()
        edt_case_size.text.clear()
        edt_case_cost.text.clear()
        edt_unit_cost.text.clear()
        spn_allow_dis.setSelection(0)
        edt_order_level.text.clear()
        edt_order_qty.text.clear()
        edt_profit.text.clear()
        edt_profit_val.text.clear()
        edt_retail_price.text.clear()
        edtVat.text.clear()
        edt_exp_date.text.clear()
        edtBuyingFrom.text.clear()
        spnDepartment.setSelection(0)
        spnCategory.setSelection(0)
        spnSubCategory.setSelection(0)
        spnManufacturer.setSelection(0)
        spnBrand.setSelection(0)
    }

    private fun getCategory(isSelected: Boolean) {

        addProcutFragmentBinding.addProductViewModel?.getCategory(db)?.observe(viewLifecycleOwner, Observer{ modelList ->
            val statusArray =  ArrayList<String>()
            statusArray.add("Select Category")
            if(modelList.isNotEmpty()){
                modelList.forEach{
                    statusArray.add(it.title!!)
                }
            }
            statusArray.add("Add New Category")
            addProcutFragmentBinding.spnCategory.adapter = CustomeSpinnerAdapterArrayListAdd(requireContext(), statusArray)
            addProcutFragmentBinding.spnCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if(p2 == statusArray.size-1 && statusArray.size > 1){
                        showDialog(1)
                    }
                    else if(p2 > 0) {
                        val parentID = addProcutFragmentBinding!!.addProductViewModel!!.categoryList!!.value!![p2-1]!!.id
                        getSubCategory(
                            parentID!!
                        ,false)
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }
            if(isSelected){
                addProcutFragmentBinding.spnCategory.setSelection(statusArray.size-2)
                val parentID = addProcutFragmentBinding!!.addProductViewModel!!.categoryList!!.value!![addProcutFragmentBinding.spnCategory.selectedItemPosition -1]!!.id
                getSubCategory(
                    parentID!!
                    ,false)
            }
//            bindSpinner(addProcutFragmentBinding.spnCategory,statusArray)
        })
    }
    private fun getMenufacturer(isSelected: Boolean) {
        addProcutFragmentBinding.addProductViewModel?.getManufacturer(db)?.observe(viewLifecycleOwner, Observer{ modelList ->
            var statusArray =  ArrayList<String>()
            statusArray.add("Select Manufacturer")
            if(modelList.isNotEmpty()){
                modelList.forEach{
                    statusArray.add(it.title!!)
                }
            }
            statusArray.add("Add New Manufacturer")
            bindSpinner(addProcutFragmentBinding.spnManufacturer,statusArray)


            addProcutFragmentBinding.spnManufacturer.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if(p2 == statusArray.size-1 && statusArray.size > 1){
                        showDialog(2)
                    }
                }
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }
            }
            if(isSelected){
                addProcutFragmentBinding.spnManufacturer.setSelection(statusArray.size-2)
            }
        })
    }
    private fun getDepartment(isSelected: Boolean) {
        addProcutFragmentBinding.addProductViewModel?.getDepartment(db)?.observe(viewLifecycleOwner, Observer{ modelList ->
            var statusArray =  ArrayList<String>()
            statusArray.add("Select Department")
            if(modelList.isNotEmpty()){
                modelList.forEach{
                    statusArray.add(it.title!!)
                }
            }
            statusArray.add("Add New Department")
            bindSpinner(addProcutFragmentBinding.spnDepartment,statusArray)

            addProcutFragmentBinding.spnDepartment.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if(p2 == statusArray.size-1 && statusArray.size > 1){
                        showDialog(3)
                    }
                }
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }
            }
            if(isSelected){
                addProcutFragmentBinding.spnDepartment.setSelection(statusArray.size-2)
            }
        })
    }   private fun getBrand(isSelected : Boolean) {
        addProcutFragmentBinding.addProductViewModel?.getBrand(db)?.observe(viewLifecycleOwner, Observer{ modelList ->
            var statusArray =  ArrayList<String>()
            statusArray.add("Select Brand")
            if(modelList.isNotEmpty()){
                modelList.forEach{
                    statusArray.add(it.title!!)
                }
            }
            statusArray.add("Add New Brand")
            bindSpinner(addProcutFragmentBinding.spnBrand,statusArray)
            addProcutFragmentBinding.spnBrand.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if(p2 == statusArray.size-1 && statusArray.size > 1){
                        showDialog(4)
                    }
                }
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }
            }
            if(isSelected){
                addProcutFragmentBinding.spnBrand.setSelection(statusArray.size-2)
            }
        })
    }

    private fun getSubCategory(parentID: Int,isSelected: Boolean) {
        addProcutFragmentBinding.addProductViewModel?.getSubCategory(db,parentID)?.observe(viewLifecycleOwner, Observer{ modelList ->
            subCategoryList.clear()
            subCategoryList.add("Select Sub Category")
            if(modelList.isNotEmpty()){
                modelList.forEach{
                    subCategoryList.add(it.title!!)
                }
            }
//           if(this::subCategoryAdapter.isInitialized){
//               subCategoryAdapter.notifyDataSetChanged()
//           }else {
               subCategoryAdapter =
                   CustomeSpinnerAdapterArrayListAdd(requireContext(), subCategoryList)
               addProcutFragmentBinding.spnSubCategory.adapter = subCategoryAdapter
//           }
        })
        subCategoryList.add("Add New Sub Category")
        addProcutFragmentBinding.spnSubCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 == subCategoryList.size-1 &&  subCategoryList.size > 1){
                    showDialog(5)
                }
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
        if(isSelected){
            addProcutFragmentBinding.spnSubCategory.setSelection(subCategoryList.size-2)
        }
    }

    private fun bindSpinner(spnAllowDis: Spinner, statusArray: java.util.ArrayList<String>) {
        spnAllowDis.adapter = CustomeSpinnerAdapterArrayListAdd(requireContext(), statusArray)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AddProcutViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun bindSpinner1() {
        statusArray = arrayOf("No", "Yes")
        customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }!!
        addProcutFragmentBinding.spnAllowDis.adapter = customSpinAdapter
    }

    fun showDialog(type:Int) {
        //1 add category, 2 manufacturer , 3 department, 4 brand, 5 SubCagegory
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_add_dialog)
        val edtTitle : EditText = dialog.findViewById(R.id.edtTitle) as EditText
        val edtComment : EditText = dialog.findViewById(R.id.edtDescription) as EditText
        var title = String()
        if(type == 1){
            title = "Add New Category"
        }else if(type == 2){
            title = "Add New Manufacturer"
        }else if(type == 3){
            title = "Add New Department"
        }else if(type == 4){
            title = "Add New Brand"
        }else if(type == 5){
            title = "Add New Sub Category"
        }
        val text = dialog.findViewById(R.id.txtTitle) as TextView
        text.text = title
        val dialogButton: Button = dialog.findViewById(R.id.btnSave) as Button
        val dialogButtonCancle: Button = dialog.findViewById(R.id.btnCancel) as Button
        dialogButtonCancle.setOnClickListener{
            if(type == 1){
                spnCategory.setSelection(0)
            }else if(type == 2){
                spnManufacturer.setSelection(0)
            }else if(type == 3){
                spnDepartment.setSelection(0)
            }else if(type == 4){
                spnBrand.setSelection(0)
            }else if (type == 5){
                spnSubCategory.setSelection(0)
            }
            dialog.dismiss()
        }
        dialogButton.setOnClickListener(View.OnClickListener {
            if(edtTitle.text.toString().trim().isEmpty()){
               Utils.showToast(requireContext(),"Enter Title")
            }else {
                val time = System.currentTimeMillis()
                if (type == 1 || type == 5) {
                    val categoryModel = CategoryModel()
                    categoryModel.created_at = time
                    categoryModel.updated_at = time
                    categoryModel.comment = edtComment.text.toString().trim()
                    categoryModel.status = 1
                    categoryModel.title = edtTitle.text.toString().trim()
                    if (type == 5) {
                        val parentID =
                            addProcutFragmentBinding!!.addProductViewModel!!.categoryList!!.value!![addProcutFragmentBinding.spnCategory.selectedItemPosition - 1]!!.id
                        categoryModel.parent_id = parentID
                    }
                    categoryModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.categoryDao().insert(categoryModel)
                    getCategory(true)
                } else if (type == 2) {
                    val manufactureModel = ManufactureModel()
                    manufactureModel.created_at = time
                    manufactureModel.updated_at = time
                    manufactureModel.comment = edtComment.text.toString().trim()
                    manufactureModel.status = 1
                    manufactureModel.title = edtTitle.text.toString().trim()
                    manufactureModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.manufactureDao().insert(manufactureModel)
                    getMenufacturer(true)
                } else if (type == 3) {
                    val appoinmentModel = DepartmentModel()
                    appoinmentModel.created_at = time
                    appoinmentModel.updated_at = time
                    appoinmentModel.comment = edtComment.text.toString().trim()
                    appoinmentModel.status = 1
                    appoinmentModel.title = edtTitle.text.toString().trim()
                    appoinmentModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.departmentDao().insert(appoinmentModel)
                    getDepartment(true)
                } else if (type == 4) {
                    val brandModel = BrandModel()
                    brandModel.created_at = time
                    brandModel.updated_at = time
                    brandModel.comment = edtComment.text.toString().trim()
                    brandModel.status = 1
                    brandModel.title = edtTitle.text.toString().trim()
                    brandModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.brandDao().insert(brandModel)
                    getBrand(true)
                } else if (type == 5) {

                }
                dialog.dismiss()
            }
        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }
}