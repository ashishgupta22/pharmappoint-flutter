package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.*
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddEditBrandFragmentBinding
import com.pharmapoint.model.BrandModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.AddBrandViewModel
import java.util.*

class AddEditBrandFragment : Fragment() {

    companion object {
        fun newInstance() = AddEditBrandFragment()
    }

    private var parentId: Int? = null
    private var exitsID: Int? = 0
    private lateinit var statusArray: Array<String>
    private var customSpinAdapter: CustomeSpinnerAdapter? = null
    var brandList = mutableListOf<BrandModel>()
    var brandListTemp = mutableListOf<BrandModel>()
    private lateinit var db: AppDatabase
    private lateinit var brandAdapter: BrandAdapter
    private lateinit var brandFragmentBinding: AddEditBrandFragmentBinding
    var brandModel = BrandModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        brandFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.add_edit_brand_fragment, container, false)
        brandFragmentBinding.addBrandViewModel = context?.let { AddBrandViewModel(it) }
        bindData();
        return  brandFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    private fun bindData() {
         db = AppDatabase.getDatabase(this.requireActivity())
        getBrandList(null)

        brandFragmentBinding.btnSearch.setOnClickListener {
            fillterBySearch(brandFragmentBinding.edtSearch.text.toString().trim());
        }

        brandFragmentBinding.btnClearFilter.setOnClickListener{
            clearFilter()
        }

        bindSpinner();
        brandFragmentBinding.btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(brandFragmentBinding.edtCatTitle.text)) {
                brandFragmentBinding.edtCatTitle.error =
                    requireView().context.getString(R.string.error_msg)
                brandFragmentBinding.edtCatTitle.requestFocus()
            } else {
                val time = System.currentTimeMillis()
                var status : Int = 0
                if(brandFragmentBinding.spnStatus.selectedItemPosition == 0){
                    status = 1
                }else{
                    status = 0
                }
                if (exitsID!! > 0) {
                    brandModel.updated_at = time
                    brandModel.comment = brandFragmentBinding.edtComment.text.toString()
                    brandModel.status = status
                    brandModel.title =
                        brandFragmentBinding.edtCatTitle.text.toString()
                    db.brandDao().update(brandModel)
                } else {
                    brandModel.created_at = time
                    brandModel.updated_at = time
                    brandModel.comment = brandFragmentBinding.edtComment.text.toString()
                    brandModel.status = status
                    brandModel.title = brandFragmentBinding.edtCatTitle.text.toString()
                    brandModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.brandDao().insert(brandModel)
                }
                clearWeidgts()
                getBrandList(null)
            }
        }

    }

    private fun clearFilter() {
        brandFragmentBinding.edtSearch.setText("")
        fillterBySearch("")
    }

    private fun fillterBySearch(searchText : String){
        if(brandListTemp.isNotEmpty()){
            brandList.clear()
            brandList.addAll(brandListTemp.filter {it.title!!.toLowerCase().contains(searchText.toLowerCase())})
            brandAdapter.notifyDataSetChanged()
        }
    }



    private fun clearWeidgts() {
        brandFragmentBinding.edtCatTitle.setText("")
        brandFragmentBinding.edtComment.setText("")
        brandFragmentBinding.spnStatus.setSelection(0)
        exitsID = 0
    }

    private fun bindSpinner() {
        statusArray = arrayOf("Active", "Inactive")
         customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }
        brandFragmentBinding.spnStatus.adapter = customSpinAdapter
    }

    private fun getBrandList(parentId1: Int?) {
        brandList.clear()
        brandListTemp.clear()
        brandList.addAll(db.brandDao().getBrandList())
        brandListTemp.addAll(brandList)
        Log.d("listData", "test${brandList.size} ${brandListTemp.size}")
//        if(this::brandAdapter.isInitialized)
//            brandAdapter.notifyDataSetChanged()
//        else{
            brandAdapter = activity?.let {
                BrandAdapter(it,brandList){ type, position ->
                    //type 1  = delete type 0 = edit
                    if(type == 1){
                        Utils.showConfirmationDialog(requireActivity(),"Alert","Are You Sure You Want to Delete the Brand","Yes","No"){ it1 ->
                            if(it1!!){
                                brandList[position!!].id?.let { it1 -> db.brandDao().deleteBrandItemByID(it1,
                                    Date().time
                                ) }
                                getBrandList(null)
                            }
                        }
                    }else{
                        brandFragmentBinding.edtCatTitle.setText(brandList[position!!].title)
                        brandFragmentBinding.edtComment.setText(brandList[position!!].comment)
                        brandFragmentBinding.spnStatus.setSelection(0)
                        exitsID = brandList[position!!].id
                        brandModel = brandList[position!!]
                        if(brandList[position!!].status == 1){
                            brandFragmentBinding.spnStatus.setSelection(0)
                        }else brandFragmentBinding.spnStatus.setSelection(1)
                    }
                }
            }!!
            brandFragmentBinding.rcyShopfront.adapter = brandAdapter
//        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}