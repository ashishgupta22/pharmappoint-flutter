package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.adapter.DepartmentAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddEditDepartmentFragmentBinding
import com.pharmapoint.model.DepartmentModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.AppointmentViewModel
import java.util.*

class AddEditDepartmentFragment : Fragment() {

    companion object {
        fun newInstance() = AddEditDepartmentFragment()
    }

    private var parentId: Int? = null
    private var exitsID: Int? = 0
    private lateinit var statusArray: Array<String>
    private var customSpinAdapter: CustomeSpinnerAdapter? = null
    var departmentList = mutableListOf<DepartmentModel>()
    var departmentListTemp = mutableListOf<DepartmentModel>()
    private lateinit var db: AppDatabase
    private lateinit var departmentAdapter: DepartmentAdapter
    private lateinit var departmentFragmentBinding: AddEditDepartmentFragmentBinding
    var departmentModel = DepartmentModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        departmentFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.add_edit_department_fragment, container, false)
        departmentFragmentBinding.appointmentViewModel = context?.let { AppointmentViewModel(it) }
        bindData();
        return  departmentFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    private fun bindData() {
         db = AppDatabase.getDatabase(this.requireActivity())
        getDepartmentList()

        departmentFragmentBinding.btnSearch.setOnClickListener {
            fillterBySearch(departmentFragmentBinding.edtSearch.text.toString());
        }

        departmentFragmentBinding.btnClearFilter.setOnClickListener {
            fillterBySearch("")
        }
        bindSpinner();
        departmentFragmentBinding.btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(departmentFragmentBinding.edtCatTitle.text)) {
                departmentFragmentBinding.edtCatTitle.error =
                    requireView().context.getString(R.string.error_msg)
                departmentFragmentBinding.edtCatTitle.requestFocus()
            } else {
                val time = System.currentTimeMillis()
                var status : Int = 0
                if(departmentFragmentBinding.spnStatus.selectedItemPosition == 0){
                    status = 1
                }else{
                    status = 0
                }
                if (exitsID!! > 0) {
                    departmentModel.updated_at = time
                    departmentModel.comment = departmentFragmentBinding.edtComment.text.toString()
                    departmentModel.status = status
                    departmentModel.title =
                        departmentFragmentBinding.edtCatTitle.text.toString()
                    db.departmentDao().update(departmentModel)
                } else {
                    departmentModel.created_at = time
                    departmentModel.updated_at = time
                    departmentModel.comment = departmentFragmentBinding.edtComment.text.toString()
                    departmentModel.status = status
                    departmentModel.title = departmentFragmentBinding.edtCatTitle.text.toString()
                    departmentModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.departmentDao().insert(departmentModel)
                }
                clearWeidgts()
                getDepartmentList()
            }
        }

    }
    private fun fillterBySearch(searchText : String){
        if(departmentListTemp.isNotEmpty()){
            departmentList.clear()
            departmentList.addAll(departmentListTemp.filter {it.title!!.toLowerCase().contains(searchText.toLowerCase())})
            departmentAdapter.notifyDataSetChanged()
        }
    }

    private fun clearWeidgts() {
        departmentFragmentBinding.edtCatTitle.setText("")
        departmentFragmentBinding.edtComment.setText("")
        departmentFragmentBinding.spnStatus.setSelection(0)
        exitsID = 0
    }

    private fun bindSpinner() {
        statusArray = arrayOf("Active", "Inactive")
         customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }
        departmentFragmentBinding.spnStatus.adapter = customSpinAdapter
    }


    private fun getDepartmentList() {
        departmentList.clear()
        departmentListTemp.clear()
        departmentList.addAll(db.departmentDao().getDepartmentList())
        departmentListTemp.addAll(departmentList)
        Log.d("listData", "test${departmentList.size} ${departmentListTemp.size}")
//        if(this::departmentAdapter.isInitialized)
//            departmentAdapter.notifyDataSetChanged()
//        else{
            departmentAdapter = activity?.let {
                DepartmentAdapter(it,departmentList){ type, position ->
                    //type 1  = delete type 0 = edit
                    if(type == 1){
                        Utils.showConfirmationDialog(requireActivity(),"Alert","Are You Sure You Want to Delete the Department","Yes","No"){ it1 ->
                            if(it1!!){
                                departmentList[position!!].id?.let { it1 -> db.departmentDao().deleteDepItemByID(it1,
                                    Date().time
                                ) }
                                getDepartmentList()
                            }
                        }
                    }else{
                        departmentFragmentBinding.edtCatTitle.setText(departmentList[position!!].title)
                        departmentFragmentBinding.edtComment.setText(departmentList[position!!].comment)
                        departmentFragmentBinding.spnStatus.setSelection(0)
                        exitsID = departmentList[position!!].id
                        departmentModel = departmentList[position!!]
                        if(departmentList[position!!].status == 1){
                            departmentFragmentBinding.spnStatus.setSelection(0)
                        }else departmentFragmentBinding.spnStatus.setSelection(1)
                    }
                }
            }!!
            departmentFragmentBinding.rcyShopfront.adapter =departmentAdapter
//        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}