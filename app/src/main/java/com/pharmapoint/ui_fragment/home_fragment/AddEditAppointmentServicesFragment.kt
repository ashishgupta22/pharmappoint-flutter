package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.*
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddEditAppointmentServicesFragmentBinding
import com.pharmapoint.model.AppointmentServicesModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.AppointmentViewModel
import java.util.*

class AddEditAppointmentServicesFragment : Fragment() {

    companion object {
        fun newInstance() = AddEditAppointmentServicesFragment()
    }

    private var parentId: Int? = null
    private var exitsID: Int? = 0
    private lateinit var statusArray: Array<String>
    private var customSpinAdapter: CustomeSpinnerAdapter? = null
    var appointmentServiceList = mutableListOf<AppointmentServicesModel>()
    var appointmentServiceListTemp = mutableListOf<AppointmentServicesModel>()
    private lateinit var db: AppDatabase
    private lateinit var appointmentServicesAdapter: AppointmentServicesAdapter
    private lateinit var appointmentServicesFragmentBinding: AddEditAppointmentServicesFragmentBinding
    var appointmentServicesModel = AppointmentServicesModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        appointmentServicesFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.add_edit_appointment_services_fragment, container, false)
        appointmentServicesFragmentBinding.appointmentViewModel = context?.let { AppointmentViewModel(it) }
        bindData();
        return  appointmentServicesFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    private fun bindData() {
         db = AppDatabase.getDatabase(this.requireActivity())
        getAppointmentServicesList()

        appointmentServicesFragmentBinding.btnSearch.setOnClickListener {
            fillterBySearch(appointmentServicesFragmentBinding.edtSearch.text.toString());
        }

        bindSpinner();
        appointmentServicesFragmentBinding.btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(appointmentServicesFragmentBinding.edtServiceName.text)) {
                appointmentServicesFragmentBinding.edtServiceName.error =
                    requireView().context.getString(R.string.error_msg)
                appointmentServicesFragmentBinding.edtServiceName.requestFocus()
            } else if (TextUtils.isEmpty(appointmentServicesFragmentBinding.edtPrice.text)) {
                appointmentServicesFragmentBinding.edtPrice.error =
                    requireView().context.getString(R.string.error_msg)
                appointmentServicesFragmentBinding.edtPrice.requestFocus()
            } else {
                val time = System.currentTimeMillis()
                var status : Int = 0
                if(appointmentServicesFragmentBinding.spnStatus.selectedItemPosition == 0){
                    status = 1
                }else{
                    status = 0
                }
                if (exitsID!! > 0) {
                    appointmentServicesModel.updated_at = time
                    appointmentServicesModel.comment = appointmentServicesFragmentBinding.edtComment.text.toString()
                    appointmentServicesModel.price = appointmentServicesFragmentBinding.edtPrice.text.toString()
                    appointmentServicesModel.status = status
                    appointmentServicesModel.title =
                        appointmentServicesFragmentBinding.edtServiceName.text.toString()
                    db.appointmentServicesDao().update(appointmentServicesModel)
                } else {
                    appointmentServicesModel.created_at = time
                    appointmentServicesModel.updated_at = time
                    appointmentServicesModel.comment = appointmentServicesFragmentBinding.edtComment.text.toString()
                    appointmentServicesModel.price = appointmentServicesFragmentBinding.edtPrice.text.toString()
                    appointmentServicesModel.status = status
                    appointmentServicesModel.title = appointmentServicesFragmentBinding.edtServiceName.text.toString()
                    appointmentServicesModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.appointmentServicesDao().insert(appointmentServicesModel)
                }
                clearWeidgts()
                getAppointmentServicesList()
            }
        }

    }
    private fun fillterBySearch(searchText : String){
        if(appointmentServiceListTemp.isNotEmpty()){
            appointmentServiceList.clear()
            appointmentServiceList.addAll(appointmentServiceListTemp.filter {it.title!!.toLowerCase().contains(searchText.toLowerCase())})
            appointmentServicesAdapter.notifyDataSetChanged()
        }
    }



    private fun clearWeidgts() {
        appointmentServicesFragmentBinding.edtServiceName.setText("")
        appointmentServicesFragmentBinding.edtComment.setText("")
        appointmentServicesFragmentBinding.edtPrice.setText("")
        appointmentServicesFragmentBinding.spnStatus.setSelection(0)
        exitsID = 0
    }

    private fun bindSpinner() {
        statusArray = arrayOf("Active", "Inactive")
         customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }
        appointmentServicesFragmentBinding.spnStatus.adapter = customSpinAdapter
    }

    private fun getAppointmentServicesList() {
        appointmentServiceList.clear()
        appointmentServiceListTemp.clear()
        appointmentServiceList.addAll(db.appointmentServicesDao().getAppointmentServicesList())
        appointmentServiceListTemp.addAll(appointmentServiceList)
        Log.d("listData", "test${appointmentServiceList.size} ${appointmentServiceListTemp.size}")
//        if(this::appointmentServicesAdapter.isInitialized)
//            appointmentServicesAdapter.notifyDataSetChanged()
//        else{
            appointmentServicesAdapter = activity?.let {
                AppointmentServicesAdapter(it,appointmentServiceList){ type, position ->
                    //type 1  = delete type 0 = edit
                    if(type == 1){
                        Utils.showConfirmationDialog(requireActivity(),"Alert","Are You Sure You Want to Delete the Appointment Service","Yes","No"){ it1 ->
                            if(it1!!){
                                appointmentServiceList[position!!].id?.let { it1 -> db.appointmentServicesDao().deleteAppointmentServicesByID(it1,
                                    Date().time
                                ) }
                                getAppointmentServicesList()
                            }
                        }
                    }else{
                        appointmentServicesFragmentBinding.edtServiceName.setText(appointmentServiceList[position!!].title)
                        appointmentServicesFragmentBinding.edtComment.setText(appointmentServiceList[position!!].comment)
                        appointmentServicesFragmentBinding.edtPrice.setText(appointmentServiceList[position!!].price)
                        appointmentServicesFragmentBinding.spnStatus.setSelection(0)
                        exitsID = appointmentServiceList[position!!].id
                        appointmentServicesModel = appointmentServiceList[position!!]
                        if(appointmentServiceList[position!!].status == 1){
                            appointmentServicesFragmentBinding.spnStatus.setSelection(0)
                        }else appointmentServicesFragmentBinding.spnStatus.setSelection(1)
                    }
                }
            }!!
            appointmentServicesFragmentBinding.rcyShopfront.adapter = appointmentServicesAdapter
//        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}