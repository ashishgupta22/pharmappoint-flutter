package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.buy786.api.ApiRepository
import com.pharmapoint.R
import com.pharmapoint.adapter.CategoryAdapter
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddEditCategoryFragmentBinding
import com.pharmapoint.databinding.SetupPinFragmentBinding
import com.pharmapoint.model.CategoryModel
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.AppointmentViewModel
import com.pharmapoint.viewmodel.SetupPinViewModel
import java.util.*

class SetupPinFragment : Fragment() {

    companion object {
        fun newInstance() = SetupPinFragment()
    }

    private var parentId: Int? = null
    private var exitsID: Int? = 0
    private lateinit var statusArray: Array<String>
    private var customSpinAdapter: CustomeSpinnerAdapter? = null
    var list = mutableListOf<CategoryModel>()
    var listTemp = mutableListOf<CategoryModel>()
    var listParent = mutableListOf<CategoryModel>()
    private lateinit var db: AppDatabase
    private lateinit var categoryAdapter: CategoryAdapter
    private lateinit var setupPinFragmentBinding: SetupPinFragmentBinding
    var categoryModel = CategoryModel()
    private val apiRepository: ApiRepository = ApiRepository()
    var type = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupPinFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.setup_pin_fragment, container, false)
        setupPinFragmentBinding.setupPinViewModel = context?.let { SetupPinViewModel(it) }
        bindData();
        return  setupPinFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    private fun bindData() {
        type = arguments!!.getInt("type",0)
        if(type == 2){
            setupPinFragmentBinding.txtOldPin.visibility = View.GONE
            setupPinFragmentBinding.inputOldPin.visibility = View.GONE
        }
        setupPinFragmentBinding.btnSubmit.setOnClickListener {
            when {
                TextUtils.isEmpty(setupPinFragmentBinding.edtEnterPin.text.trim()) -> {
                    setupPinFragmentBinding.edtEnterPin.error =
                        requireView().context.getString(R.string.error_msg)
                    setupPinFragmentBinding.edtEnterPin.requestFocus()
                }
                setupPinFragmentBinding.edtEnterPin.text.trim().length < 6 -> {
                    setupPinFragmentBinding.edtEnterPin.error = "Pin Must be 6 Digits"
                    setupPinFragmentBinding.edtEnterPin.requestFocus()
                }
                TextUtils.isEmpty(setupPinFragmentBinding.edtConfirmPin.text.trim()) -> {
                    setupPinFragmentBinding.edtConfirmPin.error =
                        requireView().context.getString(R.string.error_msg)
                    setupPinFragmentBinding.edtConfirmPin.requestFocus()
                }
                setupPinFragmentBinding.edtConfirmPin.text.trim().length < 6 -> {
                    setupPinFragmentBinding.edtConfirmPin.error = "Pin Must be 6 Digits"
                    setupPinFragmentBinding.edtConfirmPin.requestFocus()
                }
                setupPinFragmentBinding.edtEnterPin.text.trim() != setupPinFragmentBinding.edtConfirmPin.text.trim() -> {
                    Utils.showToast(requireContext(),"Pin Number Dose not Match")
                }
                else -> {
                    if(type == 1){
                        if(setupPinFragmentBinding.edtOldPin.text.trim().length < 6 ) {
                            setupPinFragmentBinding.edtOldPin.error = "Pin Must be 6 Digits"
                            setupPinFragmentBinding.edtOldPin.requestFocus()
                            return@setOnClickListener
                        }
                        val pin = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.LOGIN_MPIN                        )
                        if(setupPinFragmentBinding.edtOldPin.text.trim().toString() != pin.toString()) {
                            setupPinFragmentBinding.edtOldPin.error = "Old Pin not matched"
                            setupPinFragmentBinding.edtOldPin.requestFocus()
                            return@setOnClickListener
                        }
                    }
                    val params = HashMap<String,String>()
                    params.put(Constans.mpin,setupPinFragmentBinding.edtConfirmPin.text.trim().toString())
                    apiRepository.setUpmPIN(
                        requireContext(),
                        params,
                        type
                    )
                    clearWeidgts()
                }
            }
        }
    }
//    private fun fillterBySearch(searchText : String){
//        if(listTemp.isNotEmpty()){
//            list.clear()
//            list.addAll(listTemp.filter {it.title!!.toLowerCase().contains(searchText.toLowerCase())})
//            categoryAdapter.notifyDataSetChanged()
//        }
//    }
//
    private fun clearWeidgts() {
        setupPinFragmentBinding.edtOldPin.setText("")
        setupPinFragmentBinding.edtEnterPin.setText("")
        setupPinFragmentBinding.edtConfirmPin.setText("")
        setupPinFragmentBinding.edtConfirmPin.clearFocus()
        setupPinFragmentBinding.edtEnterPin.clearFocus()
    }
//
//    private fun bindSpinner() {
//        statusArray = arrayOf("Active", "Inactive")
//         customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }
//        categoryFragmentBinding.spnStatus.adapter = customSpinAdapter
//    }
//    private fun getCategoryList(parentId1: Int?) {
//        list.clear()
//        listTemp.clear()
//        list.addAll(db.categoryDao().getCategoryList())
//        listTemp.addAll(list)
//        Log.d("listData", "test${list.size} ${listTemp.size}")
//        if(this::categoryAdapter.isInitialized)
//            categoryAdapter.notifyDataSetChanged()
//        else{
//            categoryAdapter = activity?.let {
//                CategoryAdapter(it,list){type, position ->
//                    //type 1  = delete type 0 = edit
//                    if(type == 1){
//                        list[position!!].id?.let { it1 -> db.categoryDao().deleteCatItemByID(it1,
//                            Date().time
//                        ) }
//                        getCategoryList(null)
//                    }else{
//                        categoryFragmentBinding.edtCatTitle.setText(list[position!!].title)
//                        categoryFragmentBinding.edtComment.setText(list[position].comment)
//                        categoryFragmentBinding.spnStatus.setSelection(0)
//                        exitsID = list[position].id
//                        categoryModel = list[position]
//                        if(list[position].status == 1){
//                            categoryFragmentBinding.spnStatus.setSelection(0)
//                        }else categoryFragmentBinding.spnStatus.setSelection(1)
//                    }
//                }
//            }!!
//            categoryFragmentBinding.rcyCategory.adapter = categoryAdapter
//        }
//    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}