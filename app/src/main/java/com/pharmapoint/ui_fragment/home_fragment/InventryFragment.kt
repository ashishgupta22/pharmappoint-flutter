package com.pharmapoint.ui_fragment.home_fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.pharmapoint.R
import com.pharmapoint.activity.MainActivity
import com.pharmapoint.adapter.InventoryAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.InventryFragmentBinding
import com.pharmapoint.model.Item_inventoryModel
import com.pharmapoint.model.ProductModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.InventryViewModel
import kotlinx.android.synthetic.main.add_procut_fragment.*
import java.util.*

class InventryFragment : Fragment() {

    companion object {
        fun newInstance() = InventryFragment()
    }

    private lateinit var viewModel: InventryViewModel
    private lateinit var inventryFragmentBinding: InventryFragmentBinding
    private lateinit var inventoryAdapter: InventoryAdapter
    private val inventoryList = mutableListOf<ProductModel>()
    private val inventoryListTemp = mutableListOf<ProductModel>()
    private lateinit var db: AppDatabase

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inventryFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.inventry_fragment, container, false)
        inventryFragmentBinding.inventoryViewModel = InventryViewModel()
        bindData()
        return inventryFragmentBinding.root
    }

    override fun onResume() {
        super.onResume()
        bindData()
        inventryFragmentBinding.edtSearch.text.clear()
    }
    private fun bindData(){

        inventryFragmentBinding.btnSearch.setOnClickListener {
            fillterBySearch(inventryFragmentBinding.edtSearch.text.toString())
        }

        inventryFragmentBinding.btnClearFilter.setOnClickListener {
            inventryFragmentBinding.edtSearch.text.clear()
            fillterBySearch(inventryFragmentBinding.edtSearch.text.toString())
        }
        db = AppDatabase.getDatabase(this.requireContext())
        inventoryAdapter = activity?.let {
            InventoryAdapter(it,getInventoryList()){ type, position ->

                if(type == 0){
                    (activity as MainActivity?)!!.changePosition(1)

                }else{
                    showSearchBarcodeDialog(requireActivity(),getInventoryList()[position!!]){

                    }
                }

            }
        }!!
        inventryFragmentBinding.rcyInventory.adapter = inventoryAdapter
    }

    private fun fillterBySearch(searchText : String){
        if(inventoryListTemp.isNotEmpty()){
            inventoryList.clear()
            inventoryList.addAll(inventoryListTemp.filter { it.product_name!!.toLowerCase().contains(searchText.toLowerCase())})
            inventryFragmentBinding.rcyInventory.adapter?.notifyDataSetChanged()
        }
    }

    private fun getInventoryList(): MutableList<ProductModel> {
        inventryFragmentBinding.inventoryViewModel?.getInventoryList(db)?.observe(viewLifecycleOwner, Observer { modelList ->
            inventoryList.clear()
            inventoryListTemp.clear()
            if(modelList.isNotEmpty()){
                inventoryList.addAll(modelList)
                inventoryListTemp.addAll(inventoryList)
            }
         })
        return inventoryList
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[InventryViewModel::class.java]
        // TODO: Use the ViewModel
    }

    @SuppressLint("ClickableViewAccessibility")
    fun showSearchBarcodeDialog(
        context: Activity,
        inventoryList:ProductModel,
        clickListener: (String?) -> Unit
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_edit_inventory_dialog)
        val itemInventory = Item_inventoryModel()
        val time = System.currentTimeMillis()
        lateinit var selectedExpDate: Date

        val btnClose: TextView = dialog.findViewById(R.id.txtTitle)
        val edtStock: EditText = dialog.findViewById(R.id.edt_stock)
        val edtPrice: EditText = dialog.findViewById(R.id.edt_price)
        val edtEnterQty: EditText = dialog.findViewById(R.id.edt_enter_qty)
        val edtEnterPrice: EditText = dialog.findViewById(R.id.edt_enter_price)
        val edtLotNo: EditText = dialog.findViewById(R.id.edt_lot_no)
        val edtExpDate: EditText = dialog.findViewById(R.id.edt_exp_date)
        val dialogButton: Button = dialog.findViewById(R.id.btnDone) as Button

        edtExpDate.setOnClickListener {
            Utils.showDatePickerFeature(context){
                if (it != null){
                    selectedExpDate = it
                    edtExpDate.setText(Utils.toDateString(selectedExpDate))
                }
            }
        }

        edtStock.setText("${inventoryList.restQty}")
        edtPrice.setText("${inventoryList.retail_cost}")

        btnClose.setOnTouchListener(View.OnTouchListener { _, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action != MotionEvent.ACTION_UP) {
                if (event.rawX >= btnClose.right - btnClose.compoundDrawables
                        .get(DRAWABLE_RIGHT).bounds.width()
                ) {
                    dialog.dismiss()
                    Utils.hideKeyboard(context)
                    clickListener("")
                    return@OnTouchListener true
                }
            }
            false
        })

        dialogButton.setOnClickListener(View.OnClickListener {

            itemInventory.created_at = time
            itemInventory.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
            itemInventory.product_id = inventoryList.id

            if(edtEnterQty.text.toString().trim().isNotEmpty()) {
                itemInventory.quantity = edtEnterQty.text.toString().trim().toInt()
                itemInventory.rest_qty = edtEnterQty.text.toString().trim().toInt()
            }

            if(edtExpDate.text.toString().trim().isNotEmpty())
                itemInventory.expire_date = selectedExpDate.time

            if(edtLotNo.text.toString().trim().isNotEmpty())
                itemInventory.lot_no = edtLotNo.text.toString().trim()

                db.itemInventery().insert(itemInventory)
                dialog.dismiss()
                Utils.hideKeyboard(context)
                clickListener("")
                getInventoryList()
                inventoryAdapter.notifyDataSetChanged()

        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }

}