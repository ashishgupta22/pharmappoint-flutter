package com.pharmapoint.ui_fragment.home_fragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.databinding.LogFragmentBinding
import com.pharmapoint.viewmodel.AppointmentViewModel
import com.pharmapoint.viewmodel.LogViewModel

class LogFragment : Fragment() {

    companion object {
        fun newInstance() = LogFragment()
    }

    private lateinit var viewModel: LogViewModel
    private lateinit var logFragmentBinding: LogFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        logFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.log_fragment, container, false)
        logFragmentBinding.logViewModel = context?.let { LogViewModel(it) }
        return logFragmentBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(LogViewModel::class.java)
        // TODO: Use the ViewModel
    }

}