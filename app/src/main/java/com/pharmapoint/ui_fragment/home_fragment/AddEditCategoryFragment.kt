package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.CategoryAdapter
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddEditCategoryFragmentBinding
import com.pharmapoint.model.CategoryModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.AppointmentViewModel
import java.util.*

class AddEditCategoryFragment : Fragment() {

    companion object {
        fun newInstance() = AddEditCategoryFragment()
    }

    private var parentId: Int? = null
    private var exitsID: Int? = 0
    private lateinit var statusArray: Array<String>
    private var customSpinAdapter: CustomeSpinnerAdapter? = null
    var categoryList = mutableListOf<CategoryModel>()
    var categoryListTemp = mutableListOf<CategoryModel>()
    private lateinit var db: AppDatabase
    private lateinit var categoryAdapter: CategoryAdapter
    private lateinit var categoryFragmentBinding: AddEditCategoryFragmentBinding
    var categoryModel = CategoryModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        categoryFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.add_edit_category_fragment, container, false)
        categoryFragmentBinding.categoryViewModel = context?.let { AppointmentViewModel(it) }
        bindData();
        return  categoryFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()
        clearWidgets()
    }

    private fun bindData() {
         db = AppDatabase.getDatabase(this.requireActivity())
        getCategoryList()

        categoryFragmentBinding.btnSearch.setOnClickListener {
            fillterBySearch(categoryFragmentBinding.edtSearch.text.toString());
        }

        categoryFragmentBinding.btnClearFilter.setOnClickListener {
            fillterBySearch("")
        }

        bindSpinner();
        categoryFragmentBinding.btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(categoryFragmentBinding.edtCatTitle.text)) {
                categoryFragmentBinding.edtCatTitle.error =
                    requireView().context.getString(R.string.error_msg)
                categoryFragmentBinding.edtCatTitle.requestFocus()
            } else {
                val time = System.currentTimeMillis()
                var status : Int = 0
                if(categoryFragmentBinding.spnStatus.selectedItemPosition == 0){
                    status = 1
                }else{
                    status = 0
                }
                if (exitsID!! > 0) {
                    categoryModel.updated_at = time
                    categoryModel.comment = categoryFragmentBinding.edtComment.text.toString()
                    categoryModel.status = status
                    categoryModel.title =
                        categoryFragmentBinding.edtCatTitle.text.toString()
                    db.categoryDao().update(categoryModel)
                } else {
                    categoryModel.created_at = time
                    categoryModel.updated_at = time
                    categoryModel.comment = categoryFragmentBinding.edtComment.text.toString()
                    categoryModel.status = status
                    categoryModel.title = categoryFragmentBinding.edtCatTitle.text.toString()
                    categoryModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.categoryDao().insert(categoryModel)
                }
                clearWidgets()
                getCategoryList()
            }
        }

    }
    private fun fillterBySearch(searchText : String){
        if(categoryListTemp.isNotEmpty()){
            categoryList.clear()
            categoryList.addAll(categoryListTemp.filter {it.title!!.toLowerCase().contains(searchText.toLowerCase())})
            categoryAdapter.notifyDataSetChanged()
        }
    }

    private fun clearWidgets() {
        categoryFragmentBinding.edtCatTitle.setText("")
        categoryFragmentBinding.edtComment.setText("")
        categoryFragmentBinding.spnStatus.setSelection(0)
        exitsID = 0
    }

    private fun bindSpinner() {
        statusArray = arrayOf("Active", "Inactive")
         customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }
        categoryFragmentBinding.spnStatus.adapter = customSpinAdapter
    }
    private fun getCategoryList() {
        categoryList.clear()
        categoryListTemp.clear()
        categoryList.addAll(db.categoryDao().getCategoryList())
        categoryListTemp.addAll(categoryList)
        Log.d("listData", "test${categoryList.size} ${categoryListTemp.size}")
            categoryAdapter = activity?.let {
                CategoryAdapter(it,categoryList){ type, position ->
                    //type 1  = delete type 0 = edit
                    if(type == 1){
                        Utils.showConfirmationDialog(requireActivity(),"Alert","Are You Sure You Want to Delete the Category","Yes","No"){ it1 ->
                            if(it1!!){
                                categoryList[position!!].id?.let { it1 -> db.categoryDao().deleteCatItemByID(it1,
                                    Date().time
                                ) }
                                getCategoryList()
                            }
                        }
                    }else{
                        categoryFragmentBinding.edtCatTitle.setText(categoryList[position!!].title)
                        categoryFragmentBinding.edtComment.setText(categoryList[position].comment)
                        categoryFragmentBinding.spnStatus.setSelection(0)
                        exitsID = categoryList[position].id
                        categoryModel = categoryList[position]
                        if(categoryList[position].status == 1){
                            categoryFragmentBinding.spnStatus.setSelection(0)
                        }else categoryFragmentBinding.spnStatus.setSelection(1)
                    }
                }
            }!!
            categoryFragmentBinding.rcyCategory.adapter = categoryAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}