package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.pharmapoint.R
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.adapter.SalesTransactionAdapter
import com.pharmapoint.databinding.SalesTransactionHistoryFragmentBinding
import com.pharmapoint.viewmodel.SalesTransactionViewModel

class SalesTransactionHistoryFragment : Fragment() {

    companion object {
        fun newInstance() = SalesTransactionHistoryFragment()
    }

    private lateinit var statusArray: Array<String>
    private lateinit var customSpinAdapter: CustomeSpinnerAdapter
    private lateinit var salesTransactionHistoryFragmentBinding: SalesTransactionHistoryFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        salesTransactionHistoryFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.sales_transaction_history_fragment, container, false)
        salesTransactionHistoryFragmentBinding.salesTransctionViewModel = context?.let { SalesTransactionViewModel(it) }
        bindSpinner();
        getTransactions()
        return salesTransactionHistoryFragmentBinding.root
    }

    private fun getTransactions() {
        salesTransactionHistoryFragmentBinding.salesTransctionViewModel!!.getTransactions(requireActivity())
        salesTransactionHistoryFragmentBinding.salesTransctionViewModel!!.listData.observe(viewLifecycleOwner,{
            if(it != null && it.isNotEmpty()){
                salesTransactionHistoryFragmentBinding.rcySalesList.adapter = SalesTransactionAdapter(it)
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AddProcutViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun bindSpinner() {

        statusArray = arrayOf("Shyam", "Ram")
        customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }!!
        salesTransactionHistoryFragmentBinding.spnSelectCustomer.adapter = customSpinAdapter

        statusArray = arrayOf("Online", "Cash")
        customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }!!
        salesTransactionHistoryFragmentBinding.spnTranType.adapter = customSpinAdapter

        statusArray = arrayOf("Counter Name", "Counter Name2")
        customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }!!
        salesTransactionHistoryFragmentBinding.spnCounterName.adapter = customSpinAdapter

        statusArray = arrayOf("Online", "Cash")
        customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }!!
        salesTransactionHistoryFragmentBinding.spnPayType.adapter = customSpinAdapter

    }

}