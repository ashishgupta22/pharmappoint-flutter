package com.pharmapoint.ui_fragment.home_fragment

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.AppConfig
import com.pharmapoint.R
import com.pharmapoint.activity.MainActivity
import com.pharmapoint.adapter.ShopFrontAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.ShopFrontFragmentBinding
import com.pharmapoint.model.OrderDetailsModel
import com.pharmapoint.model.OrderModel
import com.pharmapoint.model.ProductModel
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.ShopFrontViewModel
import kotlinx.android.synthetic.main.add_procut_fragment.*
import kotlinx.android.synthetic.main.shop_front_fragment.*
import rx.subjects.PublishSubject
import java.util.*
import java.util.concurrent.TimeUnit

class ShopFrontFragment : Fragment() {

    companion object {
        fun newInstance() = ShopFrontFragment()
    }

    private lateinit var productModel: ProductModel
    private var invoiceNo: Int = 0
    var orderRefNo = ""
    private lateinit var productAdapter: ShopFrontAdapter
    private lateinit var viewModel: ShopFrontViewModel
    private lateinit var shopFrontBinding : ShopFrontFragmentBinding
    val productList = mutableListOf<ProductModel>()
    private lateinit var db: AppDatabase
    var vatTotal: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        shopFrontBinding = DataBindingUtil.inflate(inflater, R.layout.shop_front_fragment, container, false)
        bindData();
        return  shopFrontBinding.root
       // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun bindData() {
        db = AppDatabase.getDatabase(this.requireActivity())
        handleBarcodeEditBox()
        getInvoicNo()
        productAdapter = ShopFrontAdapter(requireActivity(),productList){ type, position, isChecked ->
            // 0 seleted or not
            if(type == 0) {
                if (isChecked != null) {
                    productList[position!!].isSelected = isChecked
                }
            }
        }
        shopFrontBinding.rcyShopfront.adapter = productAdapter

        shopFrontBinding.edtSearch.setOnTouchListener(View.OnTouchListener { _, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= shopFrontBinding.edtSearch.right - shopFrontBinding.edtSearch.compoundDrawables
                        .get(DRAWABLE_RIGHT).bounds.width()
                ) {
                    Utils.showSearchBarcodeDialog(
                        requireActivity(),
                        "Submit"
                    ) { barcode ->
                        if (barcode != null && barcode.isNotEmpty()) {
                            getProduct(barcode)
                        }
                    }
                    return@OnTouchListener true
                }
            }
            false
        })

        shopFrontBinding.btnDelete.setOnClickListener {
            if (productList.isNotEmpty()) {
                productList.removeAll {
                    it.isSelected
                }
                productAdapter.notifyDataSetChanged()
                calculateTotal()
            }
        }
        shopFrontBinding.btnCancel.setOnClickListener {
            if (productList.isNotEmpty()) {
                productList.clear()
                productAdapter.notifyDataSetChanged()
                calculateTotal()
            }
        }
        shopFrontBinding.btnPayment.setOnClickListener {
            if (productList.isNotEmpty()) {
                val orderIdExits = db.orderDao().getInvoiceExite(invoiceNo).toInt()
                if(orderIdExits >0){
                    getInvoicNo()
                }
                Utils.showPaymentDialog(requireActivity(),txtTotalAmt.text.toString().trim()){
                        isSubmit,paytType,discountType,discountValue,enterAmount,clName,remark ->
                    if(isSubmit) {
                        val totalAmount = txtTotalAmt.text.toString().trim()
                            .substring(1, txtTotalAmt.text.toString().trim().length).toDouble()
                        var changeAmount = 0.00
                        var discountAmount = 0.00
                        // calculate change amount
                        if (enterAmount >= totalAmount) {
                            val value = Utils.decimalFormat(enterAmount - totalAmount)
                            changeAmount = value
                        } else {
                            changeAmount = 0.0
                        }
                        // calculate discount amount
                        if (discountValue > 0) {
                            if (discountType == "%")
                                discountAmount = totalAmount.times(discountValue) / 100.toDouble()
                            else
                                discountAmount = discountValue
                            if (discountAmount <= totalAmount) {
                                discountAmount =
                                    Utils.decimalFormat(discountAmount)
                            } else {
                                discountAmount = 0.0
                            }
                        }
                        val time = System.currentTimeMillis()
                        val orderModel = OrderModel()
                        orderModel.invoiceNo = invoiceNo
                        orderModel.paidAmount = (totalAmount - discountAmount)
                        orderModel.order_date = time
                        orderModel.invoiceDiscount = discountValue
                        orderModel.clintName = clName.trim()
                        orderModel.discountAmount = discountAmount
                        orderModel.netAmount =
                            txtTotalAmt.text.toString().trim().removeRange(0, 1).toDouble()
                        orderModel.itemQty = productList.size
                        orderModel.referenceInvoice = invoiceNo
                        orderModel.counterNo = 1
                        orderModel.userId =
                            PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                        orderModel.series = "g"
                        orderModel.payStatus = "p"
                        orderModel.payMethod = paytType
                        orderModel.remark = remark.trim()
                        orderModel.created_at = time
                        orderModel.change_amount = changeAmount
                        db.orderDao().insert(orderModel)

                        productList.forEach {
                            val orderDeatils = OrderDetailsModel()
                            orderDeatils.created_at = time
                            orderDeatils.discount = it.discount
                            orderDeatils.discountAmount = it.discountAmouont
                            orderDeatils.invoiceNo = invoiceNo
                            orderDeatils.price = it.retail_cost
                            orderDeatils.salePrice = it.retail_cost
                            orderDeatils.qty = it.seletedQTY
                            orderDeatils.productId = it.id
                            orderDeatils.vat = it.vat
                            db.orderDetailsDao().insert(orderDeatils)
                        }
                        updateStock(productList, time);
                        clearDashboard()
                        Utils.showToast(requireContext(), "Payment is completed")
                    }
                }
//                db.orderDetailsDao().insert()
            }else{
                Utils.showToast(requireContext(),"Select minimum one item")
            }
        }
    }

    private fun clearDashboard() {

        productList.clear()
        productAdapter.notifyDataSetChanged()
        vatTotal = 0.0
        txtTotalAmt.text = "£0"
        txt_vat_amount.text = "${Utils.decimalFormat(vatTotal)}"
        txtItems.text = "${productList.size}"
    }

    private fun updateStock(productList: MutableList<ProductModel>, time: Long) {
        val date = Date()
        date.hours = 0
        date.minutes = 0
        date.seconds = 0
        if(productList.isNotEmpty()){
            productList.forEach{
                val inventryList = it.id?.let { it1 -> db.itemInventery().getItemList(it1,date.time) }
                if(inventryList!!.isNotEmpty()){
                    if(it.seletedQTY <= inventryList[0].rest_qty!!){
                        inventryList[0].rest_qty = inventryList[0].rest_qty!!.minus(it.seletedQTY)
                        db.itemInventery().update(inventryList[0])
                    }else{
                        var itemCount = 0
                        var leftItem = it.seletedQTY
                        inventryList.forEach{ it1->
                            if(itemCount < it.seletedQTY && leftItem > 0){
                                if(leftItem <= it1.rest_qty!!){
                                    it1.rest_qty = it1.rest_qty!!.minus(leftItem)
                                    itemCount = leftItem
                                    leftItem = 0
                                }else{
                                    leftItem  = leftItem.minus(it1.rest_qty!!)
                                    it1.rest_qty = it1.rest_qty!!.minus(it1.rest_qty!!)
                                }
                                db.itemInventery().update(it1)
                            }
                        }
                    }
                }
            }
        }

    }

    private fun getInvoicNo() {
        invoiceNo = (db.orderDao().getLastId() + Constans.startOrdrderID)
        shopFrontBinding.txtInvoiceNo.setText(invoiceNo.toString())
        if(invoiceNo <= 0){
            invoiceNo = 1
        }
        orderRefNo = Constans.startOrderRefNo + ( "%05d".format(invoiceNo))
    }

    private fun handleBarcodeEditBox() {
        shopFrontBinding.edtSearch.setInputType(InputType.TYPE_NULL)
        Handler().postDelayed(Runnable {
            shopFrontBinding.edtSearch.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN,
                0F,
                0F, 0));
            shopFrontBinding.edtSearch.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,
                0F,
                0F, 0));

        }, 100)

        val subject = PublishSubject.create<String>()
        subject.debounce(500, TimeUnit.MILLISECONDS)
            .onBackpressureLatest()
            .subscribe {
                requireActivity().runOnUiThread(Runnable {
                    val barcode = shopFrontBinding.edtSearch.text.toString().trim()
                    if(barcode.isNotEmpty()){
                        getProduct(barcode)
                    }
                })
            }
        shopFrontBinding.edtSearch.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                subject.onNext(p0.toString());
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if(shopFrontBinding.edtSearch != null){
            shopFrontBinding.edtSearch.requestFocus()
        }
    }
    private fun getProduct(barCode: String) {
        val productId = db.orderBarcodeDao().getOrderIdByBarcode(barCode.trim())
        if(productId >0 ) {
            productModel = db.productDao().getActiveProduct(productId)
            if (productModel?.product_name != null && productModel?.status == 1) {
                val isExit = productList.indexOfFirst { it.id == productModel.id }
                if (isExit >= 0) {
                    if(Utils.checkQTYExits(productModel.id!!,productList[isExit].seletedQTY + 1,db))
                        productList[isExit].seletedQTY = productList[isExit].seletedQTY + 1
                    else
                        Utils.showToast(requireContext(),"This product is out of Stock")
                } else {
                    Utils.showItemDialog(
                        requireActivity(),
                        barCode,
                        productModel
                    ) { barCode, productModel ->
                        if (barCode != null && barCode.isNotEmpty()) {
                            productModel.isSelected = false
                            if (productList.isNotEmpty()) {
                                productList.add(productModel)
                            } else
                                productList.add(productModel)
                            reCalculate()
                        }
                    }
                }
            } else if (productModel?.product_name != null && productModel?.status == 0) {
                // show popup for deactivated item
                Utils.showConfirmationDialog(requireActivity(),"Product Deactivated","Product deactivated by admin. Please active product","Okay",""){ it ->
                    if(it!!){

                    }
                }
            }
            reCalculate()
        }else{
            // show popup for add new item
            shopFrontBinding.edtSearch.setText("")
            Utils.showConfirmationDialog(requireActivity(),"Product Not Fount","$barCode \nProduct not found. Please add product","Yes","No"){ it ->
                if(it!!){
                    AppConfig.lastScanedBarcod = barCode
                    (activity as MainActivity?)!!.changePosition(1)
                }else{
                    AppConfig.lastScanedBarcod = ""
                }
            }
        }
    }

    private fun reCalculate() {
        if (productList.isNotEmpty() && productList.size > 0)
            productAdapter.notifyDataSetChanged()
        calculateTotal();
        shopFrontBinding.edtSearch.requestFocus()
        shopFrontBinding.edtSearch.setText("")
    }

    private fun calculateTotal() {
        var total:Double = 0.00
        if(productList.isNotEmpty()){
            productList.forEach {
                total += it.retail_cost?.times(it.seletedQTY!!)!!
            }
        }else{
            total = 0.00
        }
        calcuateVat();
        txtTotalAmt.text = "£${Utils.decimalFormat(total.plus(vatTotal))}"
        //Total Items
        txtItems.text = "${productList.size}"
    }

    private fun calcuateVat() {
        vatTotal = 0.00
        var total:Double = 0.00
        if(productList.isNotEmpty()){
            productList.forEach {
                total = it.vat?.let { it1 ->
                    it.retail_cost?.let { it2 ->
                        Utils.calculateVAT(
                            it2, it.seletedQTY,
                            it1
                        )
                    }
                }!!
                vatTotal += total
            }
        }else{
            total = 0.00
            vatTotal = 0.00
        }
        txt_vat_amount.text = "${Utils.decimalFormat(vatTotal)}"
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ShopFrontViewModel::class.java)
    }

}