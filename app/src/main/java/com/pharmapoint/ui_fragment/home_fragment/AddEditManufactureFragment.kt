package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.*
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddEditManufactureFragmentBinding
import com.pharmapoint.model.ManufactureModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.AppointmentViewModel
import java.util.*

class AddEditManufactureFragment : Fragment() {

    companion object {
        fun newInstance() = AddEditManufactureFragment()
    }

    private var parentId: Int? = null
    private var exitsID: Int? = 0
    private lateinit var statusArray: Array<String>
    private var customSpinAdapter: CustomeSpinnerAdapter? = null
    var manufactureList = mutableListOf<ManufactureModel>()
    var manufactureListTemp = mutableListOf<ManufactureModel>()
    private lateinit var db: AppDatabase
    private lateinit var manufactureAdapter: ManufactureAdapter
    private lateinit var manufactureFragmentBinding: AddEditManufactureFragmentBinding
    var manufactureModel = ManufactureModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        manufactureFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.add_edit_manufacture_fragment, container, false)
        manufactureFragmentBinding.appointmentViewModel = context?.let { AppointmentViewModel(it) }
        bindData();
        return  manufactureFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    private fun bindData() {
         db = AppDatabase.getDatabase(this.requireActivity())
        getManufactureList(null)

        manufactureFragmentBinding.btnSearch.setOnClickListener {
            fillterBySearch(manufactureFragmentBinding.edtSearch.text.toString());
        }

        manufactureFragmentBinding.btnClearFilter.setOnClickListener {
            fillterBySearch("")
        }

        bindSpinner();
        manufactureFragmentBinding.btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(manufactureFragmentBinding.edtCatTitle.text)) {
                manufactureFragmentBinding.edtCatTitle.error =
                    requireView().context.getString(R.string.error_msg)
                manufactureFragmentBinding.edtCatTitle.requestFocus()
            } else {
                val time = System.currentTimeMillis()
                var status : Int = 0
                if(manufactureFragmentBinding.spnStatus.selectedItemPosition == 0){
                    status = 1
                }else{
                    status = 0
                }
                if (exitsID!! > 0) {
                    manufactureModel.updated_at = time
                    manufactureModel.comment = manufactureFragmentBinding.edtComment.text.toString()
                    manufactureModel.status = status
                    manufactureModel.title =
                        manufactureFragmentBinding.edtCatTitle.text.toString()
                    db.manufactureDao().update(manufactureModel)
                } else {
                    manufactureModel.created_at = time
                    manufactureModel.updated_at = time
                    manufactureModel.comment = manufactureFragmentBinding.edtComment.text.toString()
                    manufactureModel.status = status
                    manufactureModel.title = manufactureFragmentBinding.edtCatTitle.text.toString()
                    manufactureModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.manufactureDao().insert(manufactureModel)
                }
                clearWeidgts()
                getManufactureList(null)
            }
        }

    }
    private fun fillterBySearch(searchText : String){
        if(manufactureListTemp.isNotEmpty()){
            manufactureList.clear()
            manufactureList.addAll(manufactureListTemp.filter {it.title!!.toLowerCase().contains(searchText.toLowerCase())})
            manufactureAdapter.notifyDataSetChanged()
        }
    }

    private fun clearWeidgts() {
        manufactureFragmentBinding.edtCatTitle.setText("")
        manufactureFragmentBinding.edtComment.setText("")
        manufactureFragmentBinding.spnStatus.setSelection(0)
        exitsID = 0
    }

    private fun bindSpinner() {
        statusArray = arrayOf("Active", "Inactive")
         customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }
        manufactureFragmentBinding.spnStatus.adapter = customSpinAdapter
    }

    private fun getManufactureList(parentId1: Int?) {
        manufactureList.clear()
        manufactureListTemp.clear()
        manufactureList.addAll(db.manufactureDao().getManufactureList())
        manufactureListTemp.addAll(manufactureList)
        Log.d("listData", "test${manufactureList.size} ${manufactureListTemp.size}")
//        if(this::manufactureAdapter.isInitialized)
//            manufactureAdapter.notifyDataSetChanged()
//        else{
            manufactureAdapter = activity?.let {
                ManufactureAdapter(it,manufactureList){ type, position ->
                    //type 1  = delete type 0 = edit
                    if(type == 1){
                        Utils.showConfirmationDialog(requireActivity(),"Alert","Are You Sure You Want to Delete this Item","Yes","No"){ it1 ->
                            if(it1!!){
                                manufactureList[position!!].id?.let { it1 -> db.manufactureDao().deleteManufactureItemByID(it1,
                                    Date().time
                                ) }
                                getManufactureList(null)
                            }
                        }
                    }else{
                        manufactureFragmentBinding.edtCatTitle.setText(manufactureList[position!!].title)
                        manufactureFragmentBinding.edtComment.setText(manufactureList[position!!].comment)
                        manufactureFragmentBinding.spnStatus.setSelection(0)
                        exitsID = manufactureList[position!!].id
                        manufactureModel = manufactureList[position!!]
                        if(manufactureList[position!!].status == 1){
                            manufactureFragmentBinding.spnStatus.setSelection(0)
                        }else manufactureFragmentBinding.spnStatus.setSelection(1)
                    }
                }
            }!!
            manufactureFragmentBinding.rcyShopfront.adapter = manufactureAdapter
//        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}