package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.adapter.CustomeSpinnerAdapterArrayList
import com.pharmapoint.adapter.SubCategoryAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddEditSubCategoryFragmentBinding
import com.pharmapoint.model.CategoryModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.AppointmentViewModel
import java.util.*
import kotlin.collections.ArrayList

class AddEditSubCategoryFragment : Fragment() {

    companion object {
        fun newInstance() = AddEditSubCategoryFragment()
    }

    private var listAllCategory = mutableListOf<CategoryModel>()
    private var parentId: Int? = null
    private var exitsID: Int? = 0
    private lateinit var statusArray: Array<String>
    private var customSpinAdapter: CustomeSpinnerAdapter? = null
    var subCategoryList = mutableListOf<CategoryModel>()
    var subCategoryListTemp = mutableListOf<CategoryModel>()
    var listParent = mutableListOf<CategoryModel>()
    private lateinit var db: AppDatabase
    private lateinit var subCategoryAdapter: SubCategoryAdapter
    private lateinit var subCategoryFragmentBinding: AddEditSubCategoryFragmentBinding
    var categoryModel = CategoryModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        subCategoryFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.add_edit_sub_category_fragment, container, false)
        subCategoryFragmentBinding.appointmentViewModel = context?.let { AppointmentViewModel(it) }
        bindData();
        return  subCategoryFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    private fun bindData() {
         db = AppDatabase.getDatabase(this.requireActivity())

        subCategoryFragmentBinding.llCategory.visibility = View.VISIBLE
        subCategoryFragmentBinding.txtCatTitle.text = getString(R.string.sub_category_title)
        subCategoryFragmentBinding.edtCatTitle.setHint(getString(R.string._sub_category_title))

        getCategoryListParent();
        getSubCategoryList(parentId)
        bindSpinner();
        subCategoryFragmentBinding.btnSearch.setOnClickListener{
            fillterBySearch(subCategoryFragmentBinding.edtSearch.text.toString())
        }

        subCategoryFragmentBinding.btnClearFilter.setOnClickListener {
            fillterBySearch("")
        }
        subCategoryFragmentBinding.btnSubmit.setOnClickListener {
            if (TextUtils.isEmpty(subCategoryFragmentBinding.edtCatTitle.text)) {
                subCategoryFragmentBinding.edtCatTitle.error =
                    requireView().context.getString(R.string.error_msg)
                subCategoryFragmentBinding.edtCatTitle.requestFocus()
            }else if(subCategoryFragmentBinding.spnCategory.selectedItemPosition == 0){
                activity?.let { it1 -> Utils.showToast(it1,"Select Category") }
            } else {
                val time = System.currentTimeMillis()
                var status : Int = 0
                if(subCategoryFragmentBinding.spnStatus.selectedItemPosition == 0){
                    status = 1
                }else{
                    status = 0
                }
                if (exitsID!! > 0) {
                    categoryModel.updated_at = time
                    categoryModel.comment = subCategoryFragmentBinding.edtComment.text.toString()
                    categoryModel.status = status
                    categoryModel.parent_id = parentId
                    categoryModel.title =
                        subCategoryFragmentBinding.edtCatTitle.text.toString()
                    db.categoryDao().update(categoryModel)
                } else {
                    categoryModel.created_at = time
                    categoryModel.updated_at = time
                    categoryModel.comment = subCategoryFragmentBinding.edtComment.text.toString()
                    categoryModel.status = status
                    categoryModel.parent_id = parentId
                    categoryModel.title = subCategoryFragmentBinding.edtCatTitle.text.toString()
                    categoryModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.categoryDao().insert(categoryModel)
                }
                clearWeidgts()
                getSubCategoryList(null)
            }
        }

    }
    private fun fillterBySearch(searchText : String){
        if(subCategoryListTemp.isNotEmpty()){
            subCategoryList.clear()
            subCategoryList.addAll(subCategoryListTemp.filter {it.title!!.toLowerCase().contains(searchText.toLowerCase())})
            subCategoryAdapter.notifyDataSetChanged()
        }
    }
    private fun getCategoryListParent() {
        listParent.clear()
        listParent.addAll(db.categoryDao().getActiveCategoryList())
        bindCategoryAdapter();
    }

    private fun bindCategoryAdapter() {
        var arrayList = arrayListOf<String>()
        arrayList.add("Select Category")
        if(listParent.isNotEmpty()){
            listParent.forEach {
                it.title?.let { it1 -> arrayList.add(it1) }
            }
        }
        bindCateSpinner(arrayList);
    }

    private fun clearWeidgts() {
        subCategoryFragmentBinding.edtCatTitle.setText("")
        subCategoryFragmentBinding.edtComment.setText("")
        subCategoryFragmentBinding.spnStatus.setSelection(0)
        subCategoryFragmentBinding.spnCategory.setSelection(0)
        exitsID = 0
    }

    private fun bindSpinner() {
        statusArray = arrayOf("Active", "Inactive")
         customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }
        subCategoryFragmentBinding.spnStatus.adapter = customSpinAdapter
    }
    private fun bindCateSpinner(arrayList: ArrayList<String>) {
        subCategoryFragmentBinding.spnCategory.adapter =
            context?.let { CustomeSpinnerAdapterArrayList(it,  arrayList) }
        subCategoryFragmentBinding.spnCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 > 0){
                    parentId = listParent[p2-1].id
                }else{
                    parentId = null
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
    }

    private fun getSubCategoryList(parentId1: Int?) {
        subCategoryList.clear()
        subCategoryListTemp.clear()
        subCategoryList.addAll(db.categoryDao().getSubCategoryList())
        listAllCategory.addAll(db.categoryDao().getCategoryList())
        subCategoryListTemp.addAll(subCategoryList)
        Log.d("listData", "test${subCategoryList.size} ${subCategoryListTemp.size}")
//        if(this::appoinmentAdapter.isInitialized)
//        appoinmentAdapter.notifyDataSetChanged()
//        else{
            subCategoryAdapter = activity?.let {
                SubCategoryAdapter(it,subCategoryList,listAllCategory){ type, position ->
                    //type 1  = delete type 0 = edit
                    if(type == 1){
                        Utils.showConfirmationDialog(requireActivity(),"Alert","Are You Sure You Want to Delete the Sub Category","Yes","No"){ it1 ->
                            if(it1!!){
                                subCategoryList[position!!].id?.let { it1 -> db.categoryDao().deleteCatItemByID(it1,
                                    Date().time
                                ) }
                                getSubCategoryList(null)
                            }
                        }
                    }else{
                        bindEdit(position);
                    }
                }
            }!!
            subCategoryFragmentBinding.rcyShopfront.adapter =subCategoryAdapter
//        }
    }

    private fun bindEdit(position: Int?) {
        subCategoryFragmentBinding.edtCatTitle.setText(subCategoryList[position!!].title)
        subCategoryFragmentBinding.edtComment.setText(subCategoryList[position!!].comment)
        subCategoryFragmentBinding.spnStatus.setSelection(0)

        exitsID = subCategoryList[position!!].id
        categoryModel = subCategoryList[position!!]

        if(subCategoryList[position!!].status == 1){
            subCategoryFragmentBinding.spnStatus.setSelection(0)
        }else subCategoryFragmentBinding.spnStatus.setSelection(1)
        val index : Int = listParent.indexOfFirst { it.id == subCategoryList[position!!].parent_id }
        if(index > 0){
            subCategoryFragmentBinding.spnCategory.setSelection(index+1)
            parentId = subCategoryList[position!!].parent_id
        }else{
            subCategoryFragmentBinding.spnCategory.setSelection(index+1)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}