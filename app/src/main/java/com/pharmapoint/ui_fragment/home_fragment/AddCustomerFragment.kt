package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AddCustomerFragmentBinding
import com.pharmapoint.utlity.Constans
import com.pharmapoint.viewmodel.AddCustomerViewModel

class AddCustomerFragment : Fragment() {
    companion object {
        fun newInstance() = AddCustomerFragment()
    }

    private lateinit var addCustomerFragmentBinding: AddCustomerFragmentBinding
    private lateinit var statusArray: Array<String>
    private lateinit var customSpinAdapter: CustomeSpinnerAdapter
    private lateinit var db: AppDatabase

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        addCustomerFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.add_customer_fragment, container, false)
        db = AppDatabase.getDatabase(this.requireActivity())
        addCustomerFragmentBinding.addCustomerViewModel = context?.let { AddCustomerViewModel(it,db) }
        bindSpinner()
        return addCustomerFragmentBinding.root
    }

    override fun onResume() {
        super.onResume()
        clearWidget()
    }
    private fun clearWidget(){
        addCustomerFragmentBinding.spnTitle.setSelection(0)
        addCustomerFragmentBinding.edtfirstname.text.clear()
        addCustomerFragmentBinding.edtsurname.text.clear()
        addCustomerFragmentBinding.edtEmail.text.clear()
        addCustomerFragmentBinding.edtPhone.text.clear()
        addCustomerFragmentBinding. edtDob.text.clear()
        addCustomerFragmentBinding.edtCity.text.clear()
        addCustomerFragmentBinding.edtCountry.text.clear()
        addCustomerFragmentBinding.edtPostCode.text.clear()
        addCustomerFragmentBinding.edtAddress.text.clear()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }
    private fun bindSpinner() {
        addCustomerFragmentBinding.btnCancel.setOnClickListener {
            clearWidget()
        }
        statusArray = Constans.title
        customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }!!
        addCustomerFragmentBinding.spnTitle.adapter = customSpinAdapter
    }
}