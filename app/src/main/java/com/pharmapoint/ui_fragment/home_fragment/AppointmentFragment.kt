package com.pharmapoint.ui_fragment.home_fragment

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.activity.MainActivity
import com.pharmapoint.adapter.AppointmentAdapter
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.adapter.CustomeSpinnerAdapterArrayListAdd
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.databinding.AppointmentsFragmentBinding
import com.pharmapoint.utlity.Utils
import com.pharmapoint.viewmodel.AppointmentViewModel
import java.util.*

import com.pharmapoint.adapter.SearchbleArrayAdapter
import com.pharmapoint.model.*
import com.pharmapoint.utlity.PreferenceUtils
import kotlinx.android.synthetic.main.appointments_fragment.*


class AppointmentFragment : Fragment() {

    companion object {
        fun newInstance() = AppointmentFragment()
    }

    private lateinit var customerList: List<CustomerModel>
    private var exitsID: Int? = 0
    private lateinit var statusArray: Array<String>
    private var customSpinAdapter: CustomeSpinnerAdapter? = null
    private lateinit var selectedDate: Date
    var appointmentList = mutableListOf<AppoinmentModel>()
    var appointmentListTemp = mutableListOf<AppoinmentModel>()
    private lateinit var db: AppDatabase
    private lateinit var appoinmentAdapter: AppointmentAdapter
    private lateinit var viewModel: AppointmentViewModel
    private lateinit var appointmentsFragmentBinding: AppointmentsFragmentBinding
    var appoinmentModel = AppoinmentModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        appointmentsFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.appointments_fragment, container, false)
        appointmentsFragmentBinding.appointmentViewModel = context?.let { AppointmentViewModel(it) }
        bindData();
        return  appointmentsFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    private fun bindData() {
        db = AppDatabase.getDatabase(this.requireActivity())
        getAppointmentList()
        bindSpinner()
        bindCustomer()
        bindPrice(false)
        appointmentsFragmentBinding.edtAppointmentDate.setOnClickListener {
            activity?.let { it1 ->
                Utils.showDateTimePicker(it1){
                    if (it != null) {
                        selectedDate = it
                    }
                    appointmentsFragmentBinding.edtAppointmentDate.setText(Utils.toDateTimeString(selectedDate))
                }

            }
        }

        appointmentsFragmentBinding.btnSearch.setOnClickListener {
            fillterBySearch(appointmentsFragmentBinding.edtSearch.text.toString().trim())
        }

        appointmentsFragmentBinding.btnClearFilter.setOnClickListener {
            fillterBySearch("")
        }

        appointmentsFragmentBinding.btnSubmit.setOnClickListener {
//            if (TextUtils.isEmpty(appointmentsFragmentBinding.edtAppointmentName.text)) {
//                appointmentsFragmentBinding.edtAppointmentName.error =
//                    requireView().context.getString(R.string.error_msg)
//                appointmentsFragmentBinding.edtAppointmentName.requestFocus()
            Utils.hideKeyboard(requireActivity())
           if (appointmentsFragmentBinding.spnCustomerName.selectedItem.toString().trim() == "Select Customer") {
               Toast.makeText(context, "Select Customer", Toast.LENGTH_SHORT).show()
            } else if (appointmentsFragmentBinding.spnService.selectedItemPosition == 0) {
               Toast.makeText(context, "Select Service", Toast.LENGTH_SHORT).show()
            } else if (TextUtils.isEmpty(appointmentsFragmentBinding.edtAppointmentDate.text)) {
               Toast.makeText(context, "Choose Date & Time", Toast.LENGTH_SHORT).show()
                appointmentsFragmentBinding.edtAppointmentDate.requestFocus()
            } else {
               appointmentsFragmentBinding.edtAppointmentDate.error = null
                if (this::selectedDate.isInitialized) {
                    val time = System.currentTimeMillis()
                    if (exitsID!! > 0) {
                        appoinmentModel.appointment_date = selectedDate.time
                        appoinmentModel.updated_at = time
                        appoinmentModel.comment = appointmentsFragmentBinding.edtComment.text.toString()
                        appoinmentModel.status =
                            statusArray[appointmentsFragmentBinding.spnStatus.selectedItemPosition]
                        appoinmentModel.customer_id = customerList[appointmentsFragmentBinding.spnCustomerName.selectedItemPosition-1].id
                        appoinmentModel.customer_name =
                            customerList[appointmentsFragmentBinding.spnCustomerName.selectedItemPosition-1].customer_f_name+ " " + customerList[appointmentsFragmentBinding.spnCustomerName.selectedItemPosition-1].customer_l_name
                        appoinmentModel.customer_mobile =
                            customerList[appointmentsFragmentBinding.spnCustomerName.selectedItemPosition-1].customer_mobile
                        appoinmentModel.service_id = customerList[appointmentsFragmentBinding.spnService.selectedItemPosition-1].id
                        db.appinmentDao().update(appoinmentModel)
                    } else {
                        appoinmentModel.appointment_date = selectedDate.time
                        appoinmentModel.created_at = time
                        appoinmentModel.updated_at = time
                        appoinmentModel.appointment_no = 10
                        appoinmentModel.comment = appointmentsFragmentBinding.edtComment.text.toString()
                        appoinmentModel.customer_id = customerList[appointmentsFragmentBinding.spnCustomerName.selectedItemPosition-1].id
                        appoinmentModel.status =
                            statusArray[appointmentsFragmentBinding.spnStatus.selectedItemPosition]
                        appoinmentModel.customer_name =
                            customerList[appointmentsFragmentBinding.spnCustomerName.selectedItemPosition-1].customer_f_name+ " " + customerList[appointmentsFragmentBinding.spnCustomerName.selectedItemPosition-1].customer_l_name
                        appoinmentModel.customer_mobile =
                            customerList[appointmentsFragmentBinding.spnCustomerName.selectedItemPosition-1].customer_mobile
                        appoinmentModel.service_id = customerList[appointmentsFragmentBinding.spnService.selectedItemPosition-1].id
                        appoinmentModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                        db.appinmentDao().insert(appoinmentModel)
                    }
                    clearWeidgts()
                    getAppointmentList()
                } else {
                    activity?.let { it1 -> Utils.showToast(it1, "Select appointment date again") }
                }
            }
        }

    }

    private fun fillterBySearch(searchText : String){
        if(appointmentListTemp.isNotEmpty()){
            appointmentList.clear()
            appointmentList.addAll(appointmentListTemp.filter {it.customer_name!!.toLowerCase().contains(searchText.toLowerCase())})
                appoinmentAdapter.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("testtt","test12")
        bindCustomer()
        bindPrice(false)
    }
    private fun bindPrice(isSelected: Boolean) {
        appointmentsFragmentBinding.appointmentViewModel?.getServiceList(db)?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            modelList ->
            val customerListArray = arrayListOf<String>()
            customerListArray.add("Select Service")
            if(modelList.isNotEmpty()){
                modelList.forEach{
                    customerListArray.add(it.title!!)
                }
            }
            customerListArray.add("Add New Service")
            appointmentsFragmentBinding.spnService.adapter = CustomeSpinnerAdapterArrayListAdd(requireContext(),customerListArray)
            appointmentsFragmentBinding.spnService.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if(p2 > 0 && p2 < customerListArray.size - 1){
                        bindPriceDetails(modelList[p2-1])
                    }else if (p2 == customerListArray.size - 1){
                        showDialog(1)
                    }else{
                        clearCustomer()
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
            if(isSelected){
                appointmentsFragmentBinding.spnService.setSelection(customerListArray.size-2)
            }
        })

    }

    private fun bindPriceDetails(appointmentServicesModel: AppointmentServicesModel) {
        appointmentsFragmentBinding.edtPrice.setText("£" + appointmentServicesModel.price)
    }

    private fun bindCustomer() {
        val customerListArray = arrayListOf<String>()
       customerList =  db.customerDao().getCustomerList()
        customerListArray.add("Select Customer")
        if(this::customerList.isInitialized && customerList.isNotEmpty()){
            customerList.forEach{
                val name = it.customer_f_name + " " + it.customer_l_name
                customerListArray.add(name)
            }
        }
//        customerListArray.add("Add New Customer")
//        appointmentsFragmentBinding.spnCustomerName.adapter = CustomeSpinnerAdapterArrayListAdd(requireContext(),customerListArray)
//        appointmentsFragmentBinding.spnCustomerName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
//            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                if(p2 > 0 && p2 < customerListArray.size - 1){
//                    bindCustomerDetails(customerList[p2-1])
//                }else if (p2 == customerListArray.size - 1){
//                    appointmentsFragmentBinding.spnCustomerName.setSelection(0)
//                    (activity as MainActivity?)!!.changePosition(1)
//                }else{
//                    clearCustomer()
//                }
//            }
//
//            override fun onNothingSelected(p0: AdapterView<*>?) {
//            }
//
//        }

//        val adapter: ArrayAdapter<String> =
//            ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, customerListArray)
        val adapter = SearchbleArrayAdapter(requireActivity(),customerListArray)
//        val adapter = ArrayAdapter(requireContext(),R.layout.layout_custom_spinner_view,customerListArray)
        appointmentsFragmentBinding.spnCustomerName.setTitle("Select Customer")
        appointmentsFragmentBinding.spnCustomerName.setPositiveButton("Add New Customer", DialogInterface.OnClickListener { dialogInterface, i ->
            appointmentsFragmentBinding.spnCustomerName.setSelection(0)
            (activity as MainActivity?)!!.changePosition(1)
        })
        appointmentsFragmentBinding.spnCustomerName.setAdapter(adapter)
        appointmentsFragmentBinding.spnCustomerName.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapter: AdapterView<*>, v: View, p2: Int, id: Long) {
                if(p2 > 0){
                    bindCustomerDetails(customerList[p2-1])
                }else{
                    clearCustomer()
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>?) {
                // TODO Auto-generated method stub
            }
        })
    }

    private fun bindCustomerDetails(customerModel: CustomerModel) {
//        appointmentsFragmentBinding.txtCustomerDeatils.setText(customerModel.customer_f_name+ " "+customerModel.customer_l_name )
    }

    private fun clearCustomer() {
        appointmentsFragmentBinding.edtPrice.setText("£00")

    }

    private fun clearWeidgts() {
//        appointmentsFragmentBinding.edtAppointmentName.setText("")
//        appointmentsFragmentBinding.txtCustomerDeatils.setText("")
        appointmentsFragmentBinding.edtComment.setText("")
        appointmentsFragmentBinding.edtAppointmentDate.setText("")
        appointmentsFragmentBinding.spnStatus.setSelection(0)
        appointmentsFragmentBinding.spnCustomerName.setSelection(0)
        appointmentsFragmentBinding.spnService.setSelection(0)
        appointmentsFragmentBinding.edtPrice.setText("")
        exitsID = 0
    }

    private fun bindSpinner() {
        statusArray = arrayOf("Pending", "Approved", "Failed", "Cancel")
         customSpinAdapter = activity?.let { CustomeSpinnerAdapter(it, statusArray) }
        appointmentsFragmentBinding.spnStatus.adapter = customSpinAdapter


    }

    private fun getAppointmentList() {
        appointmentList.clear()
        appointmentListTemp.clear()
        appointmentList.addAll(db.appinmentDao().getAppointmentList())
        Log.d("listData", "test${appointmentList.size}")
//        if(this::appoinmentAdapter.isInitialized)
//        appoinmentAdapter.notifyDataSetChanged()
//        else{
            appoinmentAdapter = activity?.let {
                AppointmentAdapter(it,appointmentList){ type, position ->
                    //type 1  = delete type 0 = edit
                    if(type == 1){
                        Utils.showConfirmationDialog(requireActivity(),"Alert","Are You Sure You Want to Delete the Appointment","Yes","No"){ it1 ->
                            if(it1!!){
                                appointmentList[position!!].id?.let { id -> db.appinmentDao().deleteCartItemByID(id) }
                                getAppointmentList()
                            }
                        }
                    }else{
                        bindEdit(position)
                    }

                }
            }!!
            appointmentListTemp.addAll(appointmentList)
            appointmentsFragmentBinding.rcyShopfront.adapter =appoinmentAdapter
//        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun bindEdit(position: Int?) {
        val index: Int = customerList.indexOfFirst { it.id == appointmentList[position!!].customer_id }
        val statusIndex : Int = statusArray.indexOfFirst { it == appointmentList[position!!].status }
        val serviceIndex : Int =   appointmentsFragmentBinding.appointmentViewModel!!.surviceList.value!!.indexOfFirst { it.id == appointmentList[position!!].service_id }
        appointmentsFragmentBinding.spnCustomerName.setSelection(index+1)
        appointmentsFragmentBinding.edtComment.setText(appointmentList[position!!].comment)
        appointmentsFragmentBinding.edtAppointmentDate.setText(Utils.toDateTimeString(Utils.toDate(appointmentList[position].appointment_date)))
        selectedDate = Utils.toDate(appointmentList[position].appointment_date)
        appointmentsFragmentBinding.spnStatus.setSelection(statusIndex)
        appointmentsFragmentBinding.spnService.setSelection(serviceIndex+1)
        exitsID = appointmentList[position].id
        appoinmentModel = appointmentList[position]
    }
    fun showDialog(type:Int) {
        //1 add category, 2 manufacturer , 3 department, 4 brand, 5 SubCagegory
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_add_dialog)
        val edtTitle : EditText = dialog.findViewById(R.id.edtTitle) as EditText
        val edtComment : EditText = dialog.findViewById(R.id.edtDescription) as EditText
        val edtPrice : EditText = dialog.findViewById(R.id.edtPrice) as EditText
        edtPrice.visibility = View.VISIBLE
        var title = String()
        if(type == 1){
            title = "Add New Service"
        }
        val text = dialog.findViewById(R.id.txtTitle) as TextView
        text.text = title
        val dialogButton: Button = dialog.findViewById(R.id.btnSave) as Button
        val dialogButtonCancle: Button = dialog.findViewById(R.id.btnCancel) as Button
        dialogButtonCancle.setOnClickListener{
            if(type == 1){
                spnService.setSelection(0)
            }
            dialog.dismiss()
        }
        dialogButton.setOnClickListener(View.OnClickListener {
            if(edtTitle.text.toString().trim().isEmpty()){
                Utils.showToast(requireContext(),"Enter Title")
            } else if(edtPrice.text.toString().trim().isEmpty()){
                Utils.showToast(requireContext(), "Enter Price")
            }else {
                val time = System.currentTimeMillis()
                if (type == 1) {
                    val appointmentServicesModel = AppointmentServicesModel()
                    appointmentServicesModel.created_at = time
                    appointmentServicesModel.updated_at = time
                    appointmentServicesModel.comment = edtComment.text.toString().trim()
                    appointmentServicesModel.price = edtPrice.text.toString().trim()
                    appointmentServicesModel.status = 1
                    appointmentServicesModel.title = edtTitle.text.toString().trim()
                    appointmentServicesModel.user_id = PreferenceUtils(requireContext()).getInteger(PreferenceUtils.USER_ID)
                    db.appointmentServicesDao().insert(appointmentServicesModel)
                    bindPrice(true)
                }
                dialog.dismiss()
            }
        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }

}