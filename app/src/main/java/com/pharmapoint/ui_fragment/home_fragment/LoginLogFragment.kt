package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.LoginLogsAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.viewmodel.AppointmentViewModel

import com.pharmapoint.databinding.LoginLogFragmentBinding
import com.pharmapoint.generated.callback.OnClickListener
import com.pharmapoint.model.*
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.viewmodel.LoginLogViewModel


class LoginLogFragment : Fragment() {

    companion object {
        fun newInstance() = LoginLogFragment()
    }


    private lateinit var db: AppDatabase
    private lateinit var appoinmentAdapter: LoginLogsAdapter
    private lateinit var appointmentsFragmentBinding: LoginLogFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        appointmentsFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.login_log_fragment, container, false)
        appointmentsFragmentBinding.appointmentViewModel = context?.let { LoginLogViewModel(it) }
        appointmentsFragmentBinding!!.appointmentViewModel!!.bindData();
        return  appointmentsFragmentBinding.root
        // return inflater.inflate(R.layout.shop_front_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()
        Log.d("testtt","test12")
        appointmentsFragmentBinding!!.appointmentViewModel!!.getLogList()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AppointmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}