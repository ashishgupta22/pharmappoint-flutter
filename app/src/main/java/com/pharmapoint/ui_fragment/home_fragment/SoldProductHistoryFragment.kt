package com.pharmapoint.ui_fragment.home_fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.adapter.CustomeSpinnerAdapter
import com.pharmapoint.adapter.ProductSoldAdapter
import com.pharmapoint.adapter.SalesTransactionAdapter
import com.pharmapoint.databinding.ProductSoldHistoryFragmentBinding
import com.pharmapoint.viewmodel.SoldProductHistoryViewModel

class SoldProductHistoryFragment : Fragment() {

    companion object {
        fun newInstance() = SoldProductHistoryFragment()
    }
    private lateinit var soldHistoryFragmentBinding: ProductSoldHistoryFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        soldHistoryFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.product_sold_history_fragment, container, false)
        soldHistoryFragmentBinding.soldProductViewModel = context?.let { SoldProductHistoryViewModel(it) }
        getTransactions()
        return soldHistoryFragmentBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(AddProcutViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun getTransactions() {
        soldHistoryFragmentBinding.soldProductViewModel!!.getTransactions(requireActivity())
        soldHistoryFragmentBinding.soldProductViewModel!!.listData.observe(viewLifecycleOwner,{
            if(it != null && it.isNotEmpty()){
                soldHistoryFragmentBinding.rcyProductSold.adapter = ProductSoldAdapter(it)
            }
        })
    }
}