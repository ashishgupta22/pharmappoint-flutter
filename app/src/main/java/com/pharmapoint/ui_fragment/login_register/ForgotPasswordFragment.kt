package com.pharmapoint.ui_fragment.login_register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.databinding.ForgotPasswordFragmentBinding
import com.pharmapoint.viewmodel.ForgotPasswordViewModel


class ForgotPasswordFragment : Fragment() {
    // TODO: Rename and change types of parameters



    companion object {
        fun newInstance() = ForgotPasswordFragment()
    }
    private lateinit var signUpViewModel: ForgotPasswordViewModel
    private lateinit var forgotPasswordBinding: ForgotPasswordFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        forgotPasswordBinding = DataBindingUtil.inflate(inflater, R.layout.forgot_password_fragment, container, false)
        forgotPasswordBinding.forgotPasswordViewModel = context?.let { ForgotPasswordViewModel(it) }
        return forgotPasswordBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        // TODO: Use the ViewModel
    }

}
