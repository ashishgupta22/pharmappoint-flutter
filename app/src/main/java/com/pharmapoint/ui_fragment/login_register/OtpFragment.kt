package com.pharmapoint.ui_fragment.login_register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.databinding.OtpFragmentBinding
import com.pharmapoint.viewmodel.OtpViewModel


class OtpFragment : Fragment() {
    // TODO: Rename and change types of parameters



    companion object {
        fun newInstance() = OtpFragment()
    }
    private lateinit var otpViewModel: OtpViewModel
    private lateinit var OtpBinding: OtpFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        OtpBinding = DataBindingUtil.inflate(inflater, R.layout.otp_fragment, container, false)
        OtpBinding.otpViewModel = context?.let { OtpViewModel(it) }
        return OtpBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        // TODO: Use the ViewModel
    }

}
