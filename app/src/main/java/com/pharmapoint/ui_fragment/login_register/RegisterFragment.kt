package com.pharmapoint.ui_fragment.login_register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.databinding.FragmentRegisterBinding
import com.pharmapoint.viewmodel.SignUpViewModel


class RegisterFragment : Fragment() {
    // TODO: Rename and change types of parameters



    companion object {
        fun newInstance() = RegisterFragment()
    }
    private lateinit var signUpViewModel: SignUpViewModel
    private lateinit var signupFragmentBingin: FragmentRegisterBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        signupFragmentBingin = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        signupFragmentBingin.signupViewModel = context?.let { SignUpViewModel(it) }
        return signupFragmentBingin.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        // TODO: Use the ViewModel
    }

}