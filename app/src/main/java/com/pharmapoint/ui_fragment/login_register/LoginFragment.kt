package com.pharmapoint.ui_fragment.login_register

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pharmapoint.R
import com.pharmapoint.databinding.LoginFragmentBinding
import com.pharmapoint.viewmodel.LoginViewModel

class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var loginFragmentBingin : LoginFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        loginFragmentBingin = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)
        loginFragmentBingin.loginVIewModel = context?.let { LoginViewModel(it) }
        return  loginFragmentBingin.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        // TODO: Use the ViewModel
    }

}