package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.OrderDetailsModel

@Dao
interface OrderDetailsDao {

    @Insert
    fun insert(cartEntity: OrderDetailsModel)

    @Query("SELECT * from order_details_table WHERE deleted_at IS NULL ORDER BY id DESC")
    fun getOrderDetailList(): List<OrderDetailsModel>

    @Query("SELECT order_details_table.*, product_table.item_code AS item_code, sum(rest_qty) AS  restQty from order_details_table, product_table,item_inventory_table  WHERE order_details_table.productId = product_table.id AND order_details_table.productId = item_inventory_table.product_id group by order_details_table.created_at, order_details_table.productId ORDER BY id DESC")
    fun getOrderDetailListForSold(): List<OrderDetailsModel>

    @Query("SELECT order_details_table.*, product_table.item_code AS item_code, sum(rest_qty) AS  restQty from order_details_table, product_table,item_inventory_table  WHERE product_table.created_at >= :fromDate AND product_table.created_at <= :toDate AND product_table.item_code  LIKE '%' || :searchText || '%' AND order_details_table.productId = product_table.id AND order_details_table.productId = item_inventory_table.product_id group by order_details_table.created_at, order_details_table.productId ORDER BY id DESC")
    fun getOrderDetailListForSoldFilter(fromDate:Long, toDate:Long, searchText : String): List<OrderDetailsModel>

    @Query("select * from order_details_table where id in (:orderId)")
    fun getOrderDetailsById(orderId: Int): List<OrderDetailsModel>

    @Query("delete from order_details_table")
    fun delete()

    @Query("delete from order_details_table where id = :orderId")
    fun deleteCartItemByID(orderId: Int)

    @Update
    fun update(memo: OrderDetailsModel)

    @Query("SELECT * from item_inventory_table WHERE 1 ORDER BY id DESC")
    fun getOrderList(): List<OrderDetailsModel>

}