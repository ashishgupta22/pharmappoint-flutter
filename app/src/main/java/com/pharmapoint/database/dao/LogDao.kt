package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.LogModel

@Dao
interface LogDao {
    // 0 login 1= Logout 2 break time 3 break out time

    @Insert
    fun insert(cartEntity: LogModel) : Long

    @Query("SELECT log_table.*,user_table.name AS name from log_table,user_table WHERE  log_table.user_id = user_table.user_id AND log_table.user_id = :userId AND created_at BETWEEN :fromDate AND :toDate ORDER BY log_table.created_at DESC")
    fun     getLoginLogsList(userId : Int,fromDate:Long, toDate:Long): List<LogModel>

    @Query("select * from log_table where id in (:userId) AND log_time_out IS NULL")
    fun getBytID(userId: Int): LogModel

    @Query("delete from log_table")
    fun delete()

    @Update
    fun update(memo: LogModel)
}