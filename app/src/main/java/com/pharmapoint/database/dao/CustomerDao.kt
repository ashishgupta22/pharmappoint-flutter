package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.CustomerModel

@Dao
interface CustomerDao {
    @Insert
    fun insert(cartEntity: CustomerModel)

    @Query("SELECT * from customer_table WHERE deleted_at IS NULL AND status = 1 ORDER BY id ASC")
    fun getCustomerList(): List<CustomerModel>

    @Query("select * from customer_table where id in (:productId)")
    fun getCartItemById(productId: Int): List<CustomerModel>

    @Query("delete from customer_table")
    fun delete()

    @Query("delete from customer_table where id = :productId")
    fun deleteCartItemByID(productId: Int)

    @Update
    fun update(memo: CustomerModel)
}