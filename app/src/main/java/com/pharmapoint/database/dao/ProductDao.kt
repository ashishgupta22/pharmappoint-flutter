package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.ProductModel

@Dao
interface ProductDao {
    @Insert
    fun insert(cartEntity: ProductModel) : Long

    @Query("SELECT COUNT(id) from product_table where ( item_code = :itemCode) AND deleted_at IS NULL ORDER BY id ASC")
    fun getExitsProduct( itemCode : String): Int

    @Query("SELECT product_table.*,SUM(rest_qty) AS  restQty from product_table LEFT JOIN item_inventory_table ON item_inventory_table.product_id=product_table.id where deleted_at IS NULL AND status = 1 Group By product_table.id ORDER BY id ASC")
    fun getActiveProductList(): List<ProductModel>


    @Query("SELECT * from product_table where deleted_at IS NULL AND id = :productId")
    fun getActiveProduct(productId: Int): ProductModel


    @Query("SELECT * from product_table where deleted_at IS NULL ORDER BY id ASC")
    fun getAllProductList(): List<ProductModel>


    @Query("delete from product_table")
    fun delete()

    @Query("update  product_table set deleted_at =  :deletedAd where id = :productId")
    fun deleteDepItemByID(productId: Int, deletedAd : Long)

//    @Query("update category_table SET appointment_date = (:quantity) WHERE id = (:primacyID)")
//    fun updateCatItem(primacyID: Int, quantity: Int)

    @Update
    fun update(memo: ProductModel)
}