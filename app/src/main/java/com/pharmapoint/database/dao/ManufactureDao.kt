package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.DepartmentModel
import com.pharmapoint.model.ManufactureModel

@Dao
interface ManufactureDao {
    @Insert
    fun insert(cartEntity: ManufactureModel)

    @Query("SELECT * from manufacture_table where deleted_at IS NULL ORDER BY id ASC")
    fun getManufactureList(): List<ManufactureModel>

    @Query("SELECT * from manufacture_table where deleted_at IS NULL AND status = 1 ORDER BY id ASC")
    fun getActiveManufactureList(): List<ManufactureModel>

    @Query("delete from manufacture_table")
    fun delete()

    @Query("update  manufacture_table set deleted_at =  :deletedAd where id = :productId")
    fun deleteManufactureItemByID(productId: Int, deletedAd : Long)

//    @Query("update category_table SET appointment_date = (:quantity) WHERE id = (:primacyID)")
//    fun updateCatItem(primacyID: Int, quantity: Int)

    @Update
    fun update(memo: ManufactureModel)
}