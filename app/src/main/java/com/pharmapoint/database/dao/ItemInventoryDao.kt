package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.Item_inventoryModel
import com.pharmapoint.model.ProductModel

@Dao
interface ItemInventoryDao {

    @Insert
    fun insert(cartEntity: Item_inventoryModel)

    @Query("select sum(rest_qty) FROM item_inventory_table WHERE product_id = :productId AND expire_date >= :expireDate")
    fun getTotalQty(productId:Int, expireDate:Long) : Int

    @Query("select * FROM item_inventory_table WHERE product_id = :productId AND expire_date >= :expireDate AND rest_qty > 0")
    fun getItemList(productId:Int, expireDate:Long) : List<Item_inventoryModel>


    @Update
    fun update(memo: Item_inventoryModel)
}