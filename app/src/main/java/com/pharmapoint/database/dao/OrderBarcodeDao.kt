package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.OrderBarcodeModel
import com.pharmapoint.model.OrderModel

@Dao
interface OrderBarcodeDao {
    @Insert
    fun insert(cartEntity: OrderBarcodeModel)

    @Query("select productId from order_barcode_table where barcode in (:barcode)")
    fun getOrderIdByBarcode(barcode: String): Int

    @Query("delete from order_barcode_table")
    fun delete()

    @Update
    fun update(memo: OrderBarcodeModel)
}