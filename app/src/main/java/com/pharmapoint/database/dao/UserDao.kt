package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.LoginResponseData

@Dao
interface UserDao {
    @Insert
    fun insert(cartEntity: LoginResponseData)

    @Query("SELECT COUNT(id) from user_table WHERE user_id = :userID")
    fun getUserCount(userID : Int): Int

    @Query("SELECT * from user_table where status IS NULL AND status = 1 ORDER BY id ASC")
    fun getActiveDepartmentList(): List<LoginResponseData>


    @Query("delete from user_table")
    fun delete()

    @Query("update  user_table set status  =  :deletedAd where id = :productId")
    fun deleteDepItemByID(productId: Int, deletedAd : Long)

//    @Query("update category_table SET appointment_date = (:quantity) WHERE id = (:primacyID)")
//    fun updateCatItem(primacyID: Int, quantity: Int)

    @Update
    fun update(memo: LoginResponseData)
}