package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.OrderModel

@Dao
interface OrderDao {
    @Insert
    fun insert(cartEntity: OrderModel)

    @Query("SELECT * from order_table WHERE deleted_at IS NULL ORDER BY id DESC")
    fun getOrderList(): List<OrderModel>

    @Query("select * from order_table where id in (:orderId)")
    fun getOrderById(orderId: Int): List<OrderModel>

    @Query("select invoiceNo from order_table where invoiceNo = (:invoiceNo)")
    fun getInvoiceExite(invoiceNo: Int): Int

    @Query("select id from order_table ORDER BY id DESC LIMIT 1")
    fun getLastId(): Int

    @Query("delete from order_table")
    fun delete()

    @Query("delete from order_table where id = :orderId")
    fun deleteOrderByID(orderId: Int)

    @Update
    fun update(memo: OrderModel)
}