package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.BrandModel
import com.pharmapoint.model.DepartmentModel
import com.pharmapoint.model.ManufactureModel

@Dao
interface BrandDao {
    @Insert
    fun insert(cartEntity: BrandModel)

    @Query("SELECT * from brand_table where deleted_at IS NULL ORDER BY id ASC")
    fun getBrandList(): List<BrandModel>

    @Query("SELECT * from brand_table where deleted_at IS NULL AND status = 1 ORDER BY id ASC")
    fun getActiveBrandList(): List<BrandModel>

    @Query("delete from brand_table")
    fun delete()

    @Query("update  brand_table set deleted_at =  :deletedAd where id = :productId")
    fun deleteBrandItemByID(productId: Int, deletedAd : Long)

//    @Query("update category_table SET appointment_date = (:quantity) WHERE id = (:primacyID)")
//    fun updateCatItem(primacyID: Int, quantity: Int)

    @Update
    fun update(memo: BrandModel)
}