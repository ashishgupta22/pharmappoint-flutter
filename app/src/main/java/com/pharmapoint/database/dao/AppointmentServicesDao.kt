package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.AppointmentServicesModel
import com.pharmapoint.model.BrandModel
import com.pharmapoint.model.DepartmentModel
import com.pharmapoint.model.ManufactureModel

@Dao
interface AppointmentServicesDao {
    @Insert
    fun insert(cartEntity: AppointmentServicesModel)

    @Query("SELECT * from appointment_services_table where deleted_at IS NULL ORDER BY id ASC")
    fun getAppointmentServicesList(): List<AppointmentServicesModel>

    @Query("SELECT * from appointment_services_table where deleted_at IS NULL AND status = 1 ORDER BY id ASC")
    fun getActiveApoinmentService(): List<AppointmentServicesModel>

    @Query("delete from appointment_services_table")
    fun delete()

    @Query("update  appointment_services_table set deleted_at =  :deletedAd where id = :productId")
    fun deleteAppointmentServicesByID(productId: Int, deletedAd : Long)

//    @Query("update category_table SET appointment_date = (:quantity) WHERE id = (:primacyID)")
//    fun updateCatItem(primacyID: Int, quantity: Int)

    @Update
    fun update(memo: AppointmentServicesModel)
}