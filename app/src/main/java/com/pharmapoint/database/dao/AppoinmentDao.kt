package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.AppoinmentModel

@Dao
interface AppoinmentDao {
    @Insert
    fun insert(cartEntity: AppoinmentModel)

    @Query("SELECT * from appointment_table WHERE deleted_at IS NULL ORDER BY id DESC")
    fun getAppointmentList(): List<AppoinmentModel>

    @Query("select * from appointment_table where id in (:productId)")
    fun getCartItemById(productId: Int): List<AppoinmentModel>

    @Query("delete from appointment_table")
    fun delete()

    @Query("delete from appointment_table where id = :productId")
    fun deleteCartItemByID(productId: Int)

    @Query("update appointment_table SET appointment_date = (:quantity) WHERE id = (:primacyID)")
    fun updateCartItem(primacyID: Int, quantity: Int)

    @Update
    fun update(memo: AppoinmentModel)
}