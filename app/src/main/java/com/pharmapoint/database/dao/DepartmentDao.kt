package com.pharmapoint.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.DepartmentModel

@Dao
interface DepartmentDao {
    @Insert
    fun insert(cartEntity: DepartmentModel)

    @Query("SELECT * from department_table where deleted_at IS NULL ORDER BY id ASC")
    fun getDepartmentList(): List<DepartmentModel>

    @Query("SELECT * from department_table where deleted_at IS NULL AND status = 1 ORDER BY id ASC")
    fun getActiveDepartmentList(): List<DepartmentModel>


    @Query("delete from department_table")
    fun delete()

    @Query("update  department_table set deleted_at =  :deletedAd where id = :productId")
    fun deleteDepItemByID(productId: Int, deletedAd : Long)

//    @Query("update category_table SET appointment_date = (:quantity) WHERE id = (:primacyID)")
//    fun updateCatItem(primacyID: Int, quantity: Int)

    @Update
    fun update(memo: DepartmentModel)
}