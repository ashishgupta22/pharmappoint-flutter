package com.pharmapoint.database.dao

import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.pharmapoint.model.CategoryModel

@Dao
interface CategoryDao {
    @Insert
    fun insert(cartEntity: CategoryModel)

    @Query("SELECT * from category_table where parent_id IS NULL AND deleted_at IS NULL ORDER BY id ASC")
    fun getCategoryList(): List<CategoryModel>

    @Query("SELECT * from category_table where parent_id NOTNULL AND deleted_at IS NULL ORDER BY id ASC")
    fun getSubCategoryList(): List<CategoryModel>

    @Query("SELECT * from category_table where parent_id = :parentID AND deleted_at IS NULL AND status = 1 ORDER BY id ASC")
    fun getSubCategoryListByID(parentID: Int?): List<CategoryModel>

    @Query("SELECT * from category_table where parent_id IS NULL AND deleted_at IS NULL AND status = 1 ORDER BY id ASC")
    fun getActiveCategoryList(): List<CategoryModel>

    @Query("select * from category_table where parent_id = :parentID AND deleted_at IS NULL ")
    fun getCartItemById(parentID: Int?): List<CategoryModel>

    @Query("select * from category_table where parent_id  = :parentID AND deleted_at IS NULL ")
    fun getCatItem(parentID: Int?): List<CategoryModel>

    @Query("delete from category_table")
    fun delete()

    @Query("update  category_table set deleted_at =  :deletedAd where id = :productId")
    fun deleteCatItemByID(productId: Int,deletedAd : Long)

//    @Query("update category_table SET appointment_date = (:quantity) WHERE id = (:primacyID)")
//    fun updateCatItem(primacyID: Int, quantity: Int)

    @Update
    fun update(memo: CategoryModel)
}