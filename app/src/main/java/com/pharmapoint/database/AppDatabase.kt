package com.pharmapoint.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.migration.Migration
import com.pharmapoint.database.dao.*
import com.pharmapoint.model.*


@Database(entities = [AppoinmentModel::class, CategoryModel::class, DepartmentModel::class, ManufactureModel::class, BrandModel::class,
    ProductModel::class, AppointmentServicesModel::class,CustomerModel::class,LogModel::class,LoginResponseData::class,OrderDetailsModel::class,OrderModel::class,OrderBarcodeModel::class
                     ,Item_inventoryModel::class], version =36, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun appinmentDao(): AppoinmentDao
    abstract fun categoryDao(): CategoryDao
    abstract fun departmentDao(): DepartmentDao
    abstract fun manufactureDao(): ManufactureDao
    abstract fun brandDao(): BrandDao
    abstract fun productDao(): ProductDao
    abstract fun appointmentServicesDao(): AppointmentServicesDao
    abstract fun customerDao(): CustomerDao
    abstract fun logDao(): LogDao
    abstract fun userDao(): UserDao
    abstract fun orderDetailsDao(): OrderDetailsDao
    abstract fun orderDao(): OrderDao
    abstract fun orderBarcodeDao(): OrderBarcodeDao
    abstract fun itemInventery(): ItemInventoryDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext, AppDatabase::class.java, "pharmapoint"
                ).fallbackToDestructiveMigration()
                    .addMigrations(MIGRATION_1_2)
                    .allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL(
                    "ALTER TABLE appointment_table "
                            + "RENAME COLUMN customer_mobile to  customer_mobile"
                )
            }
        }
    }
}