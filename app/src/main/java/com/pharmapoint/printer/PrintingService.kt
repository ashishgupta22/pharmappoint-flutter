package com.pharmapoint.printer;

import android.app.Service
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.os.Handler
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log
import com.pharmapoint.utlity.Constans

class PrintingService : JobService() {

    private var mActivityMessenger: Messenger? = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        mActivityMessenger = intent.getParcelableExtra(Constans.PRINT_INTENT_KEY)
        return Service.START_NOT_STICKY
    }

    override fun onStartJob(params: JobParameters): Boolean {
        sendMessage(Constans.MSG_COLOR_START, params.jobId)
        val duration = params.extras.getLong(Constans.PRINT_DURATION_KEY)

        val handler = Handler()
        handler.postDelayed({
            sendMessage(Constans.MSG_COLOR_STOP, params.jobId)
            jobFinished(params, false)
        }, duration)
        Log.i(TAG, "on start job: " + params.jobId)
        return true
    }

    override fun onStopJob(params: JobParameters): Boolean {
        sendMessage(Constans.MSG_COLOR_STOP, params.jobId)
        return false
    }

    private fun sendMessage(messageID: Int, params: Any?) {
        if (mActivityMessenger == null) {
            Log.d(TAG, "Service is bound, not started. There's no callback to send a message to.")
            return
        }
        val m = Message.obtain()
        m.what = messageID
        m.obj = params
        try {
            mActivityMessenger!!.send(m)
        } catch (e: RemoteException) {
            Log.e(TAG, "Error passing service object back to activity.")
        }

    }

    companion object {

        private val TAG = PrintingService::class.java.simpleName
    }
}
