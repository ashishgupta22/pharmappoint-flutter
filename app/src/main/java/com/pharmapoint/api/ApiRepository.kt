package com.buy786.api

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.pharmapoint.AppConfig
import com.pharmapoint.R
import com.pharmapoint.activity.MainActivity
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.ForgotPasswordResponseBean
import com.pharmapoint.model.LogModel
import com.pharmapoint.model.LoginResponseBean
import com.pharmapoint.model.VerifyOtpResponseBean
import com.pharmapoint.progressbar.ProgressBarDialog
import com.pharmapoint.ui_fragment.home_fragment.SetupPinFragment
import com.pharmapoint.ui_fragment.login_register.LoginFragment
import com.pharmapoint.ui_fragment.login_register.OtpFragment
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import rx.android.schedulers.AndroidSchedulers
import rx.Observable
import rx.Observer
import rx.schedulers.Schedulers


class ApiRepository() {
    var progressBarDialog: ProgressBarDialog? = null
    lateinit var commonResponseBeanObservable: Observable<LoginResponseBean>
    var commonMutableLiveData = MutableLiveData<LoginResponseBean>()

    fun loginAPI(
        context: Context,
        paramas:Map<String,String>
    ) {
        progressBarDialog = ProgressBarDialog(context)
        progressBarDialog!!.show()


        val responseBeanObservable = AppConfig.msApiInterface!!.login(paramas)

        responseBeanObservable.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LoginResponseBean> {
                override fun onCompleted() {

                }
                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    Utils.showToast(context,""+e.toString())
                    progressBarDialog!!.dismiss()
                }

                override fun onNext(t: LoginResponseBean) {
                    Log.d("print",t.toString())
                    if (t.error == false) {
                        if (t.errorCode == 0) {

                            PreferenceUtils(context).setString(
                                PreferenceUtils.USER_TYPE, t.data.user_type
                            )
                            PreferenceUtils(context).setInteger(
                                PreferenceUtils.USER_ID,
                                t.data.user_id
                            )

                            PreferenceUtils(context).setString(
                                PreferenceUtils.USER_NAME,
                                t.data.name
                            )
                            var mPin = 0
                            if(t.data.mpin != null && t.data.mpin > 0){
                                mPin = t.data.mpin
                            }

                            PreferenceUtils(context).setInteger(
                                PreferenceUtils.LOGIN_MPIN,
                                mPin
                            )
                            PreferenceUtils(context).setString(
                                PreferenceUtils.LOGIN_TOKEN,
                                t.data.token
                            )

                            val db = AppDatabase.getDatabase(context)
                            val userCount = db.userDao().getUserCount(t.data.user_id)
                            if (userCount <= 0){
                                db.userDao().insert(t.data)
                            }
                            val time = System.currentTimeMillis()
                            val logModel = LogModel()
                            logModel.created_at = time
                            logModel.user_id = t.data.user_id
                            logModel.log_time_in = time
                            logModel.type = Constans.log_type_login
                            val id = db.logDao().insert(logModel).toInt()
                            PreferenceUtils(context).setInteger(PreferenceUtils.LOGINTIME_ID,id)
                            if(mPin > 0){
                                PreferenceUtils(context).setBoolean(
                                    PreferenceUtils.LOGIN_STATUS,
                                    true
                                )
                                context.startActivity(Intent(context, MainActivity::class.java))
                                (context as Activity).finish()
                            }else{
                                    val fragment = SetupPinFragment.newInstance();
                                val  bundle = Bundle()
                                bundle.putInt("type",2)
                                fragment.arguments = bundle
                                (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(
                                    R.id.container, fragment).commit()
                            }
                        }
                        else {
                            Utils.showToast(context, t.message)
                        }
                    } else {
                        Utils.showToast(context, t.message)
                    }
                    progressBarDialog!!.dismiss()
                }
            })
    }

    fun forgotPasswordAPI(
        context: Context,
        paramas:Map<String,String>
    ) {
        progressBarDialog = ProgressBarDialog(context)
        progressBarDialog!!.show()
        val responseBeanObservable = AppConfig.msApiInterface!!.forgotPassword(paramas)
        responseBeanObservable.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ForgotPasswordResponseBean> {
                override fun onCompleted() {

                }
                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    Utils.showToast(context,""+e.toString())
                    progressBarDialog!!.dismiss()
                }

                override fun onNext(data: ForgotPasswordResponseBean) {
                    Log.d("print",data.toString())
                    if (data.error == false) {
                        if (data.errorCode == 0) {
                            Utils.showToast(context, data.message)
                            (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(
                                R.id.container, LoginFragment.newInstance()).commit()
                        }
//
                        else {
                            Utils.showToast(context, data.message)
                        }

                    } else {
                        Utils.showToast(context, data.message)
                    }
                    progressBarDialog!!.dismiss()
                }
            })
    }
    fun setUpmPIN(
        context: Context,
        paramas:Map<String,String>,
        type : Int
    ) {
        progressBarDialog = ProgressBarDialog(context)
        progressBarDialog!!.show()
        AppConfig.reCreateAPI()
        val responseBeanObservable = AppConfig.msApiInterface!!.setUpmPin(paramas)
        responseBeanObservable.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<VerifyOtpResponseBean> {
                override fun onCompleted() {

                }
                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    Utils.showToast(context,""+e.toString())
                    progressBarDialog!!.dismiss()
                }

                override fun onNext(data: VerifyOtpResponseBean) {
                    Log.d("print",data.toString())
                    if (data.error == false) {
                        if (data.errorCode == 0) {
                            Utils.showToast(context, data.message)
                            PreferenceUtils(context).setBoolean(
                                PreferenceUtils.LOGIN_STATUS,
                                true
                            )
                            PreferenceUtils(context).setInteger(
                                PreferenceUtils.LOGIN_MPIN,
                                paramas[Constans.mpin]!!.toInt()
                            )
                            if(type != 1){
                                context.startActivity(Intent(context, MainActivity::class.java))
                                (context as Activity).finish()
                            }
                        }
                        else {
                            Utils.showToast(context, data.message)
                        }

                    } else {
                        Utils.showToast(context, data.message)
                    }
                    progressBarDialog!!.dismiss()
                }
            })
    }

    fun verifyOtpAPI(
        context: Context,
        paramas:Map<String,String>
    ) {
        progressBarDialog = ProgressBarDialog(context)
        progressBarDialog!!.show()


        val responseBeanObservable = AppConfig.msApiInterface!!.verifyOtp(paramas)

        responseBeanObservable.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<VerifyOtpResponseBean> {
                override fun onCompleted() {

                }
                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    Utils.showToast(context,""+e.toString())
                    progressBarDialog!!.dismiss()
                }

                override fun onNext(data: VerifyOtpResponseBean) {
                    Log.d("print",data.toString())
                    if (data.error == false) {
                        if (data.errorCode == 0) {
                            Utils.showToast(context, data.message)
                            (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(
                                R.id.container, LoginFragment.newInstance()).commit()
                        }
//
                        else {
                            Utils.showToast(context, data.message)
                        }

                    } else {
                        Utils.showToast(context, data.message)
                    }
                    progressBarDialog!!.dismiss()
                }
            })
    }


}


