package com.buy786.api

import com.pharmapoint.model.ForgotPasswordResponseBean
import com.pharmapoint.model.LoginResponseBean
import com.pharmapoint.model.VerifyOtpResponseBean
import retrofit2.http.*
import rx.Observable
import java.util.*

interface ApiInterface {

    companion object {
        private const val LOGIN: String = "login"
        private const val FORGOTPASSWORD: String = "forgotPassword"
        private const val VERIFYOTP: String = "verifyOtp"
        private const val SETUPMPIN: String = "updateMpin"
    }

    @FormUrlEncoded
    @POST(LOGIN)
    fun login(@FieldMap map: Map<String, String>): Observable<LoginResponseBean>

    @FormUrlEncoded
    @POST(FORGOTPASSWORD)
    fun forgotPassword(@FieldMap map: Map<String, String>): Observable<ForgotPasswordResponseBean>

    @FormUrlEncoded
    @POST(VERIFYOTP)
    fun verifyOtp(@FieldMap map: Map<String, String>) : Observable<VerifyOtpResponseBean>

    @FormUrlEncoded
    @POST(SETUPMPIN)
    fun setUpmPin(@FieldMap map: Map<String, String>) : Observable<VerifyOtpResponseBean>
//
//    @Multipart
//    @POST(PROFILE_UPDATE)
//    fun updateProfile(
//        @Part("userId") userId: RequestBody,
//        @Part("firstName") firstName: RequestBody,
//        @Part("lastName") lastName: RequestBody,
//        @Part("gender") gender: RequestBody,
//        @Part("mobile") mobile: RequestBody,
//        @Part("phone") phone: RequestBody,
//        @Part userImage: MultipartBody.Part,
//        @Part coverImage: MultipartBody.Part
//    ): Observable<ProfileModel>


}
