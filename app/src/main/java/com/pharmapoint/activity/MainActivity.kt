package com.pharmapoint.activity

import android.annotation.SuppressLint
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayoutMediator
import com.pharmapoint.R
import com.pharmapoint.adapter.LeftMenuAdapter
import com.pharmapoint.adapter.MyViewPagerAdapter
import com.pharmapoint.databinding.ActivityMainBinding
import com.pharmapoint.ui_fragment.home_fragment.*
import com.pharmapoint.utlity.Constans
import com.pharmapoint.viewmodel.MainActivityViewModel
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.*
import com.pharmapoint.printer.BackgroundService
import com.pharmapoint.printer.PrinterFunctions
import com.pharmapoint.printer.PrintingService
import kotlinx.android.synthetic.main.add_procut_fragment.*
import java.lang.ref.WeakReference


class MainActivity : AppCompatActivity() {

    private lateinit var adapter: MyViewPagerAdapter
    private var doubleBackToExitPressedOnce = false
    private lateinit var leftMenuAdapter: LeftMenuAdapter
    private lateinit var leftMenuList: MutableList<MenuItemModel>
    private lateinit var binding: ActivityMainBinding

    private lateinit var db: AppDatabase

    /**
     * Background Service
     * */
    private var mServiceComponent: ComponentName? = null
    private var mServicePrintComponent: ComponentName? = null
    private var mHandler: IncomingMessageHandler? = null
    private var mHandlerPrint: PrintingMessageHandler? = null
    private var mJobId = 0
    private var mJobIdPrint = 0
    
    var portName = "USB:" //******
    var portSettings = 0
  
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        db = AppDatabase.getDatabase(this)
        binding.viewModel = MainActivityViewModel(this,db)
        bindView();
        binding!!.viewModel!!.bindHandler()
        bindLeftMenu()

    }

    private fun bindView() {
      //  PrinterFunctions.OpenPort(portName, portSettings)
        Log.d("print","tesssst")
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        binding!!.viewModel!!.stopHandler();
        binding!!.viewModel!!.startHandler();
    }

    override fun onPause() {
        binding!!.viewModel!!.stopHandler();
        Log.d("onPause", "onPauseActivity change");
        super.onPause()

    }

    override fun onResume() {
        super.onResume()
        binding!!.viewModel!!.startHandler();
        Log.d("onResume", "onResume_restartActivity");
    }

    override fun onDestroy() {
        stopService(Intent(this@MainActivity, BackgroundService::class.java))
        stopService(Intent(this@MainActivity, PrintingService::class.java))
        super.onDestroy()
        binding!!.viewModel!!.stopHandler();
        Log.d("onDestroy", "onDestroyActivity change");
    }

    private fun bindLeftMenu() {
        leftMenuList = Constans.getLeftMenu(this)
        binding.rcyMenuLeft.layoutManager = LinearLayoutManager(this)
        leftMenuAdapter = LeftMenuAdapter(this, leftMenuList){it ->
            Log.d("testttt","testttt")
            leftMenuList?.find { it.isSelected == true }?.isSelected = false
            leftMenuList[it!!].isSelected = true;
            leftMenuAdapter.notifyDataSetChanged()
            bindPagerView(leftMenuList[it!!])
        }
        bindPagerView(leftMenuList[0!!])
        binding.rcyMenuLeft.adapter =leftMenuAdapter

        binding.imgLogo.setOnClickListener(){
            PrinterFunctions.PrintText(
                portName,
                portSettings,
                0,
                0,
                1,
                0,
                0,
                1,
                0,
                0,
                resources.getString(R.string.app_name) + "testttt\n"
            )
        }
    }

    override fun onStart() {
        super.onStart()
        mServiceComponent = ComponentName(this, BackgroundService::class.java)
        mHandler = IncomingMessageHandler(this@MainActivity)
        cancelJob()
        mServicePrintComponent = ComponentName(this, PrintingService::class.java)
        mHandlerPrint = PrintingMessageHandler(this@MainActivity)
        schedulePrintJob()
        scheduleJob()

        val startServiceIntent = Intent(this@MainActivity, BackgroundService::class.java)
        val messengerIncoming = Messenger(mHandler)
        startServiceIntent.putExtra(Constans.MESSENGER_INTENT_KEY, messengerIncoming)
        startService(startServiceIntent)

        val printServiceIntent = Intent(this@MainActivity, PrintingService::class.java)
        val messengerPrint = Messenger(mHandlerPrint)
        printServiceIntent.putExtra(Constans.PRINT_INTENT_KEY, messengerPrint)
        startService(printServiceIntent)
//        PrinterFunctions.OpenPort(portName, portSettings)

    }
    private fun bindPagerView(menuItemModel: MenuItemModel) {
        adapter = MyViewPagerAdapter(supportFragmentManager,lifecycle)
        adapter.addFragment(menuItemModel.fragmentList,menuItemModel.tabList)

        binding.homeFragmentViewPager.adapter =adapter
        TabLayoutMediator(binding.homeFragmentTabs, binding.homeFragmentViewPager) { tab, position ->
            tab.text = adapter.getPageTitle(position)
            binding.homeFragmentViewPager.setCurrentItem(tab.position, true)
        }.attach()
        for(i in 0..binding.homeFragmentTabs.tabCount -1){
            binding.homeFragmentTabs.getTabAt(i)!!.setIcon(menuItemModel.tabList[i].icon)
        }
    }

    fun changePosition(position : Int){
//        binding.homeFragmentViewPager.setCurrentItem(position);
//        adapter.notifyDataSetChanged();
        binding.homeFragmentViewPager.postDelayed(Runnable { binding.homeFragmentViewPager.setCurrentItem(position) }, 10)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler(Looper.getMainLooper()).postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

    private fun cancelJob() {
        val tm = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        tm.cancelAll()
    }

    fun scheduleJob() {
        val builder = mServiceComponent?.let { JobInfo.Builder(mJobId++, it) }
        builder?.setMinimumLatency(java.lang.Long.valueOf("1") * 1000)
        builder?.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)

        val tm = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        builder?.build()?.let { tm.schedule(it) }
    }

    @SuppressLint("HandlerLeak")
    private inner class IncomingMessageHandler internal constructor(activity: MainActivity) :
        Handler() {
        private val mActivity: WeakReference<MainActivity> = WeakReference(activity)

        override fun handleMessage(msg: Message) {
            val mainActivity = mActivity.get() ?: return
            val m: Message
            when (msg.what) {
                Constans.MSG_COLOR_START -> {
                    m = Message.obtain(this, 0)
                    sendMessageDelayed(m, 1000L)
                }
                Constans.MSG_COLOR_STOP -> {
                    m = obtainMessage(1)
                    sendMessageDelayed(m, 1000L)
//                    apiCounter += 1
//                    if (apiCounter == 4) {
//                        apiCounter = 0
//                        getKotOrders(false)
//                    }
//                    printDataList = PreferenceUtils.getPreferenceUtil(this@MainActivity).getList()!!
//                    if (printDataList.size > 0 && !isPrintinginprocess) {
//                        runOnUiThread(Runnable {
//                            if (printDataList.get(0).modeldata != null) {
//                                printReceipt(
//                                    printDataList.get(0).modeldata!!,
//                                    printDataList.get(0).fromPrintButton!!
//                                )
//                            }
//                        })
//                    }
                    scheduleJob()
                }
            }
        }
    }

    fun schedulePrintJob() {
        val builder = mServicePrintComponent?.let { JobInfo.Builder(mJobIdPrint++, it) }
        builder?.setMinimumLatency(java.lang.Long.valueOf("1") * 1000)
        builder?.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)

        val tm = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        builder?.build()?.let { tm.schedule(it) }
    }

    @SuppressLint("HandlerLeak")
    private inner class PrintingMessageHandler internal constructor(activity: MainActivity) :
        Handler() {
        private val mActivity: WeakReference<MainActivity> = WeakReference(activity)

        override fun handleMessage(msg: Message) {
            val mainActivity = mActivity.get() ?: return
            val m: Message
            when (msg.what) {
                Constans.MSG_COLOR_START -> {
                    m = Message.obtain(this, 0)
                    sendMessageDelayed(m, 1000L)
                }
                Constans.MSG_COLOR_STOP -> {
                    m = obtainMessage(1)
                    sendMessageDelayed(m, 1000L)
//                    TODO: PRINTING PROCESS FROM A LIST
//                    printDataList = PreferenceUtils.getPreferenceUtil(this@MainActivity).getList()!!
//                    if (printDataList.size > 0) {
//                        runOnUiThread(Runnable {
//                            if (printDataList.get(0).modeldata != null) {
//                                printReceipt(
//                                    printDataList.get(0).modeldata!!,
//                                    printDataList.get(0).fromPrintButton!!
//                                )
//                            }
//                        })
//                    } else {
//                        schedulePrintJob()
//                    }
                    schedulePrintJob()
                }
            }
        }
    }


}