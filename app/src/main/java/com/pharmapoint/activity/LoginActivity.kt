package com.pharmapoint.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pharmapoint.R
import com.pharmapoint.ui_fragment.login_register.LoginFragment

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, LoginFragment.newInstance())
                .commitNow()
        }
    }
}