package com.pharmapoint.utlity

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.pharmapoint.BuildConfig
import com.pharmapoint.R
import com.pharmapoint.model.MenuItemModel
import com.pharmapoint.model.TabtemModel
import com.pharmapoint.ui_fragment.home_fragment.*

class Constans {
    companion object {
        val startOrdrderID = 100001
        val startOrderRefNo = "PP"
        val log_type_login: Int = 0
        val log_type_logout: Int = 1
        val log_type_breakIn: Int = 2
        val log_type_breakOut: Int = 3
        val title = arrayOf("Mr", "Miss", "Mrs", "Ms", "Prof" ,"Rev", "Mx")
        val userEmail = "email"
        val userPassword = "password"
        val deviceName = "device_name"
        val fcmToken = "fcm_token"
        val mpin = "mpin"
        val homeId = 1
        val appointmentsId = 2
//        val productsId = 3
        val historyId = 3
        val reportId = 4
        val adminId = 5
        val home_tab_shop_id = 6
        val home_tab_addProduct_id = 7
        val home_tab_inventory_id = 8
        val home_tab_log_id = 9
        val appointments_tab_appointment_id = 10
        val appointments_tab_addCustomer_id = 11
        val product_tab_product1_id = 12
        val product_tab_product2_id = 13
        val product_tab_product3_id = 14
        val product_tab_product4_id = 15
        val history_tab_sales_id = 16
        val history_tab_sold_id = 17
        val report_tab_report1_id = 18
        val report_tab_report2_id = 19
        val admin_tab_admin1_id = 20
        val admin_tab_admin2_id = 21
        val master_tab_cat_id = 22
        val master_tab_sub_cat_id = 23
        val master__menu_id = 24
        val master_tab_department_id = 25
        val master_tab_manufacture_id = 26
        val master_tab_brand_id = 27
        val master_appointment_services_id = 28
        val master_Settings_menu_id = 29
        val master_Setup_Pin_menu_id = 30



        const val MESSENGER_INTENT_KEY = BuildConfig.APPLICATION_ID + ".MESSENGER_INTENT_KEY"
        const val WORK_DURATION_KEY = BuildConfig.APPLICATION_ID + ".WORK_DURATION_KEY"
        const val PRINT_INTENT_KEY = BuildConfig.APPLICATION_ID + ".PRINT_INTENT_KEY"
        const val PRINT_DURATION_KEY = BuildConfig.APPLICATION_ID + ".PRINT_DURATION_KEY"
        const val MSG_COLOR_START = 11
        const val MSG_COLOR_STOP = 12
        const val PRINT_LIST = "PRINT_LIST"

        // Date Time format
        val dateTimeFormat = "dd-MMM-yyy hh:mm a"
        // Date format
        val dateFormat =  "dd-MMM-yyy"
        val timeTypeArray = arrayOf("Login Time","Logout","Break Time", "Break End")
         fun getLeftMenu(context: Context): MutableList<MenuItemModel> {
            val list  =  mutableListOf<MenuItemModel>()
            list.add(MenuItemModel("Home",true,homeId, R.drawable.ic_home_selected, getHomeTabs(context),
                getHomeFragmetnsTabs()),)
            list.add(MenuItemModel("Appointments",false,appointmentsId,R.drawable.ic_calendar_unselected,getAppoinmentTabs(context),
                getAppoinmentFragmetnsTabs()))
//            list.add(MenuItemModel("Products",false,productsId,R.drawable.ic_product_unseleted,getProductsTabs()))
            list.add(MenuItemModel("History",false,historyId,R.drawable.ic_history_unseleted,getHistoryTabs(context),
                getHistoryFragmetnsTabs()))
            list.add(MenuItemModel("Report",false,reportId,R.drawable.ic_report_unselected,getReportTabs(context),
                getReportFragmetnsTabs()))
            list.add(MenuItemModel("Admin",false,adminId,R.drawable.ic_admin_unseleted,getAdminTabs(context),
                getAdminFragmetnsTabs()))
            list.add(MenuItemModel("Master",false,master__menu_id,R.drawable.ic_admin_unseleted,getMasterTabs(context),
                getMasterFragmetnsTabs()))
             list.add(MenuItemModel("Settings",false,master_Settings_menu_id,R.drawable.ic_baseline_settings_24,getSettingTabs(context),
                getSettingFragmetnsTabs()))
            return list
        }

        fun getHomeTabs(context: Context): MutableList<TabtemModel> {
            val list  =  mutableListOf<TabtemModel>()
            list.add(TabtemModel("Shop Front",true,home_tab_shop_id,
                context.getDrawable( R.drawable.ic_shop_unselected)!!
            ))
            list.add(TabtemModel("Add Product",false,home_tab_addProduct_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))
            list.add(TabtemModel("Inventory",false,home_tab_inventory_id,
                context.getDrawable(R.drawable.ic_inventory_unselected)!!
            ))
            list.add(TabtemModel("Log",false,home_tab_log_id,
                context.getDrawable(R.drawable.ic_log_unselected)!!
            ))
            return list
        }
        fun getHomeFragmetnsTabs(): MutableList<Fragment> {
            val list  =  mutableListOf<Fragment>()
            list.add(ShopFrontFragment.newInstance())
            list.add(AddProcutFragment.newInstance())
            list.add(InventryFragment.newInstance())
            list.add(LogFragment.newInstance())
            return list
        }
        fun getAppoinmentTabs(context: Context): MutableList<TabtemModel> {
            val list  =  mutableListOf<TabtemModel>()
            list.add(TabtemModel("Appointments",true,appointments_tab_appointment_id,
                context.getDrawable(R.drawable.ic_tab_calendar_unselected)!!
            ))
            list.add(TabtemModel("Add Customer",false,appointments_tab_addCustomer_id,
                context.getDrawable(R.drawable.ic_tab_admin_unselected)!!
            ))
            return list
        }
        fun getAppoinmentFragmetnsTabs(): MutableList<Fragment> {
            val list  =  mutableListOf<Fragment>()
            list.add(AppointmentFragment.newInstance())
            list.add(AddCustomerFragment.newInstance())
            return list
        }

        fun getHistoryTabs(context: Context): MutableList<TabtemModel> {
            val list  =  mutableListOf<TabtemModel>()
            list.add(TabtemModel("Sales Transaction History",true,history_tab_sales_id,
                context.getDrawable(R.drawable.ic_sales_history_unselected)!!
            ))
            list.add(TabtemModel("Product Sold History",false,history_tab_sold_id,
                context.getDrawable(R.drawable.ic_sold_product_unselected)!!
            ))
            return list
        }
        fun getHistoryFragmetnsTabs(): MutableList<Fragment> {
            val list  =  mutableListOf<Fragment>()
            list.add(SalesTransactionHistoryFragment.newInstance())
            list.add(SoldProductHistoryFragment.newInstance())
            return list
        }

        fun getReportTabs(context: Context): MutableList<TabtemModel> {
            val list  =  mutableListOf<TabtemModel>()
            list.add(TabtemModel("Report 1",true, report_tab_report1_id,
                context.getDrawable(R.drawable.ic_shop_unselected)!!
            ))
            list.add(TabtemModel("Report 2",false, report_tab_report2_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))
            return list
        }
        fun getReportFragmetnsTabs(): MutableList<Fragment> {
            val list  =  mutableListOf<Fragment>()
            list.add(CommingSoonFragment.newInstance())
            list.add(CommingSoonFragment.newInstance())
            return list
        }
        fun getAdminTabs(context: Context): MutableList<TabtemModel> {
            val list  =  mutableListOf<TabtemModel>()
            list.add(TabtemModel("Login Log",true,admin_tab_admin1_id,
                context.getDrawable(R.drawable.ic_shop_unselected)!!
            ))
            list.add(TabtemModel("Admin 2",false, admin_tab_admin2_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))
            return list
        }
        fun getAdminFragmetnsTabs(): MutableList<Fragment> {
            val list  =  mutableListOf<Fragment>()
            list.add(LoginLogFragment.newInstance())
            list.add(CommingSoonFragment.newInstance())
            return list
        }
        fun getMasterTabs(context: Context): MutableList<TabtemModel> {
            val list  =  mutableListOf<TabtemModel>()
            list.add(TabtemModel("Add Category",true,master_tab_cat_id,
                context.getDrawable(R.drawable.ic_shop_unselected)!!
            ))
            list.add(TabtemModel("Add Sub Category",false, master_tab_sub_cat_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))
            list.add(TabtemModel("Add Department",false, master_tab_department_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))
            list.add(TabtemModel("Add Manufacture",false, master_tab_manufacture_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))
            list.add(TabtemModel("Add Brand",false, master_tab_brand_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))
            list.add(TabtemModel("Appointment Services",false, master_appointment_services_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))
            return list
        }
        fun getMasterFragmetnsTabs(): MutableList<Fragment> {
            val list  =  mutableListOf<Fragment>()
            list.add(AddEditCategoryFragment.newInstance())
            list.add(AddEditSubCategoryFragment.newInstance())
            list.add(AddEditDepartmentFragment.newInstance())
            list.add(AddEditManufactureFragment.newInstance())
            list.add(AddEditBrandFragment.newInstance())
            list.add(AddEditAppointmentServicesFragment.newInstance())
            return list
        }
        fun getSettingTabs(context: Context): MutableList<TabtemModel> {
            val list = mutableListOf<TabtemModel>()
            list.add(TabtemModel("Setup Pin",true, master_Setup_Pin_menu_id,
                context.getDrawable(R.drawable.ic_product_unseleted)!!
            ))

            return list
        }
        fun getSettingFragmetnsTabs(): MutableList<Fragment> {

            val fragment = SetupPinFragment.newInstance();
            val  bundle = Bundle()
            bundle.putInt("type",1)
            fragment.arguments = bundle

            val list  =  mutableListOf<Fragment>()
            list.add(fragment)
            return list
        }
    }
}