package com.pharmapoint.utlity

import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter

class CustomDataBinding {
    companion object {
        @JvmStatic
        @BindingAdapter("setDate")
        fun setDate(txtView: TextView, value : Long) {
            txtView.setText(Utils.toDateTimeString(Utils.toDate(value)))
        }

        @JvmStatic
        @BindingAdapter("setTextAmount")
        fun setTextAmount(txtView: TextView, value : Double) {
            txtView.setText("£$value")
        }

        @JvmStatic
        @BindingAdapter("setTextTxnType")
        fun setTextTxnType(txtView: TextView, value : String) {
            txtView.setText(value)
        }
    }
}