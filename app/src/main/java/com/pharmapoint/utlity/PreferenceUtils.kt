package com.pharmapoint.utlity

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.Nullable

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "StaticFieldLeak")
class PreferenceUtils(msContext: Context) {

    companion object {
        const val msPreferences = "com.pharmapoint"
        const val LOGIN_STATUS = "login_status"
        const val USER_TYPE = "user_type"
        const val USER_ID = "user_id"
        const val USER_NAME = "user_name"
        const val BREAK_TIME_ID = "break_time_id"
        const val LOGINTIME_ID = "login_time_id"
        const val LOGIN_MPIN = "mPIN"
        const val LOGIN_TOKEN = "loginToken"
    }

    private val pref: SharedPreferences = msContext.getSharedPreferences(msPreferences, Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = pref.edit()


    fun setBoolean(preferenceKey: String, data: Boolean) {
        editor.putBoolean(preferenceKey, data).apply()
    }

    fun getBoolean(preferenceKey: String): Boolean {
        return pref.getBoolean(preferenceKey, false)
    }


    fun setString(preferenceKey: String, data: String) {
        editor.putString(preferenceKey, data).apply()
    }

    fun getString(preferenceKey: String): String {
        return pref.getString(preferenceKey, "")!!
    }


    fun setInteger(preferenceKey: String, data: Int) {
        editor.putInt(preferenceKey, data).apply()
    }

    fun getInteger(preferenceKey: String): Int {
        return pref.getInt(preferenceKey, 0)
    }


    fun clearData() {
        pref.edit().clear().apply()
    }

}
