package com.pharmapoint.utlity

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.wifi.WifiManager
import android.provider.Settings
import android.text.TextUtils
import android.text.format.Formatter
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.widget.addTextChangedListener
import com.google.android.material.snackbar.Snackbar
import com.pharmapoint.R
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.ProductModel
import kotlinx.android.synthetic.main.add_procut_fragment.*
import kotlinx.android.synthetic.main.layout_payment_dialog.view.*
import java.io.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


object Utils {


    lateinit var progressLoader: ProgressDialog

    fun showProgressLoader(context: Context) {
        progressLoader = ProgressDialog(context)
        progressLoader.setTitle("Loading")
        progressLoader.setMessage("Wait while loading...")
        progressLoader.setCancelable(false)
        progressLoader.show()
    }

    fun cancelProgressLoader() {
        if (progressLoader.isShowing)
            progressLoader.cancel()
    }

    fun showSnackBar(view: View, message: String) {
        val snackBar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction("OK") { }
        snackBar.show()
    }

    @SuppressLint("HardwareIds")
    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun decodeFile(f: File?): Bitmap? {
        var b: Bitmap? = null
        //Decode image size
        val o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        var fis: FileInputStream? = null
        try {
            fis = FileInputStream(f)
            BitmapFactory.decodeStream(fis, null, o)
            fis.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val scale = 1
        val o2 = BitmapFactory.Options()
        o2.inSampleSize = scale
        try {
            fis = FileInputStream(f)
            b = BitmapFactory.decodeStream(fis, null, o2)
            fis.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return b
    }

    fun reduceFileSize(file: File, compresRate: Int): File? {
        val exif: ExifInterface
        try {
            exif = ExifInterface(file.absolutePath)
            val orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.postRotate(90f)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.postRotate(180f)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.postRotate(270f)
                Log.d("EXIF", "Exif: $orientation")
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val file1 = File(file.absolutePath)
        return try {
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            o.inSampleSize = 6
            // factor of downsizing the image
            var inputStream = FileInputStream(file1)
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o)
            inputStream.close()

            // The new size we want to scale to
            val REQUIRED_SIZE = 85

            // Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                o.outHeight / scale / 2 >= REQUIRED_SIZE
            ) {
                scale *= 2
            }
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            inputStream = FileInputStream(file1)
            val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
            inputStream.close()
            file1.createNewFile()
            val outputStream = FileOutputStream(file1)
            selectedBitmap!!.compress(Bitmap.CompressFormat.JPEG, compresRate, outputStream)
            file1
        } catch (e: Exception) {
            null
        }
    }

    fun setupUI(ctx: Activity, view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View, event: MotionEvent): Boolean {
                    hideKeyboard(ctx)
                    return false
                }
            })
        }

    }


    /**
     * Method use to hide keyboard.
     *
     * @param ctx context of current activity.
     */
    fun hideKeyboard(ctx: Activity) {
        if (ctx.currentFocus != null) {
            val imm = ctx.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(ctx.currentFocus!!.windowToken, 0)
        }
    }
    fun checkQTYExits(productID: Int, qty: Int, db: AppDatabase) : Boolean{
        val date = Date()
        date.hours = 0
        date.minutes = 0
        date.seconds = 0
        val totalQty = db.itemInventery().getTotalQty(productID,date.time)
        return qty <= totalQty
    }fun getTotalQty(productID: Int,  db: AppDatabase) : Int{
        val date = Date()
        date.hours = 0
        date.minutes = 0
        date.seconds = 0
        val totalQty = db.itemInventery().getTotalQty(productID,date.time)
        return totalQty
    }
    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


    fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email)
            .matches()
    }


    /**
     * Get layout width dynamic
     */
    fun getWidth(context: Context): Int {
        val displayMetrics = DisplayMetrics()
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.widthPixels
    }

    /**
     * Get layout height dynamic
     */
    fun getHeight(context: Context): Int {
        val displayMetrics = DisplayMetrics()
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    fun getIPAddress(context: Context): String? {
        val wm = context.applicationContext.getSystemService(WIFI_SERVICE) as WifiManager
        return Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
    }

    fun showDateTimePicker(context: Context, clickListener: (Date?) -> Unit): Date {
        val date = Date()
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd =
            DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, month, day ->
                TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                    val pickedDateTime = Calendar.getInstance()
                    pickedDateTime.set(year, month, day, hour, minute)
                    date.time = pickedDateTime.timeInMillis
                    clickListener(date)
                }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false).show()
            }, year, month, day)

        dpd.datePicker.minDate = date.time;
        dpd.show()
        return date
    }


    fun showDatePickerFeature(context: Context, clickListener: (Date?) -> Unit): Date {
        val date = Date()
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd =
            DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, month, day ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day)
                date.time = pickedDateTime.timeInMillis
                clickListener(date)
            }, year, month, day)

        dpd.datePicker.minDate = date.time;
        dpd.show()
        return date
    }

    fun showAfterDatePickerFeature(context: Context, date: Date, clickListener: (Date?) -> Unit): Date {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd =
            DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, month, day ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day)
                date.time = pickedDateTime.timeInMillis
                clickListener(date)
            }, year, month, day)

        dpd.datePicker.minDate = date.time;
        dpd.show()
        return date
    }

    fun showDatePicker(context: Context, clickListener: (Date?) -> Unit): Date {
        val date = Date()
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd =
            DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, month, day ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day)
                date.time = pickedDateTime.timeInMillis
                clickListener(date)
            }, year, month, day)
        dpd.show()
        return date
    }

    fun toDateTimeString(date: Date?): String {
        val format = SimpleDateFormat(Constans.dateTimeFormat)
        return format.format(date)
    }

    fun toDateString(date: Date?): String {
        val format = SimpleDateFormat(Constans.dateFormat)
        return format.format(date)
    }

    fun longDateToString(date: Long?): String {
        val format = SimpleDateFormat(Constans.dateFormat)
        return format.format(date)
    }

    fun milisecondToDateTime(milliSeconds: Long): String {
        val formatter = SimpleDateFormat(Constans.dateTimeFormat)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }

    fun milisecondToDate(milliSeconds: Long): String {
        val formatter = SimpleDateFormat(Constans.dateFormat)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }

    fun toDate(milliSeconds: Long?): Date {
        val calendar = Calendar.getInstance()
        if (milliSeconds != null) {
            calendar.timeInMillis = milliSeconds
        }
        return calendar.time
    }

    fun showConfirmationDialog(
        context: Activity,
        title: String,
        message: String,
        btnPositive: String,
        btnNegative: String,
        clickListener: (Boolean?) -> Unit
    ) {
        //1 add category, 2 manufacturer , 3 department, 4 brand, 5 SubCagegory
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_confimation_dialog)

        val dialogButton: Button = dialog.findViewById(R.id.btnDone) as Button
        dialogButton.setText(btnPositive)

        val btnNo: Button = dialog.findViewById(R.id.btnNo) as Button

        val txtTitle: TextView = dialog.findViewById(R.id.txtTitle) as TextView
        txtTitle.setText(title)

        val txtMessage: TextView = dialog.findViewById(R.id.txtMessage) as TextView
        txtMessage.setText(message)


        if (btnNegative != null && btnNegative.isNotEmpty()) {
            btnNo.setText(btnNegative)
            btnNo.visibility = View.VISIBLE
        } else {
            btnNo.visibility = View.GONE
        }

        dialogButton.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            hideKeyboard(context)
            clickListener(true)
        })
        btnNo.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            hideKeyboard(context)
            clickListener(false)
        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }

    @SuppressLint("ClickableViewAccessibility")
    fun showAddBarcodeDialog(
        context: Activity,
        btnPositive: String,
        db: AppDatabase,
        barcode: String,
        clickListener: (String?) -> Unit
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_add_barcode_dialog)

        var isExists = false
        val btnClose: TextView = dialog.findViewById(R.id.txtTitle)
        val barcodeText: EditText = dialog.findViewById(R.id.edtBarcode)
        val dialogButton: Button = dialog.findViewById(R.id.btnDone) as Button
        dialogButton.setText(btnPositive)

        btnClose.setOnTouchListener(View.OnTouchListener { _, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action != MotionEvent.ACTION_UP) {
                if (event.rawX >= btnClose.right - btnClose.compoundDrawables
                        .get(DRAWABLE_RIGHT).bounds.width()
                ) {
                    dialog.dismiss()
                    hideKeyboard(context)
                    clickListener("")
                    return@OnTouchListener true
                }
            }
            false
        })

        dialogButton.setOnClickListener(View.OnClickListener {
            if (barcodeText.text.toString().trim().isNotEmpty()) {

                val barcodeAlreadyExists =
                    db.orderBarcodeDao().getOrderIdByBarcode(barcodeText.text.toString().trim())
                if (barcodeAlreadyExists <= 0) {
                    if (barcodeText.text.toString().trim() != barcode) {
                        val barcodeArray = barcodeText.text.toString().trim().split(",")
                        barcodeArray.forEach {
                            isExists = false
                            if(barcode.contains(it)){
                                isExists = true
                            }
                        }
                        Log.d("barcodeArray", barcodeArray.toString())
                        if (isExists) {
                            showToast(context, "This Barcode is Already Selected")
                        } else {
                            dialog.dismiss()
                            hideKeyboard(context)
                            if (barcode.isEmpty()) {
                                clickListener(barcodeText.text.toString().trim())
                            } else {
                                clickListener(
                                    "${barcode}" + "," + "${barcodeText.text.toString().trim()}"
                                )
                            }
                        }
                    } else {
                        showToast(context, "This Barcode is Already Selected")
                    }
                } else {
                    showToast(context, "Barcode Already Exits")
                }

            } else {
                barcodeText.error = "This Field is Requires"
                barcodeText.requestFocus()
            }
        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }

    @SuppressLint("ClickableViewAccessibility")
    fun showSearchBarcodeDialog(
        context: Activity,
        btnPositive: String,
        clickListener: (barcode : String?) -> Unit
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_add_barcode_dialog)

        val btnClose: TextView = dialog.findViewById(R.id.txtTitle)
        val barcodeText: EditText = dialog.findViewById(R.id.edtBarcode)
        val dialogButton: Button = dialog.findViewById(R.id.btnDone) as Button
        dialogButton.text = btnPositive
        btnClose.text = "Search Barcode"
        btnClose.setOnTouchListener(View.OnTouchListener { _, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action != MotionEvent.ACTION_UP) {
                if (event.rawX >= btnClose.right - btnClose.compoundDrawables
                        .get(DRAWABLE_RIGHT).bounds.width()
                ) {
                    dialog.dismiss()
                    hideKeyboard(context)
                    clickListener("")
                    return@OnTouchListener true
                }
            }
            false
        })

        dialogButton.setOnClickListener(View.OnClickListener {
            if (barcodeText.text.toString().trim().isNotEmpty()) {
                dialog.dismiss()
                hideKeyboard(context)
                clickListener(barcodeText.text.toString().trim())
            }else{
                barcodeText.error = context?.getString(R.string.error_msg)
                barcodeText.requestFocus()
            }
        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun showPaymentDialog(
        context: Activity,
        amount: String,
        clickListener: (isSubmit : Boolean,payType: String, discountType: String, discountValue: Double, enterAmount: Double, clName : String, remark : String) -> Unit
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_payment_dialog)

        val btnClose: ImageView = dialog.findViewById(R.id.image_view_cancel)
        val btnSubmit: Button = dialog.findViewById(R.id.btnDone) as Button
        val edtDisAmount: EditText = dialog.findViewById(R.id.edtDiscount)
        val edtSubTotal: EditText = dialog.findViewById(R.id.edtSubTtl)
        val edtTotalAmount: EditText = dialog.findViewById(R.id.edtTotalAmount)
        val lLEnterAmount: View = dialog.findViewById(R.id.ll_enter_amt)
        val edtEnterAmount: EditText = dialog.findViewById(R.id.edtEnterAmount)
        val edtChange: EditText = dialog.findViewById(R.id.edtChange)
        val lLChange: View = dialog.findViewById(R.id.ll_changes)
        val edtClintName: TextView = dialog.findViewById(R.id.edt_clint_name)
        val edtRemark: TextView = dialog.findViewById(R.id.edt_ref_no)
        val radioGropDis: RadioGroup = dialog.findViewById(R.id.discountRadioGroup)
        val radioGropPayment: RadioGroup = dialog.findViewById(R.id.paymentRadioGroup)

        edtSubTotal.setText(amount)
        edtTotalAmount.setText(amount)
        edtChange.setText("£0.0")
        var discountType = ""
        var payType = "cash"

        edtDisAmount.addTextChangedListener {
            calculateDiscountAmount(
                edtDisAmount,
                amount,
                edtTotalAmount,
                radioGropDis.checkedRadioButtonId
            )

            calculateChangeAmount(edtTotalAmount, edtEnterAmount, edtChange)
        }

        edtEnterAmount.addTextChangedListener {
            calculateChangeAmount(edtTotalAmount, edtEnterAmount, edtChange)
        }

        radioGropDis.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            calculateDiscountAmount(
                edtDisAmount,
                amount,
                edtTotalAmount,
                radioGropDis.checkedRadioButtonId
            )

            calculateChangeAmount(edtTotalAmount, edtEnterAmount, edtChange)
        })

        radioGropPayment.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.rb_cash) {
                lLEnterAmount.visibility = View.VISIBLE
                lLChange.visibility = View.VISIBLE
                payType = "cash"

            } else {
                lLEnterAmount.visibility = View.GONE
                lLChange.visibility = View.GONE
                payType = "card"

            }
        })


        btnClose.setOnClickListener {
            dialog.dismiss()
            hideKeyboard(context)
            clickListener(false,"", "", 0.0, 0.0, "", "")
        }

        btnSubmit.setOnClickListener(View.OnClickListener {
                dialog.dismiss()
                hideKeyboard(context)
                var discountValue = 0.0
                var enterAmount = 0.0
                var clName = ""
                var remark = ""
                if (edtDisAmount.text.isNotEmpty()) {
                    discountValue = edtDisAmount.text.toString().trim().toDouble()
                }
                if (edtEnterAmount.text.isNotEmpty()) {
                    enterAmount = edtEnterAmount.text.toString().trim().toDouble()
                }
                if(edtClintName.text.toString().trim().isNotEmpty()){
                    clName = edtClintName.text.toString().trim()
                }
                if(edtRemark.text.toString().trim().isNotEmpty()){
                    remark = edtRemark.text.toString().trim()
                }
                clickListener(
                    true,
                    payType,
                    discountType,
                    discountValue,
                    enterAmount,
                    clName,
                    remark
                )
        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun showItemDialog(
        context: Activity,
        barcode: String,
        productModel: ProductModel,
        clickListener: (String?, productModel: ProductModel) -> Unit
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_item_dialog)
        val db = AppDatabase.getDatabase(context)
        val btnClose: ImageView = dialog.findViewById(R.id.image_view_cancel)
        val btnSubmit: Button = dialog.findViewById(R.id.btnDone) as Button
        val txtItemName : TextView = dialog.findViewById(R.id.txt_item_name)
        val txtLeftStock : TextView = dialog.findViewById(R.id.txtLeftStock)
        val edtQty : EditText = dialog.findViewById(R.id.edt_qty)
        val edtDiscount : EditText = dialog.findViewById(R.id.edt_dis)
        txtLeftStock.setText("Left Stock : ${getTotalQty(productModel.id!!, db)}")
        if (productModel?.product_name != null && productModel?.status == 1)
            txtItemName.setText(productModel?.product_name)

        if(productModel?.seletedQTY != null && productModel?.status == 1)
            edtQty.setText("${productModel.seletedQTY}")

        edtQty.addTextChangedListener {
            if(edtQty.text.toString().trim().isNotEmpty()) {
                Log.d("Edit Text", "${edtQty.text.toString().trim().toInt()}")
                productModel.seletedQTY = edtQty.text.toString().trim().toInt()
            }
        }

        if(productModel.allow_discount == "No") {
            edtDiscount.isFocusable = false
            edtDiscount.isFocusableInTouchMode = false
            edtDiscount.setOnClickListener{
                showToast(context, "Discount not Available for This Product")
            }

        }

        btnClose.setOnClickListener {
            dialog.dismiss()
            hideKeyboard(context)
            clickListener("",productModel)
        }

        btnSubmit.setOnClickListener(View.OnClickListener {

            if(checkQTYExits(productModel.id!!,edtQty.text.toString().trim().toInt(),db))
            {
                dialog.dismiss()
                hideKeyboard(context)
                clickListener(barcode,productModel)
            }
            else
                showToast(context,"This product is out of Stock")

        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun calculateChangeAmount(
        edtTotalAmount: EditText,
        edtEnterAmount: EditText,
        edtChange: EditText
    ) {

        val enterAmount: Double
        val totalAmount = edtTotalAmount.text.toString().trim().removeRange(0, 1).toDouble()


        if (edtEnterAmount.text.toString().trim().isNotEmpty()) {
            enterAmount = edtEnterAmount.text.toString().trim().toDouble()

            if (enterAmount >= totalAmount) {
                val value = decimalFormat(enterAmount - totalAmount)
                edtChange.setText("£${(value)}")
            } else {
                edtChange.setText("£0.0")
            }
        } else {
            edtChange.setText("£0.0")
        }
    }

    fun calculateDiscountAmount(
        edtdiscountAmount: EditText,
        amount: String,
        edttotalAmount: EditText,
        radioButtonid: Int
    ) {
        val discountAmount: Double
        val disAmount: Double
        val totalAmount = amount.trim().removeRange(0, 1).toDouble()
        val type: Int
        var disType = ""
        if (radioButtonid == R.id.rb_falt_dis) {
            disType = "Flat"
            type = 1
            edtdiscountAmount.hint = "Flat"
        } else {
            disType = "Percentage"
            edtdiscountAmount.hint = "%"
            type = 0
        }

        if (edtdiscountAmount.text.isNotEmpty()) {
            edttotalAmount.setText(amount)
            discountAmount = edtdiscountAmount.text.toString().trim().toDouble()
            if (type == 0)
                disAmount = totalAmount.times(discountAmount) / 100.toDouble()
            else
                disAmount = discountAmount

            if (disAmount <= totalAmount) {
                val value = decimalFormat(totalAmount.minus(disAmount))
                edttotalAmount.setText("£${value}")
//                paymentModel.subTotal = amount.trim().removeRange(0, 1).toDouble()
//                paymentModel.disAmount = disAmount
//                paymentModel.totalAmount = value

            } else {
//                paymentModel.totalAmount = amount.trim().removeRange(0, 1).toDouble()
                edttotalAmount.setText(amount)
            }
        } else {
//            paymentModel.totalAmount = amount.trim().removeRange(0, 1).toDouble()
            edttotalAmount.setText(amount)
        }
    }

    fun calculateVAT(amount: Double, qty: Int, vatRate: Double): Double {
        val vat = ((amount * qty) * vatRate) / 100.toDouble()
        return vat
    }
    fun decimalFormat(value : Double): Double {
        return DecimalFormat("##.##").format(value).toDouble()
    }
}