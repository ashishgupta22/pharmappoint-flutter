package com.pharmapoint

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import com.buy786.api.ApiInterface
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

@SuppressLint("StaticFieldLeak")
class AppConfig : Application() {

    companion object {
        var msAppConfig: AppConfig? = null
        var currActivity: Activity? = null
        var mAppContext: Context? = null
        lateinit var pref: SharedPreferences
        lateinit var editor: SharedPreferences.Editor
        val TOKEN = "TOKEN"
        var agreementURL : String? = null;
        private const val REQUEST_TIMEOUT = 60
        var msApiInterface: ApiInterface? = null
        var lastScanedBarcod = ""
        fun getInstance(): Context? {
            return mAppContext
        }
        fun writeToken(data: String) {
            editor.putString(TOKEN, data).apply()
        }
        fun readToken(): String {
            return pref.getString(TOKEN, "123")!!
        }
        fun reCreateAPI() {
            msApiInterface = msAppConfig!!.getRequestQueue().create(ApiInterface::class.java)
        }


    }

    override fun onCreate() {
        super.onCreate()
        msAppConfig = this
        mAppContext = applicationContext

        msApiInterface = getRequestQueue().create(ApiInterface::class.java)
        Log.d("tesssss","created success")
        pref = getInstance()!!.getSharedPreferences("firebasr_token", Context.MODE_PRIVATE)
        editor  = pref.edit()


        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            @SuppressLint("SourceLockedOrientationActivity")
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                currActivity = activity
            }

            override fun onActivityStarted(activity: Activity) {
                currActivity = activity
            }

            override fun onActivityResumed(activity: Activity) {
                currActivity = activity
            }

            override fun onActivityPaused(activity: Activity) {
                currActivity = activity
            }

            override fun onActivityStopped(activity: Activity) {

            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

            }

            override fun onActivityDestroyed(activity: Activity) {

            }
        })
    }
    /**
     * With Header
     */
    private fun getRequestQueue(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(interceptor)
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                    .addHeader("Authorization", "Bearer "+   PreferenceUtils(applicationContext).getString(PreferenceUtils.LOGIN_TOKEN))
                    .build()
            chain.proceed(request)
        }
        httpClient.connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS).build()

        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()
    }


}