package com.pharmapoint.viewmodel

import android.content.Context
import android.view.View
import android.widget.EditText
import androidx.lifecycle.ViewModel
import com.pharmapoint.utlity.Utils
import java.util.*

class LogViewModel(context1: Context) : ViewModel() {

    lateinit var selectDate: Date
    var context: Context = context1

    fun showDatePicker(view: View, dob: EditText) {
        Utils.showDatePickerFeature(context) {
            selectDate = it!!
            dob.setText(Utils.toDateString(selectDate))
        }
    }
}