package com.pharmapoint.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.BrandModel
import com.pharmapoint.model.CategoryModel
import com.pharmapoint.model.DepartmentModel
import com.pharmapoint.model.ManufactureModel

class AddProcutViewModel() : ViewModel() {

    var categoryList = MutableLiveData<List<CategoryModel>>()
    var subCategoryList = MutableLiveData<List<CategoryModel>>()
    var manufacturerList = MutableLiveData<List<ManufactureModel>>()
    var departmentList = MutableLiveData<List<DepartmentModel>>()
    var brandList = MutableLiveData<List<BrandModel>>()

    init {

    }
   fun getCategory(database: AppDatabase) :MutableLiveData<List<CategoryModel>> {
       categoryList.value = database.categoryDao().getActiveCategoryList()
        return this.categoryList
    }
    fun getSubCategory(database: AppDatabase,parentId:Int) :MutableLiveData<List<CategoryModel>> {
        subCategoryList.value = database.categoryDao().getSubCategoryListByID(parentId)
        return this.subCategoryList
    }
    fun getManufacturer(database: AppDatabase) :MutableLiveData<List<ManufactureModel>> {
        manufacturerList.value = database.manufactureDao().getActiveManufactureList()
        return this.manufacturerList
    }   fun getDepartment(database: AppDatabase) :MutableLiveData<List<DepartmentModel>> {
        departmentList.value = database.departmentDao().getActiveDepartmentList()
        return this.departmentList
    }  fun getBrand(database: AppDatabase) :MutableLiveData<List<BrandModel>> {
        brandList.value = database.brandDao().getActiveBrandList()
        return this.brandList
    }
}