package com.pharmapoint.viewmodel

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.pharmapoint.R
import com.pharmapoint.activity.LoginActivity
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.LogModel
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils

class MainActivityViewModel(context1: Activity, db: AppDatabase) : ViewModel() {
    val db1 = db
    val context =  context1
    private var handler: Handler? = null
    private var r: Runnable? = null
    private var isPinShowing = false

    init {

    }
    fun breakClick(){
        updateLog(Constans.log_type_breakIn)
        if(!isPinShowing) {
            isPinShowing = true
            showPinDialog()
        }
    }
    fun logoutClick(){
        Utils.showConfirmationDialog(context,"Logout","Are you sure to want to logout.","Logout","No"){ it ->
            if(it!!){
                updateLogout(Constans.log_type_logout)
            }
        }
    }
    fun showPinDialog() {
        //1 add category, 2 manufacturer , 3 department, 4 brand, 5 SubCagegory
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_pin_dialog)
        val edtTitle : EditText = dialog.findViewById(R.id.edtPIN) as EditText
        val dialogButton: Button = dialog.findViewById(R.id.btnSave) as Button
        dialogButton.setOnClickListener(View.OnClickListener {
            if(edtTitle.text.toString().trim().isEmpty() && edtTitle.text.toString().trim().length !=6){
                Utils.showToast(context,"Enter Pin")
            }else {
                val  mPIN  = PreferenceUtils(context).getInteger(PreferenceUtils.LOGIN_MPIN).toString()
                if(mPIN == edtTitle.text.toString().trim()){
                    isPinShowing = false
                    dialog.dismiss()
                    checkBreakOut()
                    startHandler()
                    Utils.showToast(context, "PIN Success")
                }else {
                    edtTitle.setText("")
                    Utils.showToast(context,"Wrong Pin")
                }
            }
        })
        val window = dialog.getWindow();
        window?.setBackgroundDrawableResource(R.color.transparant);
        window?.setGravity(Gravity.CENTER);
        dialog.show()
    }

    private fun updateLog(logTypeLogin: Int?) {
        val time = System.currentTimeMillis()
        val logModel = LogModel()
        logModel.created_at = time
        logModel.user_id = PreferenceUtils(context).getInteger(PreferenceUtils.USER_ID)
        logModel.log_time_in = time
        logModel.type = logTypeLogin
        val id = db1.logDao().insert(logModel).toInt()
        PreferenceUtils(context).setInteger(PreferenceUtils.BREAK_TIME_ID,id)

    }
    private fun updateLogout(logTypeLogin: Int?) {
        val time = System.currentTimeMillis()
        val logModel  =  db1.logDao().getBytID(PreferenceUtils(context).getInteger(PreferenceUtils.LOGINTIME_ID))
        if(logModel != null){
            logModel.log_time_out = time
            db1.logDao().update(logModel)
        }
        PreferenceUtils(context).setBoolean(PreferenceUtils.LOGIN_STATUS,false)
        val inttent = Intent(context,LoginActivity::class.java)
        context.startActivity(inttent)
        context.finish()
    }
    private fun checkBreakOut() {
        val time = System.currentTimeMillis()
        val  logModel =  db1.logDao().getBytID(PreferenceUtils(context).getInteger(PreferenceUtils.BREAK_TIME_ID))
        if(logModel != null) {
            logModel.log_time_out = time
            db1.logDao().update(logModel)
        }
    }
    fun stopHandler() {
        handler?.removeCallbacks(r!!)
        Log.d("HandlerRun", "stopHandlerMain")
    }

    fun startHandler() {
        handler?.postDelayed(r!!, 60 * 1000)
        Log.d("HandlerRun", "startHandlerMain")
    }
    fun bindHandler() {
        handler = Handler()
        r = Runnable {
            Log.d("HandlerRun", "Logged out after 3 minutes on inactivity.")
            Toast.makeText(context,
                "You are logged out.",
                Toast.LENGTH_SHORT
            ).show()
            if(!isPinShowing) {
                isPinShowing = true
                showPinDialog()
            }

        }
    }
}