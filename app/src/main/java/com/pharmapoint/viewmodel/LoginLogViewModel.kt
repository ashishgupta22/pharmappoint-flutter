package com.pharmapoint.viewmodel

import android.content.Context
import android.view.View
import android.widget.EditText
import androidx.lifecycle.ViewModel
import com.pharmapoint.adapter.LoginLogsAdapter
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.LogModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import java.util.*

class LoginLogViewModel(context1: Context) : ViewModel() {

    private lateinit var db: AppDatabase
    var fromDate =  Date()
    var toDate = Date()
    var context: Context = context1
    var list = mutableListOf<LogModel>()
    var listTemp = mutableListOf<LogModel>()
    var appoinmentAdapter = LoginLogsAdapter(context1,list){type, position ->


    }

    fun showDatePicker(view: View, dob: EditText,typ : Int) {
        Utils.showDatePicker(context) {
            if(typ == 1) {
                fromDate = it!!
                dob.setText(Utils.toDateString(fromDate))
            }else if (typ == 2) {
                toDate = it!!
                dob.setText(Utils.toDateString(toDate))
            }
        }
    }

    fun bindData() {
        db = AppDatabase.getDatabase(context)
        getLogList()
    }

    fun searchClick(){
        getLogList()
    }
    fun getLogList() {
        fromDate.hours = 0
        fromDate.minutes = 0
        fromDate.seconds = 0
        toDate.hours = 23
        toDate.minutes = 59
        toDate.seconds = 59
        list.clear()
        listTemp.clear()
        val userid = PreferenceUtils(context).getInteger(PreferenceUtils.USER_ID)
        list.addAll(db.logDao().getLoginLogsList(userid,fromDate.time, toDate.time))
        listTemp.addAll(list)
        appoinmentAdapter.notifyDataSetChanged()
    }
}