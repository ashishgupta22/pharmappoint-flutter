package com.pharmapoint.viewmodel

import android.content.Context
import android.text.TextUtils
import android.view.View
import androidx.lifecycle.ViewModel
import android.widget.EditText
import android.widget.Spinner
import androidx.core.view.isEmpty
import com.pharmapoint.R
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.CustomerModel
import com.pharmapoint.utlity.PreferenceUtils
import com.pharmapoint.utlity.Utils
import java.util.*


class AddCustomerViewModel(context1: Context, db1: AppDatabase) : ViewModel() {
    lateinit var selectDate: Date
    val context: Context = context1
   val db: AppDatabase = db1
    fun showDatePicker(view: View, dob: EditText) {
        Utils.showDatePickerFeature(context) {
            selectDate = it!!
            dob.setText(Utils.toDateString(selectDate))
        }
    }

    fun btnCreateCustomer(view: View, etxtTitle: Spinner, etxtName: EditText, etxtSurName: EditText, etxtDob: EditText, etxtEmail: EditText, edtCity: EditText, edtCountry: EditText, edtPostCode: EditText,edtPhoneNo:EditText,edtAddress:EditText) {
        val email = etxtEmail.text.toString().trim()
        var title = ""
        val surName = etxtSurName.text.toString().trim()
        val name = etxtName.text.toString().trim()
        val dob = etxtDob.text.toString().trim()
        if(etxtTitle.selectedItemPosition == 0){
            title = "Mr"
        }else{
            title = "Miss"
        }
         if (etxtTitle.isEmpty()) {
            Utils.showToast(context, view.context.getString(R.string.error_msg))
        } else if (TextUtils.isEmpty(name)) {
            etxtName.error = view.context.getString(R.string.error_msg)
            etxtName.requestFocus()
        } else if (TextUtils.isEmpty(surName)) {
            etxtSurName.error = view.context.getString(R.string.error_msg)
            etxtSurName.requestFocus()
        } else if (!TextUtils.isEmpty(email) && !Utils.isValidEmail(email)) {
            etxtEmail.error = view.context.getString(R.string.invalid_mail)
            etxtEmail.requestFocus()
        } else if(!this::selectDate.isInitialized){
            Utils.showToast(context, view.context.getString(R.string.select_dob))
        } else {
            Utils.showSnackBar(
                view,
                "Customer Created Successfully"
            )
            val time = System.currentTimeMillis()
            val customerModel = CustomerModel()
            customerModel.title = title
            customerModel.customer_f_name = etxtName.text.toString().trim()
            customerModel.customer_l_name = etxtSurName.text.toString().trim()
            customerModel.customer_email = etxtEmail.text.toString().trim()
            customerModel.customer_mobile = edtPhoneNo.text.toString().trim()
            customerModel.dob = selectDate.time
            customerModel.city = edtCity.text.toString().trim()
            customerModel.country = edtCountry.text.toString().trim()
            customerModel.postcode = edtPostCode.text.toString().trim()
            customerModel.address = edtAddress.text.toString().trim()
            customerModel.status = 1
            customerModel.created_at = time
            customerModel.updated_at = time
            customerModel.user_id = PreferenceUtils(context).getInteger(PreferenceUtils.USER_ID)
            db.customerDao().insert(customerModel)
            etxtName.setText("")
            etxtSurName.setText("")
            etxtDob.setText("")
            etxtEmail.setText("")
            edtCity.setText("")
            edtCountry.setText("")
            edtPostCode.setText("")
            edtPhoneNo.setText("")
            edtAddress.setText("")
        }
    }

}