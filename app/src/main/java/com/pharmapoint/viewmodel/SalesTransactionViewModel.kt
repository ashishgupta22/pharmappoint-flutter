package com.pharmapoint.viewmodel

import android.content.Context
import android.view.View
import android.widget.EditText
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.OrderModel
import com.pharmapoint.utlity.Utils
import java.util.*

class SalesTransactionViewModel(context1: Context) : ViewModel() {

    lateinit var selectDate: Date
    lateinit var selectDate1: Date
    var context: Context = context1
    private lateinit var db: AppDatabase
    var listData = MutableLiveData<List<OrderModel>>()
    fun showDatePicker(view: View, dob: EditText) {
        Utils.showDatePicker(context) {
            selectDate = it!!
            selectDate1 = selectDate
            dob.setText(Utils.toDateString(selectDate))
        }
    }

    fun showAfterDatePickerFeature(view: View, dob: EditText) {
        Utils.showAfterDatePickerFeature(context,selectDate1) {
            selectDate = it!!
            dob.setText(Utils.toDateString(selectDate))
        }
    }

    fun getTransactions(requireActivity: FragmentActivity) {
        db = AppDatabase.getDatabase(requireActivity)
        listData.value = db.orderDao().getOrderList()
    }
}