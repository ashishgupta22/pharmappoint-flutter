package com.pharmapoint.viewmodel

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.buy786.api.ApiRepository
import com.pharmapoint.R
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.LogModel
import com.pharmapoint.ui_fragment.login_register.ForgotPasswordFragment
import com.pharmapoint.ui_fragment.login_register.RegisterFragment
import com.pharmapoint.utlity.Connectivity
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.Utils

class LoginViewModel(context1: Context) : ViewModel() {
    var  context: Context = context1;
    private val apiRepository: ApiRepository = ApiRepository()
    private lateinit var db: AppDatabase


    fun btnLoginClick(view : View, etxtEmail: EditText, etxtPassword : EditText){
        val email = etxtEmail.text.toString()
        val password = etxtPassword.text.toString()


        if (TextUtils.isEmpty(email.trim())) {
            etxtEmail.error = view.context.getString(R.string.error_msg)
            etxtEmail.requestFocus()
        } else if (!Utils.isValidEmail(email.trim())) {
            etxtEmail.error = view.context.getString(R.string.invalid_mail)
            etxtEmail.requestFocus()
        }else if (TextUtils.isEmpty(password.trim())) {
            etxtPassword.error = view.context.getString(R.string.error_msg)
            etxtPassword.requestFocus()
        }  else {
            if (Connectivity.isConnected(view.context)) {
                initLogin(context, email, password, "213474")
            } else {
                Utils.showSnackBar(
                    view,
                    view.context.resources.getString(R.string.internet_error_msg)
                )
            }
        }
    }
    fun btnSignUp(view : View,){
        (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(R.id.container, RegisterFragment.newInstance()).commit()
    }
    fun btnForgotPassword(view : View,){
        (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(R.id.container, ForgotPasswordFragment.newInstance()).commit()
    }

    private fun initLogin(
        context: Context,
        email: String,
        password: String,
        firebaseToken: String
    ) {
        var params = HashMap<String,String>();
        params.put(Constans.userEmail,email)
        params.put(Constans.fcmToken,firebaseToken)
        params.put(Constans.userPassword,password)
        params.put(Constans.deviceName,"2134")
        Log.d("LoginRequest",params.toString());
        apiRepository.loginAPI(
            context,
            params
        )
    }

}