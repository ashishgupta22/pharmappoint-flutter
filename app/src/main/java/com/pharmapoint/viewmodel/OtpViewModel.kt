package com.pharmapoint.viewmodel

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.buy786.api.ApiRepository
import com.pharmapoint.R
import com.pharmapoint.ui_fragment.login_register.ForgotPasswordFragment
import com.pharmapoint.ui_fragment.login_register.LoginFragment
import com.pharmapoint.utlity.Connectivity
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.Utils

class OtpViewModel(context1: Context) : ViewModel() {
    var  context: Context = context1;
    private val apiRepository: ApiRepository = ApiRepository()


    fun btnSubmitClick(view : View, etxtOtp: EditText){
        val otp = etxtOtp.text.toString().trim()

        if (TextUtils.isEmpty(otp)) {
            etxtOtp.error = view.context.getString(R.string.error_msg)
            etxtOtp.requestFocus()
        } else {
            if (Connectivity.isConnected(view.context)) {
                initVerifyOtp(context, otp)
            } else {
                Utils.showSnackBar(
                    view,
                    view.context.resources.getString(R.string.internet_error_msg)
                )
            }
        }
    }
    fun btnBackClick(view : View,){
        (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(R.id.container, ForgotPasswordFragment.newInstance()).commit()
    }

    private fun initVerifyOtp(
        context: Context,
        otp: String,
    ) {
        val params = HashMap<String,String>();
        params.put(Constans.userEmail,otp)
        Log.d("VerifyOtpRequest",params.toString());
        apiRepository.verifyOtpAPI(
            context,
            params
        )
    }

}