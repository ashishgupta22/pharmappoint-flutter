package com.pharmapoint.viewmodel

import android.content.Context
import android.view.View
import android.widget.EditText
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.Item_inventoryModel
import com.pharmapoint.model.OrderDetailsModel
import com.pharmapoint.utlity.Utils
import java.util.*

class SoldProductHistoryViewModel(context1: Context) : ViewModel() {

    lateinit var selectDateFrom: Date
    lateinit var selectDateTo: Date
    var context: Context = context1
    var listData = MutableLiveData<List<OrderDetailsModel>>()
    private lateinit var db: AppDatabase

    fun showDatePicker(view: View, dob: EditText,type : Int) {
        //1  =  from Date 2 = to date
        Utils.showDatePickerFeature(context) {
            if(type == 1)
                selectDateFrom = it!!
            else if(type ==2)
                selectDateTo = it!!
            dob.setText(Utils.toDateString(it))
        }
    }

    fun onClickSerach(view: View,editSearch:EditText) {
        if(selectDateFrom != null && selectDateFrom.time > 0){
             if (selectDateTo != null && selectDateTo.time > 0){
                 getTransactionsByFilter(view.context,editSearch.text.toString().trim())
             }else
            Utils.showToast(view.context,"Select To Date")
        }else{
            Utils.showToast(view.context,"Select From Date")

        }
    }

    fun getTransactions(requireActivity: FragmentActivity) {
        val date = Date()
        date.hours = 0
        date.minutes = 0
        date.seconds = 0
        db = AppDatabase.getDatabase(requireActivity)
        listData.value = db.orderDetailsDao().getOrderDetailListForSold()
//        inventryList.value = db.orderDetailsDao().getOrderList()
    }

    fun getTransactionsByFilter(requireActivity: Context,searchText : String) {
        val date = Date()
        date.hours = 0
        date.minutes = 0
        date.seconds = 0
        db = AppDatabase.getDatabase(requireActivity)
        listData.value = db.orderDetailsDao().getOrderDetailListForSoldFilter(selectDateFrom.time,selectDateTo.time,searchText)
//        inventryList.value = db.orderDetailsDao().getOrderList()
    }
}