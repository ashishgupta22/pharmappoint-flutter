package com.pharmapoint.viewmodel

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.ViewModel
import com.buy786.api.ApiRepository
import com.pharmapoint.R
import com.pharmapoint.ui_fragment.login_register.LoginFragment
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.Utils
import android.widget.Toast

import com.pharmapoint.activity.MainActivity

import android.text.style.ClickableSpan




class SignUpViewModel(context1: Context) : ViewModel() {
    var  context: Context = context1;
    private val apiRepository: ApiRepository = ApiRepository()


    fun btnSignupClick(view : View, etxtName: EditText, etxtEmail: EditText, etxtPassword : EditText, checkBox: CheckBox){
        val name = etxtName.text.toString().trim()
        val isChecked = checkBox.isChecked
        val email = etxtEmail.text.toString().trim()
        val password = etxtPassword.text.toString().trim()


        if (TextUtils.isEmpty(name)) {
            etxtName.error = view.context.getString(R.string.error_msg)
            etxtName.requestFocus()
        } else if (TextUtils.isEmpty(email)) {
            etxtEmail.error = view.context.getString(R.string.error_msg)
            etxtEmail.requestFocus()
        } else if (!Utils.isValidEmail(email)) {
            etxtEmail.error = view.context.getString(R.string.invalid_mail)
            etxtEmail.requestFocus()
        } else if (TextUtils.isEmpty(password)) {
            etxtPassword.error = view.context.getString(R.string.error_msg)
            etxtPassword.requestFocus()
        } else if (!isChecked) {
            Utils.showToast(context, "Please agree with our terms and conditions")
        } else{
            (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginFragment.newInstance()).commit()
        }
    }
    fun btnLoginClick(view: View){
        (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginFragment.newInstance()).commit()
    }
    fun btnTermServices(view: View, text: TextView) {
        val text1 = text.text
        val ss = SpannableString(text1)
        val clickableSpan1: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.nhs.uk/conditions/"))
                startActivity(context, i, null)
            }
        }
        ss.setSpan(clickableSpan1, 42, 58, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        text.text = ss
        text.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun initLogin(
        context: Context,
        email: String,
        password: String,
        firebaseToken: String
    ) {
        var params = HashMap<String,String>();
        params.put(Constans.userEmail,email)
        params.put(Constans.fcmToken,firebaseToken)
        params.put(Constans.userPassword,password)
        params.put(Constans.deviceName,"2134")
        Log.d("LoginRequest",params.toString());
        apiRepository.loginAPI(
            context,
            params
        )
    }

}