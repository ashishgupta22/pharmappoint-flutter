package com.pharmapoint.viewmodel

import android.content.Context
import android.view.View
import android.widget.EditText
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.AppointmentServicesModel
import com.pharmapoint.utlity.Utils
import java.util.*

class AppointmentViewModel(context1: Context) : ViewModel() {
    lateinit var selectDate: Date
    var context: Context = context1
    var surviceList = MutableLiveData<List<AppointmentServicesModel>>()

    fun showDatePicker(view: View, dob: EditText) {
        Utils.showDatePickerFeature(context) {
            selectDate = it!!
            dob.setText(Utils.toDateString(selectDate))
        }
    }

    fun getServiceList(database: AppDatabase) : MutableLiveData<List<AppointmentServicesModel>> {
        surviceList.value = database.appointmentServicesDao().getAppointmentServicesList()
        return this.surviceList
    }


}