package com.pharmapoint.viewmodel

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.buy786.api.ApiRepository
import com.pharmapoint.R
import com.pharmapoint.ui_fragment.login_register.LoginFragment
import com.pharmapoint.utlity.Connectivity
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.Utils

class ForgotPasswordViewModel(context1: Context) : ViewModel() {
    var  context: Context = context1;
    private val apiRepository: ApiRepository = ApiRepository()


    fun btnContinueClick(view : View, etxtEmail: EditText){
        val email = etxtEmail.text.toString()


        if (TextUtils.isEmpty(email)) {
            etxtEmail.error = view.context.getString(R.string.error_msg)
            etxtEmail.requestFocus()
        } else if (!Utils.isValidEmail(email)) {
            etxtEmail.error = "Please enter valid email"
            etxtEmail.requestFocus()
        }  else {
            if (Connectivity.isConnected(view.context)) {
                initForgotPassword(context, email)
            } else {
                Utils.showSnackBar(
                    view,
                    view.context.resources.getString(R.string.internet_error_msg)
                )
            }
        }
    }
    fun btnLoginClick(view : View,){
        (context as AppCompatActivity).getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginFragment.newInstance()).commit()
    }

    private fun initForgotPassword(
        context: Context,
        email: String,
    ) {
        val params = HashMap<String,String>()
        params.put(Constans.userEmail,email)
        Log.d("ForgotPasswordRequest",params.toString());
        apiRepository.forgotPasswordAPI(
            context,
            params
        )
    }

}