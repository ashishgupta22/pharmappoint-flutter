package com.pharmapoint.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmapoint.database.AppDatabase
import com.pharmapoint.model.ProductModel

class InventryViewModel : ViewModel() {
    var productList = MutableLiveData<List<ProductModel>>()

    init {

    }

    fun getInventoryList(database: AppDatabase) : MutableLiveData<List<ProductModel>> {
        productList.value = database.productDao().getActiveProductList()
        return this.productList
    }
}