package com.pharmapoint.model

data class ProductDetailModel (
    val amount:String,
    var vat : String,
    val prince : String,
    val qty : Int,
    val medName : String,
    val discount : String
)
