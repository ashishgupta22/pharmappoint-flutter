package com.pharmapoint.model

data class ForgotPasswordResponseBean (
    val error: Boolean,
    val errorCode: Int,
    val message: String,
    val state: String
)