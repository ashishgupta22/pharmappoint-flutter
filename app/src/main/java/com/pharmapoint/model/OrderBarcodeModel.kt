package com.pharmapoint.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "order_barcode_table")
class OrderBarcodeModel {

    @PrimaryKey
    var id: Int? = null

    @SerializedName("productId")
    @Expose
    var productId: Int? = null

    @SerializedName("barcode")
    @Expose
    var barcode: String? = null

    @SerializedName("created_at")
    @Expose
    var created_at: Long? = null

}