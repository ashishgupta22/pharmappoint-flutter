package com.pharmapoint.model

data class AppointmentDetailModel (
    val name:String,
    var email : String,
    val date_time : String,
    val comment : String,
    val status : String,
    val action : String
)
