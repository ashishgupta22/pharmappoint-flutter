package com.pharmapoint.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "product_table")
class ProductModel {

        @PrimaryKey
        var id: Int? = null

        @SerializedName("item_code")
        @Expose
        var item_code: String? = null

        @SerializedName("g_tin")
        @Expose
        var g_tin: String? = null

        @SerializedName("product_name")
        @Expose
        var product_name: String? = null

        @SerializedName("product_description")
        @Expose
        var product_description: String? = null

        @SerializedName("category_id")
        @Expose
        var category_id: Int? = null

        @SerializedName("user_id")
        @Expose
        var user_id: Int? = null

        @SerializedName("sub_category_id")
        @Expose
        var sub_category_id: Int? = null

        @SerializedName("manufacturer_id")
        @Expose
        var manufacturer_id: Int? = null

        @SerializedName("brand_id")
        @Expose
        var brand_id: Int? = null

        @SerializedName("department_id")
        @Expose
        var department_id: Int? = null

        @SerializedName("pack_size")
        @Expose
        var pack_size: Int? = null



        @SerializedName("case_size")
        @Expose
        var case_size: Int? = null

        @SerializedName("reorder_level")
        @Expose
        var reorder_level: Int? = null

        @SerializedName("reorder_qty")
        @Expose
        var reorder_qty: Int? = null

        @SerializedName("case_cost")
        @Expose
        var case_cost: Double? = null

        @SerializedName("unit_cost")
        @Expose
        var unit_cost: Double? = null

        @SerializedName("retail_cost")
        @Expose
        var retail_cost: Double? = null

        @SerializedName("buying_from")
        @Expose
        var buying_from: String? = null

        @SerializedName("vat")
        @Expose
        var vat: Double? = null

        @SerializedName("allow_discount")
        @Expose
        var allow_discount: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("expire_date")
        @Expose
        var expire_date: Long? = null

        @SerializedName("deleted_at")
        @Expose
        var deleted_at: Long? = null

        @SerializedName("created_at")
        @Expose
        var created_at: Long? = null

        @SerializedName("updated_at")
        @Expose
        var updated_at: Long? = null

        var seletedQTY =1
        var discountAmouont  = 0.0
        var discount  = 0.0

        var isSelected = false
        var restQty = 0
}