package com.pharmapoint.model

data class InventoryDetailModel(
    val medName:String,
    var price: Double?,
    val vat: Double,
    val allowDiscount: String,
    val stock: Int,
    val status: String,
)
