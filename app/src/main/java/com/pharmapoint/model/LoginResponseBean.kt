package com.pharmapoint.model

data class LoginResponseBean(
    val `data`: LoginResponseData,
    val error: Boolean,
    val errorCode: Int,
    val message: String,
    val state: String

)
