package com.pharmapoint.model

data class VerifyOtpResponseBean (
    val error: Boolean,
    val errorCode: Int,
    val message: String,
    val state: String
)