package com.pharmapoint.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "department_table")
class DepartmentModel {

        @PrimaryKey
        var id: Int? = null


        @SerializedName("title")
        @Expose
        var title: String? = null


        @SerializedName("comment")
        @Expose
        var comment: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("user_id")
        @Expose
        var user_id: Int? = null

        @SerializedName("deleted_at")
        @Expose
        var deleted_at: Long? = null
        @SerializedName("created_at")
        @Expose
        var created_at: Long? = null

        @SerializedName("updated_at")
        @Expose
        var updated_at: Long? = null

}