package com.pharmapoint.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "customer_table")
class CustomerModel {

        @PrimaryKey
        var id: Int? = null

        @SerializedName("titlle")
        @Expose
        var title: String? = null

        @SerializedName("customer_f_name")
        @Expose
        var customer_f_name: String? = null
        @SerializedName("customer_l_name")
        @Expose
        var customer_l_name: String? = null

        @SerializedName("customer_mobile")
        @Expose
        var customer_mobile: String? = null

        @SerializedName("customer_email")
        @Expose
        var customer_email : String? = null

        @SerializedName("comment")
        @Expose
        var comment: String? = null

        @SerializedName("city")
        @Expose
        var city: String? = null
        @SerializedName("country")
        @Expose
        var country: String? = null
          @SerializedName("postcode")
        @Expose
        var postcode: String? = null
     @SerializedName("address")
        @Expose
        var address: String? = null

        @SerializedName("dob")
        @Expose
        var dob: Long? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("user_id")
        @Expose
        var user_id: Int? = null

        @SerializedName("deleted_at")
        @Expose
        var deleted_at: String? = null
        @SerializedName("created_at")
        @Expose
        var created_at: Long? = null

        @SerializedName("updated_at")
        @Expose
        var updated_at: Long? = null

}