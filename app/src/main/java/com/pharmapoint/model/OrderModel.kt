package com.pharmapoint.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "order_table")
class OrderModel {

    @PrimaryKey
    var id: Int? = null

    @SerializedName("invoiceNo")
    @Expose
    var invoiceNo: Int? = null

    @SerializedName("paidAmount")
    @Expose
    var paidAmount: Double? = null

    @SerializedName("order_date")
    @Expose
    var order_date: Long? = null

    @SerializedName("discount")
    @Expose
    var invoiceDiscount: Double? = null

    @SerializedName("clintName")
    @Expose
    var clintName: String? = null

    @SerializedName("discountAmount")
    @Expose
    var discountAmount: Double? = null

    @SerializedName("netAmount")
    @Expose
    var netAmount: Double? = null

    @SerializedName("itemQty")
    @Expose
    var itemQty: Int? = null

    @SerializedName("referenceInvoice")
    @Expose
    var referenceInvoice: Int? = null

    @SerializedName("counterNo")
    @Expose
    var counterNo: Int? = null

    @SerializedName("userId")
    @Expose
    var userId: Int? = null

    @SerializedName("series")
    @Expose
    var series: String? = null

    @SerializedName("payStatus")
    @Expose
    var payStatus: String? = null

    @SerializedName("payMethod")
    @Expose
    var payMethod: String? = null

    @SerializedName("remark")
    @Expose
    var remark: String? = null

    @SerializedName("change_amount")
    @Expose
    var change_amount: Double? = null

    @SerializedName("deleted_at")
    @Expose
    var deleted_at: String? = null

    @SerializedName("created_at")
    @Expose
    var created_at: Long? = null

    @SerializedName("updated_at")
    @Expose
    var updated_at: Long? = null

}