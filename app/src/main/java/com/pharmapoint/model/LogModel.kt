package com.pharmapoint.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "log_table")
class LogModel {

        @PrimaryKey
        var id: Int? = null

        @SerializedName("log_time_in")
        @Expose
        @ColumnInfo(name = "log_time_in")
        var log_time_in: Long? = null

        @SerializedName("log_time_out")
        @Expose
        @ColumnInfo(name = "log_time_out")
        var log_time_out: Long? = null

        @SerializedName("type")
        @Expose
        @ColumnInfo(name = "type")
        var type: Int? = null

        @SerializedName("user_id")
        @Expose
        @ColumnInfo(name = "user_id")
        var user_id: Int? = null

        @SerializedName("created_at")
        @Expose
        @ColumnInfo(name = "created_at")
        var created_at: Long? = null

        var name: String? = null

}