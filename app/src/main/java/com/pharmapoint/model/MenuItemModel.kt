package com.pharmapoint.model

import android.graphics.drawable.Drawable
import androidx.fragment.app.Fragment

data class MenuItemModel (
    val menuName:String,
    var isSelected : Boolean = false,
    val menuId : Int = 0,
    val icon : Int,
    val tabList : MutableList<TabtemModel>,
    val fragmentList : MutableList<Fragment>
)
data class TabtemModel (
    val menuName:String,
    var isSelected : Boolean = false,
    val menuId : Int = 0,
    val icon : Drawable,
)