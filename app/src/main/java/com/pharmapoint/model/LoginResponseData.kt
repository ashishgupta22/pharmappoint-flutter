package com.pharmapoint.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user_table")
data class LoginResponseData(

    @PrimaryKey
    var id: Int? = null,

    @SerializedName("email")
    @Expose
    val email: String,

    @SerializedName("mobile_no")
    @Expose
    val mobile_no: String,

    @SerializedName("name")
    @Expose
    val name: String,

    @SerializedName("status")
    @Expose
    val status: Boolean,

    @SerializedName("token")
    @Expose
    val token: String,

    @SerializedName("user_id")
    @Expose
    val user_id: Int,

    @SerializedName("mpin")
    @Expose
    val mpin: Int = 0,

    @SerializedName("user_type")
    @Expose
    val user_type: String


)