package com.pharmapoint.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "appointment_table")
class AppoinmentModel {

        @PrimaryKey
        var id: Int? = null

        @SerializedName("appointment_no")
        @Expose
        var appointment_no: Int? = null

        @SerializedName("service_id")
        @Expose
        var service_id: Int? = null

        @SerializedName("user_id")
        @Expose
        var user_id: Int? = null

        @SerializedName("customer_name")
        @Expose
        var customer_name: String? = null

        @SerializedName("customer_mobile")
        @Expose
        var customer_mobile: String? = null

        @SerializedName("customer_id")
        @Expose
        var customer_id: Int? = null

        @SerializedName("comment")
        @Expose
        var comment: String? = null
        @SerializedName("appointment_date")
        @Expose
        var appointment_date: Long? = null

        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("deleted_at")
        @Expose
        var deleted_at: String? = null
        @SerializedName("created_at")
        @Expose
        var created_at: Long? = null

        @SerializedName("updated_at")
        @Expose
        var updated_at: Long? = null

}