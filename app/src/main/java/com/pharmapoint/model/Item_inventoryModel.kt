package com.pharmapoint.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "item_inventory_table")
class Item_inventoryModel {

        @PrimaryKey
        var id: Int? = null


        @SerializedName("product_id")
        @Expose
        var product_id: Int? = null


        @SerializedName("quantity")
        @Expose
        var quantity: Int? = null

        @SerializedName("rest_qty")
        @Expose
        var rest_qty: Int? = null

        @SerializedName("user_id")
        @Expose
        var user_id: Int? = null

        @SerializedName("lot_no")
        @Expose
        var lot_no: String? = null

        @SerializedName("expire_date")
        @Expose
        var expire_date: Long? = null

        @SerializedName("created_at")
        @Expose
        var created_at: Long? = null

        @SerializedName("updated_at")
        @Expose
        var updated_at: Long? = null

}