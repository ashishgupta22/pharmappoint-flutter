package com.pharmapoint.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "order_details_table")
class OrderDetailsModel {

    @PrimaryKey
    var id: Int? = null

    @SerializedName("invoiceNo")
    @Expose
    var invoiceNo: Int? = null

    @SerializedName("productId")
    @Expose
    var productId: Int? = null

    @SerializedName("qty")
    @Expose
    var qty: Int? = null

    @SerializedName("price")
    @Expose
    var price: Double? = null

    @SerializedName("salePrice")
    @Expose
    var salePrice: Double? = null

    @SerializedName("discount")
    @Expose
    var discount: Double? = null

    @SerializedName("discountAmount")
    @Expose
    var discountAmount: Double? = null

    @SerializedName("vat")
    @Expose
    var vat: Double? = null

    @SerializedName("deleted_at")
    @Expose
    var deleted_at: String? = null

    @SerializedName("created_at")
    @Expose
    var created_at: Long? = null

    @SerializedName("updated_at")
    @Expose
    var updated_at: Long? = null

    var item_code: String? = ""

    var restQty : Int? = 0

}