package com.pharmapoint.adapter

import android.content.Context
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R
import com.pharmapoint.model.MenuItemModel
import kotlinx.android.synthetic.main.layout_menu.view.*


class LeftMenuAdapter(
    private val context: Context,
    private val leftMenuItemList: MutableList<MenuItemModel>,
    private val clickListener: (Int?) -> Unit
) : RecyclerView.Adapter<LeftMenuAdapter.ViewHolder>() {



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val restaurantName: TextView = view.txtStatus
        val llMain: LinearLayout = view.llMenu
        val img_icon: ImageView = view.img_logo
        val view_selected  : View = view.view_selected


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_menu, parent, false))
    }

    override fun getItemCount(): Int {
        return leftMenuItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val displaymetrics = DisplayMetrics()
//        (context as Activity).windowManager.defaultDisplay.getMetrics(displaymetrics)

        val selectedColor: Int
        if(leftMenuItemList[position].isSelected){
            selectedColor = context.resources.getColor(R.color.selectedMuneuColor)
            holder.view_selected.visibility = View.VISIBLE
        }else{
            selectedColor = context.resources.getColor(R.color.color_menu_left)
            holder.view_selected.visibility = View.GONE
        }
        holder.restaurantName.setTextColor(selectedColor)
        holder.restaurantName.text = ""+leftMenuItemList[position].menuName
        holder.llMain.setOnClickListener { clickListener(position)}

//        val mIcon = ContextCompat.getDrawable(context, leftMenuItemList[position].icon)
//        mIcon!!.setColorFilter(
//            selectedColor,
//            PorterDuff.Mode.MULTIPLY
//        )
        holder.img_icon.setImageResource(leftMenuItemList[position].icon)
        holder.img_icon.setColorFilter(selectedColor)
    }
}