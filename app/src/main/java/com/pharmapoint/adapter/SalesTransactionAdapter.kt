package com.pharmapoint.adapter


import android.util.Log
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.pharmapoint.R
import com.pharmapoint.databinding.LayoutSalesTxnAdapterBinding
import com.pharmapoint.model.OrderModel

class SalesTransactionAdapter(
    private val list: List<OrderModel>,
) : BaseAdapter<LayoutSalesTxnAdapterBinding, OrderModel>(list) {

    override val layoutId: Int = R.layout.layout_sales_txn_adapter

    override fun bind(binding: LayoutSalesTxnAdapterBinding, item: OrderModel) {
        binding.apply {
            salesTxnViewModel = item
            executePendingBindings()
        }
    }

}
