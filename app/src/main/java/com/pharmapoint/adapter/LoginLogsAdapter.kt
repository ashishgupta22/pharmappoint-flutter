package com.pharmapoint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R
import com.pharmapoint.model.LogModel
import com.pharmapoint.utlity.Constans
import com.pharmapoint.utlity.Utils
import kotlinx.android.synthetic.main.layout_login_logs_adapter.view.*

class LoginLogsAdapter(private val context: Context, private val leftMenuItemList: MutableList<LogModel>, private val clickListener: (Int?, Int?) -> Unit) : RecyclerView.Adapter<LoginLogsAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val txtSrNo: TextView = view.txtSrNo
        val txtIn: TextView = view.txtIn
        val txtOut: TextView = view.txtOut
        val txtType: TextView = view.txtType
        val txtDate: TextView = view.txtDate
        val txtUserName: TextView = view.txtUserName
        val txtActiveTime: TextView = view.txtActiveTime
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_login_logs_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return leftMenuItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtSrNo.text = "${(position+1)}"
        if(leftMenuItemList[position].log_time_in != null)
        holder.txtIn.text = Utils.toDateTimeString(Utils.toDate(leftMenuItemList[position].log_time_in))
        if(leftMenuItemList[position].log_time_out != null)
        holder.txtOut.text = Utils.toDateTimeString(Utils.toDate(leftMenuItemList[position].log_time_out))
        else
            holder.txtOut.text = "--"
        holder.txtType.text = ""+Constans.timeTypeArray[leftMenuItemList[position].type!!]
        if(leftMenuItemList[position].log_time_in != null && leftMenuItemList[position].log_time_out != null){
            val millis: Long = leftMenuItemList[position]?.log_time_out!! - leftMenuItemList[position].log_time_in!!
            val hours = (millis / (1000 * 60 * 60)).toInt()
            val mins = (millis / (1000 * 60) % 60).toInt()
            val diff = "${hours}hr : ${mins}min"
            holder.txtActiveTime.setText(diff)

        }else
            holder.txtActiveTime.setText("--")
        holder.txtDate.text = Utils.toDateString(Utils.toDate(leftMenuItemList[position].created_at))
        holder.txtUserName.text  = leftMenuItemList[position].name
    }
}
