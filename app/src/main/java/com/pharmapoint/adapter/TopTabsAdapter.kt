package com.pharmapoint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R
import com.pharmapoint.model.TabtemModel
import kotlinx.android.synthetic.main.layout_menu.view.*
import android.graphics.PorterDuff


import androidx.core.content.ContextCompat

import android.graphics.drawable.Drawable
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.layout_menu.view.img_logo
import kotlinx.android.synthetic.main.layout_menu.view.txtStatus
import kotlinx.android.synthetic.main.layout_menu_top.view.*

class TopTabsAdapter(
    private val context: Context,
    private val leftMenuItemList: List<TabtemModel>,
    private val clickListener: (Int?) -> Unit
) : RecyclerView.Adapter<TopTabsAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val restaurantName: TextView = view.txtStatus
        val llMain: RelativeLayout = view.rlMenu
        val img_icon: ImageView = view.img_logo
        val imgBackground: ImageView = view.imgBackground

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_menu_top, parent, false))
    }

    override fun getItemCount(): Int {
        return leftMenuItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val displaymetrics = DisplayMetrics()
//        (context as Activity).windowManager.defaultDisplay.getMetrics(displaymetrics)
        val selectedColor: Int
        if(leftMenuItemList[position].isSelected){
            selectedColor = context.resources.getColor(R.color.purple_700)
            holder.imgBackground.setImageResource(R.drawable.ic_top_background_white)
        }else{
            holder.imgBackground.setImageResource(R.drawable.ic_top_background)
            selectedColor = context.resources.getColor(R.color.color_menu_left)
        }
        holder.restaurantName.setTextColor(selectedColor)
        holder.restaurantName.text = ""+leftMenuItemList[position].menuName
        holder.llMain.setOnClickListener { clickListener(position)}

//        val mIcon = ContextCompat.getDrawable(context, leftMenuItemList[position].icon)
//        mIcon!!.setColorFilter(
//            selectedColor,
//            PorterDuff.Mode.MULTIPLY
//        )
//        holder.img_icon.setImageDrawable(mIcon)
        holder.img_icon.setColorFilter(selectedColor)
    }
}