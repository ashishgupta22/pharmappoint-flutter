package com.pharmapoint.adapter


import android.util.Log
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.pharmapoint.R
import com.pharmapoint.databinding.LayoutProductSoldAdapterBinding
import com.pharmapoint.databinding.LayoutSalesTxnAdapterBinding
import com.pharmapoint.model.OrderDetailsModel
import com.pharmapoint.model.OrderModel

class ProductSoldAdapter(
    private val list: List<OrderDetailsModel>,
) : BaseAdapter<LayoutProductSoldAdapterBinding, OrderDetailsModel>(list) {
    override val layoutId: Int = R.layout.layout_product_sold_adapter
    override fun bind(binding: LayoutProductSoldAdapterBinding, item: OrderDetailsModel) {
        binding.apply {
            viewModel = item
            executePendingBindings()
        }
    }
}
