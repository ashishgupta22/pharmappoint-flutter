package com.pharmapoint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R
import com.pharmapoint.model.CategoryModel
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.imgDelete
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.imgEdit
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.txtComment
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.txtName
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.txtStatus
import kotlinx.android.synthetic.main.layout_sub_category_adapter.view.*

class SubCategoryAdapter(
    private val context: Context,
    private val subCategoryList: MutableList<CategoryModel>,
    private val categoryList: MutableList<CategoryModel>,
    private val clickListener: (Int?, Int?) -> Unit
) : RecyclerView.Adapter<SubCategoryAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val txtName: TextView = view.txtName
        val txtComment: TextView = view.txtComment
        val txtStatus: TextView = view.txtStatus
        val txtMainCategory: TextView = view.txtMainCategory
        val imgEdit: ImageView = view.imgEdit
        val imgDelete: ImageView = view.imgDelete
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_sub_category_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return subCategoryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var status = ""
        if(subCategoryList[position].status == 1){
            status = "Active"
            holder.txtStatus.setTextColor(context.resources.getColor(R.color.green))
        }else {
            holder.txtStatus.setTextColor(context.resources.getColor(R.color.red))
            status = "Inactive"
        }
        val index = categoryList.indexOfFirst { it.id ==  subCategoryList[position].parent_id}
        if(index>=0){
            holder.txtMainCategory.text = ""+categoryList[index].title
        }
        holder.txtName.text = ""+subCategoryList[position].title
        holder.txtComment.text = ""+subCategoryList[position].comment
        holder.txtStatus.text = status
        holder.imgEdit.setOnClickListener{
            clickListener(0,position)
        }
        holder.imgDelete.setOnClickListener{
            clickListener(1,position)
        }

    }
}