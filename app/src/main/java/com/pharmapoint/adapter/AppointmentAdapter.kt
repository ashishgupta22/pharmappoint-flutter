package com.pharmapoint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R
import com.pharmapoint.model.AppoinmentModel
import com.pharmapoint.utlity.Utils
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.*

class AppointmentAdapter(
    private val context: Context,
    private val leftMenuItemList: List<AppoinmentModel>,
    private val clickListener: (Int?,Int?) -> Unit
) : RecyclerView.Adapter<AppointmentAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val txtName: TextView = view.txtName
        val txtEmail: TextView = view.txtEmail
        val txtDateTime: TextView = view.txtDateTime
        val txtComment: TextView = view.txtComment
        val txtStatus: TextView = view.txtStatus
        val imgEdit: ImageView = view.imgEdit
        val imgDelete: ImageView = view.imgDelete
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_appointment_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return leftMenuItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtName.text = ""+leftMenuItemList[position].customer_name
        if(leftMenuItemList[position].customer_mobile != null)
        holder.txtEmail.text = ""+leftMenuItemList[position].customer_mobile
        holder.txtDateTime.text = Utils.toDateTimeString(Utils.toDate(leftMenuItemList[position].appointment_date))
        holder.txtComment.text = ""+leftMenuItemList[position].comment
        holder.txtStatus.text = ""+leftMenuItemList[position].status
        holder.imgEdit.setOnClickListener({
            clickListener(0,position)
        })
        holder.imgDelete.setOnClickListener({
            clickListener(1,position)
        })

    }
}