package com.pharmapoint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R
import com.pharmapoint.model.CategoryModel
import com.pharmapoint.utlity.Utils
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.*

class CategoryAdapter(
    private val context: Context,
    private val leftMenuItemList: MutableList<CategoryModel>,
    private val clickListener: (Int?, Int?) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val txtName: TextView = view.txtName
        val txtComment: TextView = view.txtComment
        val txtStatus: TextView = view.txtStatus
        val imgEdit: ImageView = view.imgEdit
        val imgDelete: ImageView = view.imgDelete
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_category_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return leftMenuItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var status = ""
        if(leftMenuItemList[position].status == 1){
            status = "Active"
            holder.txtStatus.setTextColor(context.resources.getColor(R.color.green))
        }else {
            holder.txtStatus.setTextColor(context.resources.getColor(R.color.red))
            status = "Inactive"
        }

        holder.txtName.text = ""+leftMenuItemList[position].title
        holder.txtComment.text = ""+leftMenuItemList[position].comment
        holder.txtStatus.text = status
        holder.imgEdit.setOnClickListener({
            clickListener(0,position)
        })
        holder.imgDelete.setOnClickListener({
            clickListener(1,position)
        })

    }
}