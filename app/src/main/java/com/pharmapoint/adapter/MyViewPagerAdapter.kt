package com.pharmapoint.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.pharmapoint.model.TabtemModel
import android.text.Spannable

import android.text.style.ImageSpan

import android.text.SpannableString

import android.graphics.drawable.Drawable




class MyViewPagerAdapter(manager: FragmentManager, lifecycle: Lifecycle) :  FragmentStateAdapter(manager,lifecycle ){
    private val fragmentList : MutableList<Fragment> =ArrayList()
    private val titleList : MutableList<TabtemModel> =ArrayList()
    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return  fragmentList[position]
    }


    fun addFragment(fragment: MutableList<Fragment>, title: MutableList<TabtemModel>){
        fragmentList.clear()
        titleList.clear()
        fragmentList.addAll(fragment)
        titleList.addAll(title)
    }

    fun getPageTitle(position: Int): CharSequence? {
//        val image: Drawable = titleList[position].icon
//        image.setBounds(0, 0, image.intrinsicWidth, image.intrinsicHeight)
//        // Replace blank spaces with image icon
//        // Replace blank spaces with image icon
//        val sb = SpannableString("   " + titleList[position].menuName)
//        val imageSpan = ImageSpan(image, ImageSpan.ALIGN_BOTTOM)
//        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return titleList[position].menuName
    }
}