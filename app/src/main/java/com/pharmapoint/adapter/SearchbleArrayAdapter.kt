package com.pharmapoint.adapter

import android.app.Activity
import android.content.Context
import android.view.ViewGroup

import android.view.LayoutInflater
import android.view.View

import android.widget.ArrayAdapter
import com.pharmapoint.R
import kotlinx.android.synthetic.main.layout_custom_spinner.view.*

class SearchbleArrayAdapter(activity: Activity, Strings: ArrayList<String>) :
    ArrayAdapter<String>(activity, R.layout.layout_custom_spinner_view, Strings) {

    var filtered = Strings

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return convertView ?: createView(position, parent)
    }

    private fun createView(position: Int, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_custom_spinner_view, parent, false)
        view?.txtTitle?.text = filtered[position]
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        convertView ?: LayoutInflater.from(context).inflate(R.layout.layout_custom_spinner, parent, false)
        convertView?.txtTitle?.text = filtered[position]
        return super.getDropDownView(position, convertView, parent)
    }

    override fun getCount() = filtered.size

    override fun getItem(position: Int) = filtered[position]

}
