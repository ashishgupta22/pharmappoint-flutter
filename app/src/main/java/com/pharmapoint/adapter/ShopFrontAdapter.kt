package com.pharmapoint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R


import com.pharmapoint.model.ProductModel
import com.pharmapoint.utlity.Utils
import kotlinx.android.synthetic.main.layout_shopfront_adapter.view.*

class ShopFrontAdapter(
    private val context: Context,
    private val productList: MutableList<ProductModel>,
    private val clickListener: (Int?,Int?,Boolean?) -> Unit
) : RecyclerView.Adapter<ShopFrontAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val txtMedName: TextView = view.txtMedName
        val txtQty: TextView = view.txtQty
        val txtPrice: TextView = view.txtPrice
        val txtVat: TextView = view.txtVat
        val txtAmount: TextView = view.txtAmount
        val txtDiscount: TextView = view.txtDiscount
        val chk_product: CheckBox = view.chk_product
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_shopfront_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


       val totalVat =  productList[position].retail_cost?.let { productList[position].vat?.let { it1 ->
            Utils.calculateVAT(it,productList[position].seletedQTY,
                it1
            )
        } }
        holder.txtMedName.text = ""+productList[position].product_name
        holder.txtQty.text = "${productList[position].seletedQTY}"
        holder.txtPrice.text = ""+productList[position].retail_cost
        holder.txtVat.text = ""+Utils.decimalFormat(productList[position].vat!!)
        holder.txtAmount.text = "${Utils.decimalFormat((productList[position].retail_cost?.times(productList[position].seletedQTY!!)!! + totalVat!!))}"
        holder.txtDiscount.text = ""+productList[position].allow_discount
        holder.chk_product.setOnCheckedChangeListener{buttonView, isChecked ->
            productList[position].isSelected = isChecked
            clickListener(0,position,isChecked)
        }
        holder.chk_product.isChecked = productList[position].isSelected
    }
}