package com.pharmapoint.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R
import com.pharmapoint.model.ProductModel
import com.pharmapoint.utlity.Utils
import kotlinx.android.synthetic.main.inventry_fragment.view.txtDiscount
import kotlinx.android.synthetic.main.inventry_fragment.view.txtMedName
import kotlinx.android.synthetic.main.inventry_fragment.view.txtPrice
import kotlinx.android.synthetic.main.inventry_fragment.view.txtStock
import kotlinx.android.synthetic.main.inventry_fragment.view.txtvat
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.txtStatus
import kotlinx.android.synthetic.main.layout_inventory_adapter.view.*

class InventoryAdapter(
    private val context: Context,
    private val leftMenuItemList: MutableList<ProductModel>,
    private val clickListener: (Int?,Int?) -> Unit
) : RecyclerView.Adapter<InventoryAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val txtMedName: TextView = view.txtMedName
        val txtPrice: TextView = view.txtPrice
        val txtvat: TextView = view.txtvat
        val txtDiscount: TextView = view.txtDiscount
        val txtStock: TextView = view.txtStock
        val txtStatus: TextView = view.txtStatus
        val txtExpDate: TextView = view.txtExpDate
        val imgEdit: ImageView = view.imgServiceEdit
        val imgDelete: ImageView = view.imgServiceDelete    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_inventory_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return leftMenuItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtMedName.text = ""+leftMenuItemList[position].product_name
        holder.txtPrice.text = ""+leftMenuItemList[position].retail_cost
        holder.txtvat.text = ""+leftMenuItemList[position].vat
        holder.txtDiscount.text = ""+leftMenuItemList[position].allow_discount
        holder.txtStock.text = ""+leftMenuItemList[position].restQty
        holder.txtExpDate.text = ""+Utils.longDateToString(leftMenuItemList[position].expire_date)
        if(leftMenuItemList[position].status == 1){
            holder.txtStatus.setTextColor(Color.parseColor("#4CAF50"))
            holder.txtStatus.text = "Active"
        }else{
            holder.txtStatus.setTextColor(Color.parseColor("#F44336"))
            holder.txtStatus.text = "Deactive"
        }

        holder.imgEdit.setOnClickListener({
            clickListener(0,position)
        })
        holder.imgDelete.setOnClickListener({
            clickListener(1,position)
        })
    }
}