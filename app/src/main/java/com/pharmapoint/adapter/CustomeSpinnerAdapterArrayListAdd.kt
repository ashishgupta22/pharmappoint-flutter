package com.pharmapoint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.pharmapoint.R

class CustomeSpinnerAdapterArrayListAdd(internal var context: Context, internal var fruit: ArrayList<String>) : BaseAdapter() {
    internal var inflter: LayoutInflater

    init {
        inflter = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return fruit.size
    }

    override fun getItem(i: Int): Any? {
        return i
    }

    override fun getItemId(i: Int): Long {
        return 0
    }
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = inflter.inflate(R.layout.layout_custom_spinner,null)
        val names = view.findViewById<View>(R.id.txtTitle) as TextView?
        names!!.text = fruit[position]
        if(position ==fruit.size -1){
            names.setTextColor(context.resources.getColor(R.color.red))
        }

        if(position ==0){
            names.setTextColor(context.resources.getColor(R.color.color_theme))
        }
        return view
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        val view = inflter.inflate(R.layout.layout_custom_spinner_view,null)
        val names = view.findViewById<View>(R.id.txtTitle) as TextView?
        names!!.text = fruit[i]
        return view
    }
}