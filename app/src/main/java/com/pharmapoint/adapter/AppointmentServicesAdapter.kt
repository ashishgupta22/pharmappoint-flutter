package com.pharmapoint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pharmapoint.R
import com.pharmapoint.model.*
import com.pharmapoint.utlity.Utils
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.*
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.imgDelete
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.imgEdit
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.txtComment
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.txtName
import kotlinx.android.synthetic.main.layout_appointment_adapter.view.txtStatus
import kotlinx.android.synthetic.main.layout_appointment_services_adapter.view.*

class AppointmentServicesAdapter(
    private val context: Context,
    private val leftMenuItemList: MutableList<AppointmentServicesModel>,
    private val clickListener: (Int?, Int?) -> Unit
) : RecyclerView.Adapter<AppointmentServicesAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val txtName: TextView = view.txtServiceName
        val txtComment: TextView = view.txtServiceComment
        val txtPrice: TextView = view.txtServicePrice
        val txtStatus: TextView = view.txtServiceStatus
        val imgEdit: ImageView = view.imgServiceEdit
        val imgDelete: ImageView = view.imgServiceDelete
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_appointment_services_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return leftMenuItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var status = ""
        if(leftMenuItemList[position].status == 1){
            status = "Active"
            holder.txtStatus.setTextColor(context.resources.getColor(R.color.green))
        }else {
            holder.txtStatus.setTextColor(context.resources.getColor(R.color.red))
            status = "Inactive"
        }

        holder.txtName.text = ""+leftMenuItemList[position].title
        holder.txtComment.text = ""+leftMenuItemList[position].comment
        holder.txtPrice.text = ""+leftMenuItemList[position].price
        holder.txtStatus.text = status
        holder.imgEdit.setOnClickListener({
            clickListener(0,position)
        })
        holder.imgDelete.setOnClickListener({
            clickListener(1,position)
        })

    }
}